---
layout: page
title: Whitepaper Space Leiden
permalink: /whitepaper
redirect_from: /en/whitepaper/
---
## Wat is een maker / hackerspace
Zowel makerspace als hackerspace zijn werkplaatsen die ruimte bieden voor mensen die geïnteresseerd zijn om samen te komen; te maken, te hacken, te innoveren, te inspireren, om kennis te delen en van elkaar te leren.

Een makerspace draait om het kunnen maken van verschillende producten, zoals een eigen designlamp, werken aan elektronica of 3D printen. Er is toegang tot verschillende gereedschappen en men deelt kennis met elkaar. In het geval van een hackerspace hoeft dit niet zozeer te gaan om het maken van iets fysieks, maar kan dit bijvoorbeeld gaan om het maken van programma's, knutselen met verschillende technologieën of het zoeken van "mogelijkheden" met bestaande toepassingen.

## Visie
Onze visie is om een eigen draai te geven aan een hacker/makerspace in Leiden, door ons genoemd; "Space Leiden". Space Leiden moet een plek zijn waar je kan werken, vergaderen, maken, onderzoeken en gezellig kan zijn met mede geïnteresseerden. Het moet een combinatie zijn van een makerspace, hackerspace en bar. Met gereedschap, 3D printers, IT servers en honeypots voor mensen om te kunnen klooien en hacken. Apparatuur die te groot, te duur of te specialistisch is om thuis te hebben.

Het moet een plek zijn om samen te kunnen werken maar ook om een kopje koffie of een klein biertje te kunnen drinken.

## Wat willen we bereiken
We willen een functionerende maker- en hackerspace, met genoeg ruimte voor minstens 50 man op elk moment van de dag. We willen de mogelijkheid bieden voor goed bezochte workshops, meetups en evenementen om te netwerken. Daarnaast willen we een plek zijn waar men overdag, zowel doordeweeks als in het weekend, kan (samen)werken.

## Wat gaan wij betekenen voor Leiden
We willen een community zijn waar men elkaar helpt, waar men kan leren en waar wij mensen kunnen onderwijzen. We willen een plek zijn waar scholieren, studenten, werkenden en creatievelingen met elkaar in contact kunnen komen. Vanuit de expertise van de organisatie willen we graag de mogelijkheid bieden tot workshops voor programmeren en werken met verschillende tools die beschikbaar zijn in de makerspace. Daarnaast willen wij graag iedereen onderwijzen in het belang van privacy en cybersecurity.

## Wat hebben we nodig

### Ruimte
Om onze visie te realiseren, zijn wij op zoek naar een ruimte die voor mensen gemakkelijk bereikbaar en toegankelijk is (tevens voor mindervaliden). Onze voorkeur gaat uit naar een bereikbare locatie; bijvoorbeeld binnen vijf minuten van een bushalte. Bij voorkeur hebben we een ruimte dicht bij het centraal station of het biosciencepark. Dit omdat we dan makkelijker met studenten en werkenden in contact kunnen blijven. In deze ruimte moet voldoende ruimte zijn om deze in te richten met werkplekken, het geven van een lecture of workshop, het organiseren van meetups en het stallen van onze apparatuur.

### Spullen
Op het gebied van apparatuur en materiaal willen wij graag in ieder geval de volgende faciliteiten kunnen aanbieden:
- werkplekken (bureau's, stoelen, elektriciteit en internetverbinding);
- toegang tot of korting op verschillende software en licenties;
- toegang tot het gebruik van materiaal, apparatuur en hardware zoals:
	- Vaste computers
	- VPN's en servers
	- 3D printers
	- VR-brillen
	- Domotica en robotica materialen: sensoren, weerstanden, printplaten, soldeerbouten, etc.
	- Gereedschap en apparaten zoals: boormachines, zagen, lasapparatuur, laser-snijders, etc.
	- Honeypots

### Budget
Om dit op te zetten en het eerste jaar zonder andere financiële investeringen te kunnen dragen hopen wij een bedrag op te halen waarmee wij een locatie, materiaal en promotie kunnen dekken. Dit bedrag willen we halen uit sponsoring zoals bijvoorbeeld een GoFundMe pagina, inkomsten uit de bar, lidmaatschap opbrengsten en eventuele subsidies.

## Toegankelijkheid
Space Leiden is voor iedereen toegankelijk, maar deze toegang heeft wel een prijs. Er zijn mogelijkheden tot een maandelijks of jaarlijks abonnement, ook mogelijk om te kiezen voor een eenmalige toegang of strippenkaart.

We richten ons op iedereen van jong tot oud, om de veiligheid te waarborgen moet men onder de 16 begeleid worden door een volwassene.
