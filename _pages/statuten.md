---
layout: page
title: Statuten
permalink: /statuten
redirect_from: /en/statuten
---

## Artikel 1.
## Begripsbepalingen.

In deze statuten wordt verstaan onder:

- Bestuur: het bestuur van de Stichting;
- Schriftelijk: bij brief, fax of e-mail, of bij boodschap die via een ander gangbaar communicatiemiddel wordt overgebracht en elektronisch of op schrift kan worden ontvangen mits de identiteit van de verzender met afdoende zekerheid kan worden vastgesteld;
- Statuten: de statuten van de Stichting, zoals die van tijd tot tijd zullen luiden;
- Stichting: de rechtspersoon waarop de Statuten betrekking hebben.

## Artikel 2.
## Naam en zetel.

1. De Stichting draagt de naam: Stichting The Space Leiden.
2. Zij heeft haar zetel in de gemeente Leiden

## Artikel 3.
## Doel.

1. De Stichting heeft ten doel het bevorderen van technische ontwikkeling van (jong)volwassenen alsmede het aanvaarden van erfstellingen onder het voorrecht van boedelbeschrijving en voorts al hetgeen in de ruimste zin met één en ander verband houdt, daartoe behoort en/of daartoe bevorderlijk kan zijn.
2. De Stichting tracht haar doel onder meer te bereiken door:
	a. het opzetten en beschikbaar stellen van een technisch clubhuis
        ("hackerspace" en "makerspace") aan deelnemers met werkplaatsen waar plek is voor nieuwe technieken zoals 3D printers, lasersnijders, servers en honeypots alsmede klassieke technieken zoals hout-, textiel- en metaalbewerking.
    b. het ontwikkelen en/of aanbieden van activiteiten programma's, projecten en cursussen gericht op (jong)volwassenen en gericht op eigen initiatief en ontwikkelen van de "maker” en “hacker" mentaliteit.
3. De Stichting heeft niet ten doel het maken van winst.

## Artikel 4.
## Vermogen.

1. Het vermogen van de Stichting zal worden gevormd door:
    a.    subsidies en andere bijdragen;
    b.    schenkingen, erfstellingen en legaten;
    c.    alle andere verkrijgingen en baten.
2. De Stichting kan erfstellingen slechts aanvaarden onder het voorrecht van boedelbeschrijving.

## Artikel 5.
## Bestuur.

1. Het Bestuur bestaat uit een door het Bestuur te bepalen oneven aantal van ten minste drie (3) en maximaal negen (9) leden en wordt voor de eerste maal bij deze akte benoemd.
2. Het Bestuur (met uitzondering van het eerste Bestuur, waarvan de leden in functie worden benoemd) kiest uit zijn midden een voorzitter, een secretaris en een penningmeester, tezamen vormende het dagelijks bestuur. De functies van secretaris en penningmeester kunnen ook door één persoon worden vervuld.
3. De bestuurders worden benoemd voor onbepaalde tijd.
4. Bij het ontstaan van één (of meer) vacature(s) in het Bestuur zullen de
    overblijvende bestuurders met algemene stemmen (of zal de enig
    overblijvende bestuurder) binnen drie maanden na het ontstaan van de
    vacature(s) daarin voorzien door de benoeming van één (of meer) opvolger(s).
5. Mocht dan wel mochten in het Bestuur om welke reden dan ook één of meer leden ontbreken, dan vormen de overblijvende bestuurders, of vormt de enig overblijvende bestuurder niettemin een wettig Bestuur.
6. Bij verschil van mening tussen de overblijvende bestuurders omtrent de
    benoeming, alsmede wanneer te eniger tijd alle bestuurders mochten komen te ontbreken voordat aanvulling van de ontstane vacature(s) plaats had en voorts indien de overgebleven bestuurders zouden nalaten binnen de in lid 4 van dit artikel genoemde termijn in de vacature(s) te voorzien, zal die voorziening geschieden door de rechtbank op verzoek van iedere belanghebbende of op vordering van het openbaar ministerie.

## Artikel 6.
## Vergaderingen van het Bestuur en besluiten van het Bestuur.

1. De vergaderingen van het Bestuur worden gehouden op de van keer tot keer door het Bestuur te bepalen plaatsen.
2. Ieder half jaar wordt ten minste één vergadering gehouden.
3. Vergaderingen zullen voorts telkenmale worden gehouden, wanneer de voorzitter dit wenselijk acht of indien één van de andere bestuurders daartoe Schriftelijk en onder nauwkeurige opgave van de te behandelen punten aan de voorzitter het verzoek richt.
   Indien de voorzitter aan een dergelijk verzoek geen gevolg geeft zodanig, dat de vergadering kan worden gehouden binnen drie weken na het verzoek, is de verzoeker gerechtigd zelf een vergadering bijeen te roepen met inachtneming van de vereiste formaliteiten.
4. De oproeping tot de vergadering geschiedt - behoudens het in lid 3 bepaalde - door de voorzitter, ten minste zeven dagen tevoren, de dag van de oproeping en die van de vergadering niet meegerekend, Schriftelijk.
5. De oproeping vermeldt behalve plaats en tijdstip van de vergadering, de te behandelen onderwerpen.
6. Indien de door de Statuten gegeven voorschriften voor het oproepen en houden van vergaderingen niet in acht zijn genomen, kunnen desalniettemin in een vergadering van het Bestuur geldige besluiten worden genomen over alle aan de orde komende onderwerpen, mits in de betreffende vergadering van het Bestuur alle in functie zijnde bestuurders aanwezig zijn en mits de betreffende besluiten worden genomen met algemene stemmen.
7. De vergaderingen worden geleid door de voorzitter van het Bestuur; bij diens afwezigheid wijst de vergadering zelf haar voorzitter aan.
8. Van het verhandelde in de vergaderingen worden notulen gehouden door de secretaris of door een van de andere aanwezigen, door de voorzitter daartoe aangezocht.
    De notulen worden vastgesteld in de eerstvolgende vergadering en ten blijke daarvan getekend door de voorzitter en secretaris van die vergadering.
9. Het Bestuur kan in een vergadering alleen dan geldige besluiten nemen indien de meerderheid van zijn in functie zijnde leden in de vergadering aanwezig of vertegenwoordigd is.
    Een bestuurder kan zich in de vergadering door een mede-bestuurder laten vertegenwoordigen op overlegging van een schriftelijke, ter beoordeling van de voorzitter van de vergadering voldoende, volmacht.
    Een bestuurder kan daarbij slechts voor één mede-bestuurder als gevolmachtigde optreden.
    Een bestuurder neemt niet deel aan de beraadslaging en besluitvorming indien hij daarbij een direct of indirect persoonlijk belang heeft dat tegenstrijdig is met het belang van de Stichting en de met haar verbonden onderneming of organisatie.
    Wanneer hierdoor geen bestuursbesluit zou kunnen worden genomen, wordt het besluit desalniettemin genomen door het Bestuur onder schriftelijke vastlegging van de overwegingen die aan het besluit ten grondslag liggen.
10. Het Bestuur kan ook buiten vergadering besluiten nemen, mits alle bestuurders hun stem Schriftelijk hebben uitgebracht.
    Het bepaalde in de vorige volzin geldt ook voor besluiten tot wijziging van de Statuten of ontbinding van de Stichting.
    Voor besluitvorming buiten vergadering gelden dezelfde meerderheden als voor besluitvorming in vergadering.
    Van een buiten vergadering genomen besluit wordt onder bijvoeging van de ingekomen stemmen door de secretaris een relaas opgemaakt, dat na medeondertekening door de voorzitter bij de notulen wordt gevoegd.
11. Iedere bestuurder heeft het recht tot het uitbrengen van één stem.
    Voor zover de Statuten geen grotere meerderheid voorschrijven worden alle besluiten van het Bestuur genomen met volstrekte meerderheid van de geldig uitgebrachte stemmen.
    Indien de stemmen staken komt geen besluit tot stand.
    Eén of meer bestuurders hebben het recht om binnen tien dagen na de dag van de vergadering, waarin de stemmen hebben gestaakt, aan het Nederlands Arbitrage Instituut te verzoeken een adviseur te benoemen, teneinde een beslissing over het betreffende voorstel te nemen.
    De beslissing van de adviseur geldt alsdan als een besluit van het Bestuur.
12. Alle stemmingen ter vergadering geschieden mondeling, tenzij de voorzitter een schriftelijke stemming gewenst acht of één van de stemgerechtigden dit voor de stemming verlangt.
    Schriftelijke stemming geschiedt bij ongetekende, gesloten briefjes.
13. Blanco stemmen worden beschouwd als niet te zijn uitgebracht.
14. In alle geschillen omtrent stemmingen, niet bij de Statuten voorzien, beslist de voorzitter.
15. Het hiervoor in dit artikel bepaalde is zoveel mogelijk van overeenkomstige toepassing op vergaderingen en besluiten van het dagelijks bestuur.

## Artikel 7.
## Bestuursbevoegdheid en vergoedingen.

1. Het Bestuur is belast met het besturen van de Stichting.
2. Het Bestuur is, mits de betreffende besluiten worden genomen met algemene stemmen van alle in functie zijnde bestuurders, bevoegd te besluiten tot het aangaan van overeenkomsten tot verkrijging, vervreemding en bezwaring van registergoederen en tot het aangaan van overeenkomsten waarbij de Stichting zich als borg of hoofdelijk medeschuldenaar verbindt, zich voor een derde sterk maakt of zich tot zekerheidstelling voor een schuld van een ander verbindt.
3. Bij de vervulling van hun taak richten de bestuurders zich naar het belang van de Stichting en de met haar verbonden organisatie.
4. In geval van ontstentenis of belet van één of meer bestuurders is (zijn) de overblijvende bestuurder(s) met het gehele bestuur belast.
    In geval van ontstentenis of belet van alle bestuurders of van de enige bestuurder wordt de Stichting tijdelijk bestuurd door een persoon die daartoe door het Bestuur steeds moet zijn aangewezen.
    Onder belet wordt in deze statuten in ieder geval verstaan de omstandigheid dat de bestuurder gedurende een periode van meer dan zeven dagen onbereikbaar is door ziekte of andere oorzaken.
5. Aan de bestuurders kan geen beloning worden toegekend. Kosten worden aan de bestuurders op vertoon van de bewijsstukken vergoed.

## Artikel 8.
## Vertegenwoordiging.

1. De Stichting wordt vertegenwoordigd door het Bestuur, voor zover uit de wet niet anders voortvloeit.
    De Stichting kan voorts worden vertegenwoordigd door twee gezamenlijk handelende leden van het dagelijks bestuur.
2. Het Bestuur kan aan anderen volmacht geven om de Stichting in en buiten rechte te vertegenwoordigen binnen de in die volmacht omschreven grenzen.

## Artikel 9.
## Einde lidmaatschap van het Bestuur.

Het lidmaatschap van het Bestuur eindigt:

-    door overlijden van een bestuurder;
-    bij verlies van het vrije beheer over zijn vermogen;
-    bij schriftelijke ontslagneming (bedanken);
-    bij ontslag op grond van artikel 2:298 Burgerlijk Wetboek;
-    door een besluit door de overige bestuurders met algemene stemmen genomen;

## Artikel 10.
## Boekjaar en jaarstukken.

1. Het boekjaar van de Stichting loopt van de eerste dag van september tot en met de laatste dag van augustus.
2. Per het einde van ieder boekjaar maakt de penningmeester een balans en een staat van baten en lasten over het geëindigde boekjaar op, welke jaarstukken binnen zes maanden na afloop van het boekjaar en, indien de subsidiënten zulks wensen, vergezeld van een rapport van een registeraccountant of een accountant-administratieconsulent aan het Bestuur worden aangeboden.
3. De jaarstukken worden door het Bestuur vastgesteld.
Vaststelling van de jaarstukken door het Bestuur strekt de penningmeester tot decharge voor het door hem gevoerde beheer.

## Artikel 11.
## Commissies.

Het Bestuur is bevoegd een of meer commissies in te stellen, waarvan de taken en bevoegdheden alsdan zullen worden vastgesteld bij huishoudelijk reglement.

## Artikel 12.
## Raad van Advies.

Het Bestuur kan een Raad van Advies instellen, die alsdan in ieder geval tot taak zal hebben het Bestuur gevraagd en ongevraagd te adviseren.
De verdere taken en bevoegdheden zullen alsdan bij huishoudelijk reglement worden vastgesteld.

## Artikel 13.
## Directeur.

1. Het Bestuur kan een directeur benoemen en deze belasten met de dagelijkse gang van zaken van de Stichting.
2. Indien een directeur is benoemd kan deze door het Bestuur met inachtneming van de daartoe strekkende wettelijke bepalingen worden ontslagen.
3. De directeur heeft in de vergaderingen van het Bestuur een adviserende stem.

## Artikel 14.
## Reglementen.

1. Het Bestuur is bevoegd een of meer reglementen vast te stellen, waarin die onderwerpen worden geregeld, welke niet in de Statuten zijn vervat.
2. De reglementen mogen niet met de wet of de Statuten in strijd zijn.
3. Het Bestuur is te allen tijde bevoegd de reglementen te wijzigen of op te heffen.
4. Op de vaststelling, wijziging en opheffing van de reglementen is het bepaalde in artikel 15 leden 1 en 2 van overeenkomstige toepassing.

## Artikel 15.
## Statutenwijziging.

1. Het Bestuur is bevoegd de Statuten te wijzigen.
    Onverminderd het bepaalde in artikel 6 lid 10 moet het besluit daartoe worden genomen met een meerderheid van ten minste drie/vierde van de uitgebrachte stemmen in een vergadering van het Bestuur, waarin alle bestuurders aanwezig of vertegenwoordigd zijn.
2. Zijn in een vergadering, waarin een voorstel als bedoeld in lid 1 van dit artikel aan de orde is gesteld niet alle bestuurders aanwezig of vertegenwoordigd dan zal een tweede vergadering van het Bestuur worden bijeengeroepen, te houden niet eerder dan zeven dagen, doch niet later dan één en twintig dagen na de eerste, waarin een zodanig besluit kan worden genomen met een meerderheid van ten minste drie/vierde van de uitgebrachte stemmen, en in welke vergadering ten minste de meerderheid van de in functie zijnde bestuurders aanwezig of vertegenwoordigd is.
3. Iedere bestuurder is bevoegd de notariële akte van statutenwijziging te verlijden.

## Artikel 16.
## Ontbinding en vereffening.

1. Het Bestuur is bevoegd de Stichting te ontbinden.
    Op het daartoe te nemen besluit is het bepaalde in artikel 15 leden 1 en 2 van overeenkomstige toepassing.
2. De Stichting blijft na haar ontbinding voortbestaan voor zover dit tot vereffening van haar vermogen nodig is.
3. De vereffening geschiedt door het Bestuur.
4. De vereffenaars dragen er zorg voor dat van de ontbinding van de Stichting inschrijving geschiedt in het register, bedoeld in artikel 2:289 Burgerlijk Wetboek.
5. Gedurende de vereffening blijven de bepalingen van de Statuten zoveel mogelijk van kracht.
6. Een eventueel batig saldo van de ontbonden Stichting wordt besteed ten behoeve van een algemeen nut beogende instelling met een soortgelijke doelstelling als de doelstelling van de Stichting of van een buitenlandse instelling die uitsluitend of nagenoeg uitsluitend het algemeen nut beoogt en die een soortgelijke doelstelling als de doelstelling van de Stichting heeft.
7. Na afloop van de vereffening blijven de boeken, bescheiden en andere gegevensdragers van de ontbonden Stichting gedurende zeven jaren berusten onder de jongste vereffenaar.

## Artikel 17.
## Slotbepaling.

In alle gevallen waarin zowel de wet als de Statuten niet voorzien, beslist het Bestuur.

## Artikel 18.
## Overgangsbepaling.

Het eerste boekjaar van de Stichting loopt tot en met éénendertig augustus
tweeduizend éénentwintig (31-08-2021).
Dit artikel vervalt nadat het eerste boekjaar is geëindigd.
