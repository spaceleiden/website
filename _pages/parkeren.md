---
layout: page
title: Parkeren
permalink: /parkeren
---

Het is mogelijk om te parkeren bij de Space. Er zijn grofweg drie mogelijkheden:

1. Achter de PLUS Lammenschans via de Kanaalweg (Leiden zone B, gratis vanaf 19:30)
2. Onder de PLUS via de Lammenschansweg (betaald, 24/7)
(https://parkingleidenlammenschans.nl/parkeertarieven/)
3. Bij de GAMMA of Intersport voor de parkeerplaats (Leiden zone B, gratis vanaf 19:30)
(https://gemeente.leiden.nl/inwoners-en-ondernemers/parkeren/parkeren-op-straat-zones-
en-tarieven/)

![](/uploads/parkeren1.jpg)

![](/uploads/parkeren2.jpg)

![](/uploads/parkeren3.jpg)
