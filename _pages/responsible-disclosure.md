---
layout: page
title: Responsible disclosure
permalink: /responsible-disclosure
redirect_from: /en/responsible-disclosure
---
Bij Space Leiden vinden wij de veiligheid van onze systemen erg belangrĳk. Ondanks onze zorg voor de beveiliging van onze systemen kan het voorkomen dat er toch een zwakke plek is.

Als je een zwakke plek in één van onze systemen hebt gevonden horen wij dit graag zodat we zo snel mogelĳk maatregelen kunnen treffen. Wij willen graag met jou samenwerken om onze klanten en onze systemen beter te kunnen beschermen.

Wij vragen van jou:

- Je bevindingen te mailen naar [security@spaceleiden.nl](mailto:security@spaceleiden.nl). Versleutel je bevindingen met [onze PGP sleutel](/uploads/pubkey.asc) om te voorkomen dat de informatie in verkeerde handen valt,
- Het probleem niet te misbruiken door bijvoorbeeld meer data te downloaden dan nodig is om het lek aan te tonen of gegevens van derden in te kijken, verwijderen of aanpassen,
- Het probleem niet met anderen te delen totdat het is opgelost en alle vertrouwelijke gegevens die zijn verkregen via het lek direct na het dichten van het lek te wissen,
- Geen gebruik te maken van aanvallen op fysieke beveiliging, social engineering, distributed denial of service, spam of applicaties van derden, en
- Voldoende informatie te geven om het probleem te reproduceren zodat wij het zo snel mogelijk kunnen oplossen. Meestal is het IP-adres of de URL van het getroffen systeem en een omschrijving van de kwetsbaarheid voldoende, maar bij complexere kwetsbaarheden kan meer nodig zijn.

Wat wij beloven:

- Wij reageren binnen 3 dagen op jouw melding met onze beoordeling van de melding en een verwachte datum voor een oplossing,
- Als je je aan bovenstaande voorwaarden hebt gehouden zullen wij geen juridische stappen tegen jou ondernemen betreffende de melding,
- Wij behandelen jouw melding vertrouwelijk en zullen jouw persoonlijke gegevens niet zonder jouw toestemming met derden delen tenzij dat noodzakelijk is om een wettelijke verplichting na te komen. Melden onder een pseudoniem is mogelijk,
- Wij houden je op de hoogte van de voortgang van het oplossen van het probleem,
- In berichtgeving over het gemelde probleem zullen wij, indien je dit wenst, je naam vermelden als de ontdekker, en
- Als dank voor je hulp bieden wij een beloning aan voor elke melding van een ons nog onbekend beveiligingsprobleem. De grootte van de beloning bepalen wij aan de hand van de ernst van het lek en de kwaliteit van de melding.

Wij streven er naar om alle problemen zo snel mogelijk op te lossen en wij worden graag betrokken bij een eventuele publicatie over het probleem nadat het is opgelost.
