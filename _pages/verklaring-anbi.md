---
layout: page
title: Verklaring ANBI
permalink: /verklaring-anbi
redirect_from: /en/verklaring-anbi/
---
Hieronder vind je onze Verklaring van het Bestuur van de Stichting The Space Leiden
omtrent de vereisten van ANBI's.
Momenteel zijn wij als Stichting nog niet in het bezit van de ANBI-status. Deze pagina dient ter ondersteuning van onze aanvraag bij de Belastingdienst.

**Naam van de instelling**  
Stichting The Space Leiden

**RSIN**  
861919737

**Contactgegevens**  
Stichting The Space Leiden  
bestuur@spaceleiden.nl  
+31 (0)71 56 903 56  

**Bezoekadres**  
Bètaplein 28  
2321KS Leiden

**Doelstelling**  
De Stichting heeft als doel het bevorderen van technische ontwikkeling van
(jong)volwassenen en tracht dit doel te bereiken door het opzetten en beschikbaar
stellen van een technische openbare werkplek ("hackerspace" / "makerspace") aan deelnemers
met werkplaatsen waar plek is voor nieuwe technieken en het aanbieden van workshops
en cursussen. De Stichting heeft niet ten doel het maken van winst.

De belangrijkste doelen waar Space Leiden zich in de komende drie jaar op wil richten zijn:

- Een community opzetten die zich verenigt om te willen leren over verscheidene
onderdelen op het gebied van IT.
- Een zowel openbare als digitale ruimte beschikbaar stellen voor deze community
om meerdere keren per week samen te kunnen komen.
- Kennisdeling over IT te laten plaatsvinden.
- Verzamelen van en toegang verlenen tot specialistische apparatuur.

**Beleidsplan & Whitepaper**  
Zie [The Space Beleidsplan 2021](/beleidsplan) en [The Space Whitepaper 2020](/whitepaper).

**Bestuurssamenstelling**  
Het bestuur bestaat uit 7 leden, waaronder een voorzitter, penningmeester en secretaris.

**Belongingsbeleid**  
Alle bestuursleden zijn onbezoldigd.

**Verslag uitgeoefende activiteiten en financiële verantwoording**  
Space Leiden is opgericht op 2 december 2020 en heeft per haar Statuten nog geen jaarrekening gepubliceerd.
De eerste publicatie van deze stukken zal plaatsvinden na het aflopen van het eerste boekjaar op 31 augustus 2021.
