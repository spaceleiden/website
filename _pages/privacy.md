---
layout: page
title: Privacy policy
permalink: /privacy-policy
redirect_from:
  - /en/privacy-policy/
  - /privacy
---
{% capture intro %}
1. [Wie wij zijn](#wie-wij-zijn)
2. [Welke persoonsgegevens wij verwerken](#welke-persoonsgegevens-wij-verwerken)
3. [Met welk doel wij persoonsgegevens verwerken](#met-welk-doel-wij-persoonsgegevens-verwerken)
4. [Hoe lang we persoonsgegevens bewaren](#hoe-lang-we-persoonsgegevens-bewaren)
5. [Hoe wij persoonsgegevens beveiligen](#hoe-wij-persoonsgegevens-beveiligen)
6. [Cookies die wij gebruiken](#cookies-die-wij-gebruiken)

Dingen die wij niet doen:
- [Delen van persoonsgegevens met derden](#delen-van-persoonsgegevens-met-derden)
- [Automatische besluiten nemen](#automatische-besluiten)

Dingen die jij kan doen:
- [Je gegevens inzien, aanpassen of verwijderen](#gegevens-inzien-aanpassen-of-verwijderen)
- [Opt-out voor analytics en cookies](#analytics-opt-out)
- [Een beveiligingsprobleem melden](/responsible-disclosure)
{% endcapture %}
{% capture privacy_content %}
Dit is de pagina waar wij uitleggen welke persoonsgegevens wij verzamelen over jou
en je internetgedrag, waarmee we dit doen, hoe we deze gegevens beschermen, hoe we deze
gebruiken en hoe je deze gegevens kan inzien, wijzigen of verwijderen. Ook leggen
we hier uit welke rechten je hebt met betrekking tot jouw persoonsgegevens.

## Wie wij zijn
Stichting The Space Leiden is verantwoordelijk
voor de verwerking van persoonsgegevens zoals weergegeven
in deze privacyverklaring. Wij hebben een Privacy Officer die
bereikbaar via het onderstaande e-mailadres.

{:.table.table-sm.table-borderless.row}
| **Website** | [spaceleiden.nl](https://spaceleiden.nl) |
| **E-mail** | [privacy@spaceleiden.nl](mailto:privacy@spaceleiden.nl) |
| **Bezoekadres** | Bètaplein 28<br> 2321KS Leiden |

## Welke persoonsgegevens wij verwerken
The Space Leiden verwerkt je persoonsgegevens doordat je gebruik maakt van onze
diensten en/of omdat je deze gegevens zelf aan ons verstrekt. Hieronder vind je
een overzicht van de persoonsgegevens die wij verwerken, opgedeeld in vier
categorieen: website bezoekers, geregistreerde bezoekers, deelnemers en gevoelige
persoonsgegevens.

**Website bezoekers**
- Gegevens over jouw activiteiten op onze website zoals je IP-adres, browser
  versie en welke pagina's je hebt bezocht.

**Geregistreerde bezoekers**
- Voor- en achternaam
- Geboortedatum
- Telefoonnummer
- E-mailadres

**Deelnemers**

Voor deelnemers gelden de persoonsgegevens van de voorgaande twee categorieen plus het volgende:

- Bankrekeningnummer en naam rekeninghouder
- Overige persoonsgegevens die je actief verstrekt bijvoorbeeld door een profiel
op portal.spaceleiden.nl aan te maken, iets te bestellen of in schriftelijke correspondentie en telefonisch

**Gevoelige persoonsgegevens die wij verwerken**

The Space Leiden kan bijzondere en/of gevoelige persoonsgegevens van
jou verwerken. In het bijzonder kan het zijn dat wij gegevens van personen
jonger dan 18 jaar opslaan.

Onze website en diensten hebben niet de intentie gegevens te verzamelen over of van
websitebezoekers die jonger zijn dan 18 jaar, tenzij hij of zij toestemming
heeft van ouders of voogd. We kunnen echter niet controleren of een website
bezoeker ouder dan 18 is.

Wij raden ouders dan ook aan betrokken te zijn bij
de online activiteiten van hun kinderen, om zo te voorkomen dat er gegevens
over kinderen verzameld worden zonder ouderlijke toestemming.

Als je er van overtuigd bent dat wij zonder toestemming persoonlijke gegevens hebben
verzameld over of van een minderjarige, neem dan contact met ons op via
[privacy@spaceleiden.nl](mailto:privacy@spaceleiden.nl), dan verwijderen wij deze informatie.

## Met welk doel wij persoonsgegevens verwerken
The Space Leiden verwerkt jouw persoonsgegevens voor de volgende doelen.
Deze doelen noemen we 'grondslagen' omdat dat ook zo genoemd wordt in de wet
[AVG](https://autoriteitpersoonsgegevens.nl/nl/onderwerpen/algemene-informatie-avg/algemene-informatie-avg).

- Het afhandelen van eventuele betalingen en/of abonnementen die je met ons aangaat
- Het verzenden van onze nieuwsbrief en/of reclamefolder (opt-in)
- Om je te bellen, SMS'en of e-mailen indien dit nodig is om onze dienstverlening uit te voeren
- Om je te informeren over wijzigingen van onze diensten, producten en voorwaarden
- Om goederen en diensten bij je af te leveren als je deze besteld hebt
- Om onze website, servers en diensten te verbeteren en veilig te houden
- Voor bron- en contactonderzoek van de GGD voor de bestrijding van het coronavirus

The Space Leiden verwerkt ook persoonsgegevens als wij daar wettelijk toe
verplicht zijn, zoals betaalgegevens die wij nodig hebben voor onze belastingaangifte.

## Hoe lang we persoonsgegevens bewaren
The Space Leiden bewaart je persoonsgegevens niet langer dan strikt nodig is om
de doelen te realiseren waarvoor je gegevens worden verzameld.

Zolang jij gebruik maakt van de diensten van The Space Leiden bewaren wij jouw
verzamelde en opgegeven persoonsgegevens tot uiterlijk één jaar na beëindiging.

Verder moeten wij van bijvoorbeeld de Belastingdienst bepaalde betalingsgegevens
voor onze boekhouding tot maximaal zeven jaar bewaren.

Voor bron- en contactonderzoek van de GGD in verband met de COVID-19 pandemie
bewaren wij tot 30 dagen na jouw bezoek aan The Space Leiden je opgegeven
 contactgegevens. Na 30 dagen worden deze contactgegevens verwijderd.

## Hoe wij persoonsgegevens beveiligen
The Space Leiden neemt de bescherming van jouw gegevens serieus en neemt
passende maatregelen om misbruik, verlies, onbevoegde toegang, ongewenste
openbaarmaking en ongeoorloofde wijziging tegen te gaan.

Zo passen wij security-by-design toe bij het maken van onze software,
gebruiken wij industriestandaarden met betrekking
tot informatiebeveiliging en versleuteling van (virtuele) harde schijven en servers,
passen wij overal waar mogelijk tweestapsauthenticatie toe,
gebruiken wij sterke wachtwoorden en monitoren wij onze omgevingen geautomatiseerd
op onregelmatigheden.

Als jij het idee hebt dat jouw gegevens toch niet goed beveiligd zijn of er
aanwijzingen zijn van misbruik, neem dan contact op via
[security@spaceleiden.nl](mailto:security@spaceleiden.nl).

Mocht je een zwakke plek in één van onze systemen hebben gevonden, dan horen
wij dit graag. The Space Leiden heeft een Responsible Disclosure die je kan
lezen op [spaceleiden.nl/responsible-disclosure](/responsible-disclosure).

## Cookies die wij gebruiken
The Space Leiden gebruikt noodzakelijke, functionele en analytische cookies
en maakt geen gebruik van marketing cookies of cookies van derde partijen.

Een cookie is een klein tekstbestand dat bij het eerste bezoek aan onze website wordt opgeslagen in de browser van je computer, tablet of smartphone.

Wij gebruiken cookies met puur technische functionaliteit.
Deze zorgen ervoor dat de website naar behoren werkt en dat bijvoorbeeld jouw
voorkeursinstellingen onthouden worden.

Wij gebruiken ook cookies om de website goed te laten werken en deze te
kunnen verbeteren. Dat doen we doordat door het gedrag van je browser te volgen
op de website. Op deze website worden geen cookies geplaatst door of voor derden.
Wij hosten ons eigen analytics programma en delen deze gegevens met niemand.

Bij jouw eerste bezoek aan onze website hebben wij je al geïnformeerd over deze
cookies en hebben we je toestemming gevraagd voor het plaatsen ervan. [Je kunt je
afmelden](#analytics-opt-out) voor cookies door je internetbrowser zo in te stellen dat deze geen cookies meer opslaat of door [onze opt-out](#analytics-opt-out) te gebruiken. Daarnaast kun je ook alle informatie die eerder is opgeslagen via de instellingen van je browser verwijderen.

Zie voor een toelichting de website [veiliginternetten.nl](https://veiliginternetten.nl/themes/situatie/cookies-wat-zijn-het-en-wat-doe-ik-ermee/).

De volgende cookies kunnen door
jouw browser opgeslagen worden als je daar toestemming voor hebt gegeven en/of geen
opt-out verzoek hebt gestuurd wanneer je onze website bezoekt.

{:.table-responsive}
{:.table}
| Naam cookie | Functionaliteit | Levensduur |
| ----------- | --------------- | ---------- |
| `_pk_id.`   | Matomo gebruiker cookie | 1 jaar |
| `_pk_ses.`  | Matomo sessie cookie | tot einde browser-sessie |
| `matomo_ignore` | Matomo opt-out keuze | 1 jaar |
| `MATOMO_SESSID` | Functioneel voor bovenstaande | tot einde browser-sessie |
| `commentoCommenterToken` | Commento gebruiker cookie | 1 jaar |

Deze vier cookies worden door ons eigen analytics programma [Matomo Analytics](https://matomo.org/)
gemaakt en verwerkt. De vijfde cookie wordt aangemaakt als je gebruik maakt van
ons zelf gehoste reactie platform [Commento](https://commento.io/).

## Dingen die wij niet doen

### Delen van persoonsgegevens met derden
Wij verkopen of delen jouw gegevens **niet** aan derden.

Alleen wanneer dit nodig is voor de uitvoering van onze
overeenkomst met jou (als je een bestelling doet bijvoorbeeld, moet je adres naar PostNL) of om te voldoen aan een wettelijke verplichting.

Met bedrijven die jouw gegevens moeten verwerken (zoals onze website hoster en
e-mail provider), sluiten wij een bewerkers- of verwerkersovereenkomst af om
te zorgen voor eenzelfde niveau van beveiliging en vertrouwelijkheid van jouw gegevens. Wij blijven verantwoordelijk voor deze verwerkingen.

De volgende partijen verwerken persoonsgegevens die wij beheren:

{:.table-responsive}
{:.table}
| Partij | Soort dienst | Soort gegevens | Meer informatie |
| ------ | ------------ | -------------- | -------------- |
| Google Ireland Ltd | Google Workspace | E-mail, opslag | [Privacy Policy](https://workspace.google.com/learn-more/security/security-whitepaper/page-6.html) |
| Microsoft Nederland B.V. | Azure Cloud | Website hosting | [Privacy Policy](https://azure.microsoft.com/nl-nl/overview/trusted-cloud/privacy/) |
| Mollie B.V. | Payment provider | Betalingsgegevens | [Privacy Policy](https://www.mollie.com/nl/privacy) |
| Twilio Ireland Ltd | Sendgrid | Automatische e-mails | [Privacy Policy](https://sendgrid.com/policies/privacy) |
| Twilio Ireland Ltd | Twilio | Automatische SMSjes | [Privacy Policy](https://www.twilio.com/legal/privacy/shield) |
| Jortt B.V. | Boekhouding | Financiële gegevens | [Privacy Policy](https://www.jortt.nl/over-ons/privacybeleid/) |

De partijen waar wij onze gegevens opslaan hebben aangegeven geen toegang te hebben
tot de gegevens die wij daar opslaan. Deze partijen doorzoeken of verkopen deze gegevens ook niet. Verder slaan wij bepaalde persoonsgegevens versleuteld op waardoor alleen wij toegang hebben tot de inhoud.

### Automatische besluiten
The Space Leiden neemt **niet** op basis van geautomatiseerde verwerkingen besluiten
over zaken die (aanzienlijke) gevolgen kunnen hebben voor personen. Het gaat hier
om besluiten die worden genomen door computerprogramma's of -systemen, zonder dat
daar een mens (bijvoorbeeld een bestuurslid van The Space Leiden) tussen zit.

## Dingen die jij kan doen

### Gegevens inzien, aanpassen of verwijderen
Je hebt het recht om je persoonsgegevens in te zien, te corrigeren of te
verwijderen. Daarnaast heb je het recht om je eventuele toestemming voor de
gegevensverwerking in te trekken of bezwaar te maken tegen de verwerking van
jouw persoonsgegevens door ons en heb je het recht op gegevensoverdraagbaarheid.

Dat betekent dat je bij ons een verzoek kan indienen om de persoonsgegevens die
wij van jou hebben in een computerbestand naar jou of een ander, door jou
genoemde organisatie, te sturen.

Je kunt een verzoek tot inzage, correctie, verwijdering, gegevensoverdraging
van je persoonsgegevens of verzoek tot intrekking van je toestemming of bezwaar
op de verwerking van jouw persoonsgegevens sturen naar
[privacy@spaceleiden.nl](mailto:privacy@spaceleiden.nl).

Om er zeker van te zijn dat het verzoek door jou is gedaan, vragen
wij jou om een communicatiemiddel te gebruiken dat bij ons bekend is als
zijnde van jou. Wanneer dat niet mogelijk is of wij onze twijfels hebben
over de authenticiteit van het verzoek, kunnen wij je vragen om een kopie
van je identiteitsbewijs met het verzoek mee te sturen.

Wij willen je vragen in deze kopie je pasfoto, MRZ (machine readable zone,
de strook met nummers onderaan het paspoort), paspoortnummer en Burgerservicenummer
(BSN) zwart te maken. Dit is ter bescherming van je privacy. We raden je aan om
 gebruik te maken van een app zoals
[KopieID](https://www.rijksoverheid.nl/onderwerpen/identiteitsfraude/vraag-en-antwoord/veilige-kopie-identiteitsbewijs)
van de Rijksoverheid.

We reageren zo snel mogelijk op je verzoek en zorgen er voor dat je uiterlijk
binnen vier weken een antwoord hebt.
Indien wij meer tijd nodig hebben om je verzoek te verwerken, bijvoorbeeld
bij een uitgebreid verzoek, kunnen wij deze termijn met twee maanden verlengen.

Wij willen je er ook op wijzen dat je de mogelijkheid hebt om een
klacht in te dienen bij de nationale toezichthouder, de Autoriteit Persoonsgegevens.
Dat kan via de volgende link: [autoriteitpersoonsgegevens.nl/nl/contact-met-de-autoriteit-persoonsgegevens/tip-ons](https://autoriteitpersoonsgegevens.nl/nl/contact-met-de-autoriteit-persoonsgegevens/tip-ons)

### Analytics opt-out
Wij maken gebruik van het privacy-vriendelijke alternatief op Google Analytics:
[Matomo](https://matomo.org/).

Mocht je niet willen dat Matomo je volgt op onze website, dan kan
je met een browser extensie zoals [Ublock Origin](https://github.com/gorhill/uBlock)
of [Noscript](https://noscript.net/) dit uit zetten, of met de
[DNT-modus van je browser](https://allaboutdnt.com/) vragen om niet gevolgd te worden.

Ook kan je hier onder aangeven dat je niet door onze Matomo Analytics gevolgd wil worden.
Let wel op dat dit alleen voor de website van The Space Leiden werkt en andere websites
op het internet je nog wel kunnen volgen als je geen browser extensies hebt geinstalleerd.
{% endcapture %}
<div class="row">
  <div class="col-lg-8">
    {{ privacy_content | markdownify }}
    <iframe style="border: 0; height: 200px; width: 100%;" src="https://analytics.apps.spaceleiden.nl/index.php?module=CoreAdminHome&action=optOut&language=nl&backgroundColor=&fontColor=&fontSize=16px&fontFamily=Helvetica"></iframe>
  </div>
  <div class="col-lg-4">
    <div class="scroll-with">
      {{ intro | markdownify }}
    </div>
  </div>
</div>
