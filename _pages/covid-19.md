---
layout: page
title: Coronavirus maatregelen
permalink: /coronavirus-maatregelen
redirect_from:
  - /corona-maatregelen
  - /en/covid-19/
  - /covid-19
---
**Per 25 februari 2022 zijn er verschillende versoepelingen doorgevoerd door de overheid.**\
Hierdoor hebben we de onderstaande regels bovenop de bovenop [de bestaande maatregelen van de overheid](https://www.rijksoverheid.nl/onderwerpen/coronavirus-covid-19/algemene-coronaregels/kort-overzicht-coronamaatregelen):

<!--// more //-->
COVID Maatregelen:
- Met klachten blijf je thuis en laat je testen bij de GGD. Ook als je al gevaccineerd bent.

Liefs,\
Bestuur\
Space Leiden
