const showBanner = function() {
  let banner = document.createElement('div')
  banner.className = 'cookie-banner'
  banner.hidden = true
  banner.innerHTML = `
    <div class="card">
      <div class="card-body">
        <div class="container">
          <span>Wij gebruiken cookies om onze website te verbeteren en veilig te houden.</span>
          <a href="#" class="cookie-banner-agree btn btn-warning">OK</a>
          <a href="/privacy-policy#cookies-die-wij-gebruiken" class="cookie-banner-opt-out btn btn-link">Meer informatie</a>
        </div>
      </div>
    </div>
  `
  document.querySelector('body').prepend(banner)
  window.setTimeout(function () {
    document.querySelector('.cookie-banner').hidden = false
  }, 500)
  document.querySelector('.cookie-banner-agree').onclick = function(event) {
    window.localStorage.setItem('agree_cookies', true)
    document.querySelector('.cookie-banner').hidden = true
  }
}

window.addEventListener('load', function() {
  if (window.localStorage.getItem('agree_cookies') === null) {
    showBanner()
  }
})
