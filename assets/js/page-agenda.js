---
# Jekyll frontmatter so this file gets parsed.
---
let loadCalendar = function () {
    let calendarEl = document.getElementById('calendar')
    let calendar = new FullCalendar.Calendar(calendarEl, {
        locale: 'nl',
        initialView: 'listMonth',
        height: 'auto',
        headerToolbar: { end: 'prev,next' },
        listDayFormat: { weekday: 'long', day: 'numeric', month: 'long' },
        listDaySideFormat: false,
        googleCalendarApiKey: '{{ site.api.calendar.key }}',
        events: { googleCalendarId: '{{ site.api.calendar.id }}' }
    })
    calendar.render()
}

document.addEventListener('DOMContentLoaded', function() {
    loadCalendar()
})
