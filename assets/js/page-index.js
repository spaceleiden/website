---
# Jekyll frontmatter so this file gets parsed.
---

let loadCalendar = function () {
    let calendarEl = document.getElementById('calendar')
    let calendar = new FullCalendar.Calendar(calendarEl, {
        locale: 'nl',
        initialView: 'listMonth',
        height: 'auto',
        headerToolbar: { end: 'prev,next' },
        listDayFormat: { weekday: 'long', day: 'numeric', month: 'long' },
        listDaySideFormat: false,
        googleCalendarApiKey: '{{ site.api.calendar.key }}',
        events: { googleCalendarId: '{{ site.api.calendar.id }}' }
    })
    calendar.render()
}

let loadSpaceApi = async function () {
    const endpoint = '{{ site.api.space.endpoint }}'
    const r = await fetch(endpoint)
    .then((response) => {
      return response.json()
    })
    .then((response_obj) => {
      document.querySelector('#announcements .page-banner-default').style.display = 'none'
      if (response_obj.state.open == true) {
          let openBanner = document.querySelector('#announcements .page-banner-success')
          openBanner.querySelector('.banner-message').innerHTML = response_obj.state.message
          openBanner.style.display = 'block'
      } else {
          let closedBanner = document.querySelector('#announcements .page-banner-warning')
          closedBanner.querySelector('.banner-message').innerHTML = response_obj.state.message
          closedBanner.style.display = 'block'
          let onlineOnly = document.querySelector('#visit-online')
          onlineOnly.style.display = 'block'
          let openForVisits = document.querySelector('#visit-us')
          openForVisits.style.display = 'none'
      }
    })
    .catch((err) => {
      let defaultBanner = document.querySelector('#announcements .page-banner-default')
      defaultBanner.querySelector('.banner-message').innerHTML = '\
      <p></p><center>Oops, dat ging niet goed!<br>De status van de space kon niet worden opgehaald.</center><p></p>'
    })
}

document.addEventListener('DOMContentLoaded', function() {
    loadCalendar()
    loadSpaceApi()
})
