
dynamische website (laravel app?)
- deelnemersadministratie
- betalingssysteem
- archief talks, sheets en opnames
- reserveringssysteem

losse systemen:
- spaceapi.json status gekoppeld aan token systeem / mqtt endpoint / knopje
- google calendar caching

nice to haves:
- blogposts met 'scroll to top button'
- betere mobile navbar
- betere mobile cookiebanner
- privacy policy inhoudsopgave voor mobiel bovenaan pagina
