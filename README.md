# Website
The website spaceleiden.nl runs on a statically generated Jekyll website.
Jekyll uses markdown, HTML, SASS and Lyquid.

## Developing
To run this project locally you can either use docker or the shell commands below
suited for your OS. Clone the repository using git first.

```shell
mkdir -p spaceleiden/website
git clone git@gitlab.com:spaceleiden/website.git spaceleiden/website
cd spaceleiden/website
```

### Docker
You can use a pre-built docker image from jekyll:

- Install dependencies
```shell
docker run -v $(pwd):/srv/jekyll -p 4000:4000 -w /srv/jekyll -it jekyll/jekyll:4.2.0 bundle install
```
- Startup the server
```shell
docker run -v $(pwd):/srv/jekyll -p 4000:4000 -it jekyll/jekyll:4.2.0 jekyll serve
```

### Local installation
Commands below for Debian, Ubuntu, WSL and MacOS.

#### MacOS
For MacOS it's recommended to just use [Homebrew](https://formulae.brew.sh/formula/ruby).

```shell
brew install ruby
gem install bundler

# install dependencies locally to prevent pollution of your machine
bundle config set --local path 'vendor/bundle'
bundle install

# run jekyll on http://localhost:4000
bundle exec jekyll serve
```

#### Debian/Ubuntu/WSL
Commands below are suited for Debian or Ubuntu (works on WSL too).
Replace `apt` with your package manager of choice.

```shell
# remove package manager ruby versions
sudo apt purge ruby

# install dev dependencies
sudo apt install gnupg2

# install Ruby version manager RVM (https://rvm.io)
gpg2 --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
curl -sSL https://get.rvm.io | bash -s stable
source /home/user/.rvm/scripts/rvm

# install the latest ruby version and bundler
rvm install ruby --latest
gem install bundler

# install gems from Gemfile
bundle config set --local path 'vendor/bundle'
bundle install

# run the project
bundle exec jekyll serve
# > go to http://localhost:4000/ in your browser!
# (use --force_polling if using BashOnWindows or WSL)
```

## Deploying
Deploying the website to production works through
[GitLab CI/CD](https://docs.gitlab.com/ee/ci/) and
[GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).
See the `.gitlab-ci.yml` file for more information.
