---
title: Capture the flag
author: Sander
date: 2021-06-03
layout: blog
redirect_from:
  - /capture-the-flag/
  - /projects/capture-the-flag/
header: /uploads/ctf.jpg
---
Afgelopen februari heeft hackerspace The Space Leiden een maand lang online
een capture the flag evenement gehouden. Iedere dag zijn er twee nieuwe challenges online gekomen.

<!--// more //-->

Ons doel was om een zo breed mogelijk scala aan challenges te maken, dus niet
alleen security gerelateerde opdrachten, maar ook programming, forensics,
network security, reverse engineering, cryptography, osint en kennisvragen
over de security en engineering wereld.

Al deze challenges zijn samengekomen in een thema: de Big Kaas fabriek in Leiden.
Deze fabriek bleek nog al wat problemen te hebben met haar beveiliging. Gelukkig
waren de deelnemers van The Space er om de systeembeheerder Dave te helpen.

![](/uploads/ctf-impression.jpg)

En niet zonder succes! In een maand tijd hebben er 33 deelnemers hard gestreden
om de felbegeerde top-3 positie op het scoreboard. Halverwege de maand kwam daar
nog een onverwachte nieuwe persoon bij, die uiteindelijk nog als eerste is geeindigd ook!

![](/uploads/ctf-scoreboard.jpg)

In totaal zijn er 60 challenges gepubliceerd en zijn deze allemaal ten minste één
keer opgelost. In de top drie zijn uiteindelijk geeindigd: Erwin, Mattie en BusyR!
Nogmaals gefeliciteerd!

![](/uploads/ctf-winners.jpg)

De winnaars mochten een boek naar keuze bestellen. De nummer 1 kreeg ook nog
een limited edition Rian van Rijbroek muismat cadeau.

![](/uploads/ctf-prizes.jpg)

De CTF is overigens nogsteeds speelbaar. De prijzen zijn welliswaar al vergeven,
maar mocht je nog een plek zoeken om op een leuke manier te leren hoe CTFs werken,
of ben je een doorgewinterde CTF'er en wil je je skills up-to-date houden, neem
dan snel een kijkje op [ctf.spaceleiden.nl](https://ctf.spaceleiden.nl)!

![](/uploads/ctf-prizes-2.jpg)
