---
layout: blog
title: "Je Eigen Blog Hosten op Gitlab"
author: Niels
date: 2021-05-27
header: /uploads/blogging.jpg
---
Vandaag de dag is het als IT'er leuk om een blog bij te houden over de projectjes
en research die je doet. Niet alleen omdat je zo een portfolio bijhoudt wat interessant
zou kunnen zijn voor een potentiële werkgever. Maar het is ook leuk om zo nu en dan
eens terug te kijken op wat je de afgelopen jaren gedaan hebt.

De meest populaire software om een blog mee te hosten vandaag de dag is Wordpress.
Zelfs de website van The Space draait er op al zijn we bezig met het migreren naar
een nieuw systeem. Mensen die al eens een blog van mij gelezen hebben weten dat ik
niet fan ben van Wordpress, voornamelijk door het grote aantal beveiligingslekken
en de spaghetti code die erachter schuil gaat. Maar zoals de meeste mensen graag roepen:
"Commentaar hebben is beter kunnen!", nou bij deze!

## Structuur
Veel blogs worden gegenereerd aan de hand van een stukje software (vaak Wordpress)
die de content uit een database haalt. Dit betekent dat iedere keer dat iemand jouw
pagina opvraagt (aannemende dat er geen chache tussenhangt) er een connectie met
de database gemaakt moet worden om een blogpost op te halen die zeer waarschijnlijk
altijd hetzelfde blijft. Dit is een hoop overhead voor een simpel blogje, je hebt:

- een database
- een webserver
- een PHP interpreter
- een Wordpress installatie die dat allemaal aan elkaar knoopt

Kan dit simpeler? Ja! Komt op via de linkerzeide van het podium: Hugo!

Hugo is een zogenoemde "statische website generator", heel simpel betekent dit dat
je een zooi aan bestanden aan Hugo geeft. Die bakt er vervolgens een statische
website van met platte HTML, die is vervolgens te hosten op iedere webserver.

Dit zorgt ervoor dat men geen database of CMS nodig heeft om de boel in op te slaan.
 We spelen echter een klein beetje vals, dit komt door het feit dat we Gitlab als
 een soort database gebruiken.

De workflow is als volgt:
1. Je wil een blogpost toevoegen aan je website.
2. Je schrijft de blogpost in je favoriete formaat (HTML, Rst, Txt, Markdown, etc).
3. Je test de boel lokaal en kijkt of je het mooi vindt.
4. Je commit de blogpost naar je website repository.
5. Gitlab start een pipeline die de met alle input files je statische website genereert.
6. Gitlab is lief genoeg om deze statische files voor je te hosten zonder kosten.
7. Gitlab pakt de statische files uit deze pipeline, en updatet je website automagisch.

Oftewel zodra we de boel hebben ingericht hoef je enkel een blogpost te schrijven en
die te comitten. Gitlab updatet alles vanzelf en jij hoeft niet om te kijken naar
de server, database of Wordpress.

## De Praktijk
Gebruik [deze](https://gohugo.io/getting-started/installing) pagina om Hugo te
installeren op jouw systeem, dit gebruiken we om lokaal
naar onze website te kunnen kijken.

Maak een Gitlab account aan als je dit nog niet gedaan hebt, het is mogelijk omdat
met GitHub actions te doen maar daar heb ik geen ervaring mee. Wellicht leuk uitzoek
werk, heb je gelijk iets om op je nieuwe blog te posten ;).

Eenmaal ingelogd op Gitlab maken we een nieuw project aan en kiezen we voor "Create from template".

![Screenshot van de Gitlab New Project pagina](/uploads/blog-hugo-new-project.png)

*Zoek in de lijst naar Pages/Hugo en druk op "Use template":*

![Screenshot van de Gitlab template pagina](/uploads/blog-hugo-template-hugo.png)

*Je kan nu een naam maken voor je project (dit wordt niet per sé de naam van je blog), klik op "Create project":*

![Screenshot van de Gitlab project creatie pagina](/uploads/blog-hugo-create-project.png)

*Er staat nu een voorbeeld project in je Gitlab repository, die kun je gebruiken om dingen aan te passen.*

Nu we het skelet hebben staan kunnen we dingen gaan aanpassen! Aangezien we Gitlab
nu voor ons een repository hebben laten maken moeten we deze clonen. Als je voor
Git een GUI gebruikt kan je het via die weg doen,
aangezien ik een nerd met een baard ben gebruik ik de command line:

```bash
# De URL van jouw repo kan je vinden op de gitlab repository
git clone git@gitlab.com:nielsvangijzen/mijn-vette-blog.git
```

Nu we onze repository lokaal hebben staan is het tijd voor het leukste onderdeel,
we gaan shoppen voor een leuk thema.
Het is ook zeker mogelijk om zelf een thema te ontwerpen, maar als we briljante
web developers waren hadden we wel een over de top
nodejs stack met een zware mongodb omdat we 2 JSON bestandjes op willen slaan (/rant).

Op [themes.gohugo.io](https://themes.gohugo.io/)
kunnen we kijken naar thema's die ons aanspreken. Laten we voor het voorbeeld
[Anatole](https://themes.gohugo.io/anatole/) pakken.
Bijna alle Hugo themes worden geïnstalleerd door de Git repository toe te voegen
aan de themes map. Dit kan op 2 manieren, je kan de
repository downloaden en in de themes map zetten. Maar je kan ook de repository
toevoegen als submodule in je eigen repository. Dit laatste
heeft het voordeel dat je van tijd tot tijd het thema kan updaten!

Om het thema toe te voegen aan je project kan je het volgende commando uitvoeren
in de root van je repository:

```bash
git submodule add https://github.com/lxndrblz/anatole.git themes/anatole
```

Door het bestand `config.toml` aan te passen kunnen we nu zeggen dat we van het
Anatole thema gebruik willen maken:

```toml
baseurl = "https://pages.gitlab.io/hugo/"
contentdir    = "content"
layoutdir     = "layouts"
publishdir    = "public"
title = "Beautiful Hugo"
canonifyurls  = true

DefaultContentLanguage = "en"
theme = "anatole" # <------- Pas deze regel aan
metaDataFormat = "yaml"
pygmentsUseClasses = true
pygmentCodeFences = true

```

Er zijn een hoop instellingen die je kan aanpassen dus daar ga ik niet allemaal
op in in deze blog. Op de webpagina van het thema staat er per instelling wat het
doet en wat je er aan hebt!

Het belangrijkst is momenteel het schrijven van je eerste blogpost, in de
`content/post` map komen je blogposts te staan:

![Screenshot van de content/post map](/uploads/blog-hugo-project-tree.png)

De bestaande blogposts geven je een voorbeeld van hoe de structuur van een markdown
bestand er uit ziet. De rest van het formaat wordt geregeld door Hugo!
Je kan nu de huidige versie comitten en pushen naar Gitlab:
```bash
git add .
git commit -m "Ik heb het anatole thema toegevoegd aan mijn Hugo blog"
git push
```

Gitlab zal hierdoor een pipeline starten om jouw website te bouwen, dit kan je
zien op Gitlab onder het kopje "CI/CD > Pipelines"

![Screenshot van de Gitlab CI/CD pagina](/uploads/blog-hugo-gitlab-cicd.png)

Om nu naar je website te gaan kan je bij Gitlab drukken op "Settings > Pages"
en kijken naar het kopje "Access pages". Daar staat de
url van je website:

![Screenshot van de Gitlab Pages pagina](/uploads/blog-hugo-gitlab-pages.png)

Op deze pagina kan je door op de knop "New Domain" te drukken ook je eigen
domeinnaam toevoegen aan jouw blog. Gitlab vraagt dan
automagisch een SSL certficaat voor je aan!

Zie voor meer informatie de volgende bronnen:

1. [Gitlab Docs: Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/)
2. [Gitlab Docs: CI for Pages](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html)
3. [Gitlab Docs: Custom domain for Pages](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/)

Lukt er iets niet of wil je laten zien wat je gebouwd hebt? Kom gezellig langs
op onze [Discord]({{ site.socials.discord }})!
