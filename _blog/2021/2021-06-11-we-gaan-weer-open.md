---
layout: blog
title: We gaan weer open!
author: Raoul
redirect_from: /opening
header: /uploads/opening_header.jpg
---
Per 5 juni zijn er verschillende versoepelingen doorgevoerd door de overheid.
Hierdoor kunnen wij jullie met trots melden dat we vanaf volgende donderdag
(17 juni) weer fysiek open gaan.

Haal je projecten uit het stof, koop je nieuwe materialen in, we kunnen weer aan
de slag! We zullen fysiek aanwezig zijn op onze locatie bij Technolab. Jullie
zijn welkom van 19:00 tot 22:30 uur om te komen werken aan jullie eigen projecten.

Wel gelden er nog een aantal basis regels, [lees ze hier](/covid-19).

Betreffende onze talks en workshops zal er een kleine verandering plaatsvinden.
Vanaf volgende week zullen we deze om de week gaan geven. Dit doen we om mensen meer de
ruimte te geven om te werken aan hun eigen projecten. Dit houdt in dat de talk
"Fun with Numbers" met een week verplaatst is naar 24 juni en dat de
workshop "Donuts maken in Blender" 8 juli plaats zal vinden.

Mochten jullie vragen hebben dan horen wij dit graag via [onze mail](/contact)
of via [Discord]({{ site.socials.discord }}). Zien we jullie volgende week?

Liefs,
Raoul namens Team The Space

![opening_2021.jpg](/uploads/opening_2021.jpg)
