---
layout: blog
title: "The Space, Fysieke lol tot digitale madness"
author: Raoul
date: 2021-05-02
header: /uploads/space_laser.jpg
redirect_from:
    - /news/the-space-fysieke-lol-tot-digitale-madness/
    - /en/news/the-space-fysieke-lol-tot-digitale-madness/
---
Op 2 mei 2021 bestaat Stichting The Space Leiden officieel pas een half jaar. Maar eigenlijk waren we hiervoor al een tijd bezig. Het idee van The Space begon een paar jaar geleden. Ikzelf, Raoul, toen nog een student informatica aan de hogeschool Leiden, miste iets. Ik miste een plek waar men aan eigen projecten kon werken, waar men gelijkgestemden kon vinden, waar men met gereedschap kon werken die te groot, te duur of te specialistisch zijn voor de gemiddelde student in een kleine kamer.

Na het pitchen van dit idee aan wat vrienden van de opleiding, kwam ik erachter dat dit toch wel een idee was waar meer mensen wel wat in zagen. En zo ontstond er een groepje gelijkgestemden. Een groep mensen die vandaag de dag de kern zijn van The Space. Het bestuur.

Na vele plannen, enquêtes en beleidsplannen gingen wij op zoek naar manieren om een community te vormen en een plek om aan onze projecten te werken. In Leiden was al een Makerspace maar deze deed alles toch net iets anders dan hoe wij het in gedachten hadden.

Wij wilden toch meer dan alleen een plek om dingen te maken. Een makerspace draait, zoals het woord “maker” zegt, om het kunnen maken van verschillende producten. Hier kun je werken aan fysieke producten zoals een eigen designlamp, elektronica of werken met gereedschappen zoals 3D printers. Er is toegang tot verschillende gereedschappen en men deelt kennis met elkaar.

In het geval van een hackerspace hoeft dit niet zozeer te gaan om het maken van iets fysieks, maar kan dit bijvoorbeeld gaan om het maken van programma's, knutselen met verschillende technologieën of het zoeken van "mogelijkheden" met bestaande toepassingen. Dit laatste is iets wat beter past bij oud informatica studenten, het liefste wilden we een combinatie van de twee.

Een eigen plekje, waar wij en gelijkgestemden aan onze projecten konden werken, met 3D printers mogelijkheden tot server ruimte en gespecialiseerde tools voor het werken met kleine elektronica of het werken met verschillende technieken. Een ruimte waar wij ons eigen ding konden doen, van gezelligheid tot talks, ruimte voor ieder wat wils.

Afgelopen zomer hadden we in dat opzicht een grote meevaller, een plek waar wij ondanks de Corona gekheid, fysiek aan de gang konden. De ruimte die ons beschikbaar werd gesteld is van Technolab, een stichting in Leiden gericht op het interesseren van jongeren in natuur, techniek en wetenschap. Technolab gebruikt hun ruimte overdag voor het organiseren van workshops en projecten voor basisscholen en het voortgezet onderwijs. In de avond staat deze ruimte echter leeg.

De ruimte van Technolab is groot, heeft verschillende gereedschappen zoals 3D printers, verscheidene houtbewerking tools, gereedschap om te werken met kleine elektronica en zelfs een heuse lasersnijder! Technolab gaf ons de mogelijkheid om hier zeker één avond in de week fysieke evenementen te organiseren.

Dit is voor een beginnende hackerspace met eigenlijk vrij weinig tools of fondsen natuurlijk een grote stap. Zo’n kans konden we dus niet laten liggen. Vanaf september begonnen we, met een limiet van 30 mensen op een avond hadden we regelmatig een drukke avond. Iedere donderdagavond een workshop of talk.

![](/uploads/opening_poster.jpg)

Workshops zoals het maken van een AM radio, Blender, Indesign en werken met verscheiden vormen van hardware was mogelijk en naast deze workshops en talks, kwamen er regelmatig mensen om te werken aan eigen projecten.

Zo zijn we toch bijna 6 fysiek weken open geweest voordat het noodlot toesloeg. Nieuwe maatregelen werden aangekondigd en het land ging op slot, en zo ook wij. Naar onze mening was openblijven niet verantwoord, men moest reizen om naar The Space toe te komen en het was toch weer een fysiek moment extra. Om die reden gingen wij digitaal verder. Onze fysieke donderdagen, werden digitale donderdagen.

Ondanks dat dit alles wat lastiger ging dan fysieke evenementen hielden we de moed erin. Inmiddels zijn we een half jaar digitaal verder en nog steeds digitaal bezig. Toch blijven de donderdagen één en al gezelligheid, gepaard met een workshop of talk.

We zijn ons voor deze talks meer gaan richten op digitale technieken omdat die toch wat beter naar voren komen in een digitale setting. We gaven talks over verschillende IoT protocollen, UnRaid, de Telecom wereld en Pentesten. Nog steeds kwamen mensen naar onze talks en bleef het gezellig.

Wij zijn nog steeds erg enthousiast over onze plannen voor The Space. We gaan door met het plannen voor de toekomst. We hopen dan ook dat we snel weer met jullie fysiek aan de gang kunnen.

Ben je nog nooit bij ons langs geweest? Neem eens een kijkje op onze Discord en kom op de donderdagen eens buurten!

Hopelijk tot snel en anders tot digitaal.

Liefs,
Raoul Vos
Voorzitter
Stichting The Space Leiden
