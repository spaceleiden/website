---
layout: blog
title: The Space Leiden gaat weer online!
author: Raoul
redirect_from: /sluiting-12-11-2021
header: /uploads/closing_header.png
---
Momenteel is The Space Leiden fysiek gesloten en zijn al onze activiteiten online.
Per 12 november zijn er verschillende [nieuwe maatregelen](https://www.rijksoverheid.nl/onderwerpen/coronavirus-covid-19/algemene-coronaregels/kort-overzicht-coronamaatregelen) doorgevoerd door de overheid.

<!--// more //-->

Door deze nieuwe maatregelen is het voor The Space momenteel helaas niet mogelijk om open te blijven.
Onze openingstijden liggen grotendeels na de verplichte sluitingstijd en daarom zullen wij per direct (komende donderdag 18 november) overgaan op digitale talks/ workshops op onze [Discord]({{ site.socials.discord }}).

We snappen dat het jammer is dat we dicht moeten.
Mocht je verdere vragen hebben neem dan contact op met [bestuur@spaceleiden.nl](mailto:bestuur@spaceleiden.nl).

Hopelijk zien we jullie allemaal snel voor wat (digitale) gezelligheid.

Liefs,
Bestuur
The Space Leiden
