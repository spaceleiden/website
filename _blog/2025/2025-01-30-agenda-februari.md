---
title: Activiteiten Februari 2025  
author: Raoul  
date: 2025-01-22  
layout: blog  
redirect_from:  
- /februari-2025/  
header: /uploads/space_laser.jpg
---
De agenda voor februari is bekend! Deze maand hebben we een mix van creatieve, spannende en uitdagende activiteiten, van een D&D one-shot tot Hack The Box en de realisatie van onze Break-in-Box. Lees verder voor meer informatie!  
(English below)

<!--// more //-->

### 06 februari - Projectavond
De eerste donderdag van de maand is gereserveerd voor een projectavond. Werk verder aan je eigen project, krijg feedback van anderen of help iemand met hun idee. Of je nu bezig bent met elektronica, softwareontwikkeling of iets creatiefs, er is altijd ruimte om samen te werken en kennis te delen!

<img src="/uploads/2023/raspberry_pi_zero.jpeg" alt="Projectavond" style="width:600px"/>

### 13 februari - D&D Valentijns One-Shot
Op Valentijnsdag gaan we op avontuur in een speciale Dungeons & Dragons one-shot! Of je nu een doorgewinterde speler bent of voor het eerst een dobbelsteen rolt, deze romantische, hilarische en soms chaotische sessie is voor iedereen. Sluit je aan en ontdek wat Cupido voor jou in petto heeft in de wereld van fantasy en avontuur!
Schrijf je in via https://forms.gle/PKAKQQue4W4qrrFN6! 

<img src="/uploads/2023/dnd_banner.png" alt="D&D Valentijns One-Shot" style="width:600px"/>

### 20 februari - Hack The Box
Deze avond duiken we in Hack The Box! Test je ethical hacking skills in een uitdagende en interactieve omgeving. Of je nu een beginner bent of al ervaring hebt, er zijn genoeg uitdagingen op elk niveau. Kom langs en leer hoe je systemen analyseert, kwetsbaarheden vindt en problemen oplost – allemaal op een legale en educatieve manier!

<img src="/uploads/2023/hackthebox.png" alt="Hack The Box" style="width:600px"/>

### 27 februari - Break-in-Box Avond - Realisatie
Na het brainstormen en puzzelen in de vorige sessies is het tijd om de Break-in-Box echt te bouwen! We gaan de fysieke puzzels en mechanismen samenstellen en testen. Help mee met ontwerpen, bouwen en finetunen van de ultieme escape-ervaring. Samen maken we er iets unieks van!

<img src="/uploads/2023/hw_hackaton.png" alt="Break-in-Box Realisatie" style="width:600px"/>

# Programme for February!

## February 6 - Project Evening
The first Thursday of the month is dedicated to a project evening. Continue working on your own project, get feedback from others, or help someone with their idea. Whether you’re working on electronics, software development, or something creative, there’s always room to collaborate and share knowledge!

<img src="/uploads/2023/raspberry_pi_zero.jpeg" alt="Projectavond" style="width:600px"/>

## February 13 - D&D Valentine’s One-Shot
On Valentine’s Day, we embark on a special Dungeons & Dragons one-shot adventure! Whether you're a seasoned player or rolling dice for the first time, this romantic, hilarious, and sometimes chaotic session is for everyone. Join us and see what Cupid has in store in the world of fantasy and adventure!
Register via https://forms.gle/PKAKQQue4W4qrrFN6!

<img src="/uploads/2023/dnd_banner.png" alt="D&D Valentijns One-Shot" style="width:600px"/>

## February 20 - Hack The Box
This evening, we dive into Hack The Box! Test your ethical hacking skills in a challenging and interactive environment. Whether you’re a beginner or an experienced hacker, there are challenges for every level. Come by and learn how to analyze systems, find vulnerabilities, and solve problems – all in a legal and educational way!

<img src="/uploads/2023/hackthebox.png" alt="Hack The Box" style="width:600px"/>

## February 27 - Break-in-Box Night - Build & Realization
After brainstorming and puzzling in previous sessions, it’s time to bring the Break-in-Box to life! We will assemble and test the physical puzzles and mechanisms. Help design, build, and fine-tune the ultimate escape experience. Let’s create something truly unique together!

<img src="/uploads/2023/hw_hackaton.png" alt="Break-in-Box Realisatie" style="width:600px"/>
