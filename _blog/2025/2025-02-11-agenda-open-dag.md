---
title: Internationale Open Hackerspace Dag - 29-03-2025
author: Raoul
date: 2025-02-10
layout: blog
redirect_from:
  - /open-dag/
  - /open-dag-25/
  - /blog/open-dag-25/
header: /assets/img/workshop.jpg
---
Ook dit jaar doet Space Leiden mee met de Open Hackerspace Dag 2025.
Sinds 2012 is er ieder jaar open dag voor hackerspaces, eerst alleen in Nederland en de laatste paar jaar ook internationaal.
De open dag is altijd de laatste zaterdag van maart, voor 2025 zal dit 29 maart zijn.

<!--// more //-->

Hackerspaces zijn locaties waar mensen met een technische, creatieve en nieuwsgierige mindset bijeenkomen.
Ze komen daar om te werken aan projecten, elkaar te ontmoeten en te praten over onderwerpen die hen interesseren.

Tijdens de open dag is het mogelijk om zelf te ontdekken wat een hackerspace inhoudt en te zien wat er allemaal mogelijk is.
Tipje van de sluier? Iemands verbeeldingskracht is de meest beperkende factor!

Iedereen is welkom om een kijkje te komen nemen tijdens deze jaarlijks terugkerende open dag wij zullen open zijn van 10:00 tot 17:00.
Voor meer informatie over de Hackerspaces die hieraan mee doen kijk op [Hackerspaces.nl](https://hackerspaces.nl/open-dag/).

Op onze opendag doen we verschillende demonstraties en talks om te laten zien wat we hier allemaal doen.

**Tijdens de open dag bieden we demonstraties om te laten zien wat we zoal doen bij onze space.**

| *  | Type        |   | Beschrijving                                      |   | Door         |   | Tijd  |
|----|-------------|---|---------------------------------------------------|---|--------------|---|-------|
| 1. | TALK        |   | Wat doen we bij de hackerspace                    |   | Raoul V.     |   | 12:00 |
| 2. | DEMO        |   | Blender voor 3D modellen                          |   | Dion vd B.   |   | 13:00 |
| 3. | DEMO        |   | 3D ontwerpen                                      |   | Jelle W.     |   | 14:00 |
| 4. | Talk / DEMO |   | CTF - wat is het en hoe pak ik een challenge aan  |   | Niels van G. |   | 15:00 |

Naast de demos is er tijdens de opendag ruimte om te komen werken aan eigen projecten of een keertje te kijken hoe de lasersnijder of 3D printer werkt.
Wil je aan de slag met de CTF, de lasersnijder of een van de 3D printers? Neem vooral je eigen laptop mee!

Space Leiden is een hackerspace in Leiden. We zijn een plek waar iedereen kan werken, vergaderen, maken, onderzoeken of gewoon gezellig kan zijn.
Met gereedschap, 3D printers, servers en meer. Apparatuur die te groot, te duur of te specialistisch is om thuis te hebben.

Momenteel zijn wij gevestigd in het gebouw van [Stichting Technolab](https://www.technolableiden.nl/) en MBO Rijnland, vlak achter Station Lammenschans.
Onze locatie zit in de ruimte van Technolab Leiden boven de Plus (Bètaplein 28, Leiden).
Als je voor de Plus staat en aan je linkerhand de trap op loopt dan zul je vanzelf de ingang zien.
Normaliter zijn we iedere donderdag open tussen 19:00 en 22:30.

Onze doelgroep is voornamelijk gericht op 16+, ben je jonger dan dit en wil je graag komen? Dan vragen we je om een volwassen begeleider mee te nemen.

Kom je ook op de opendag of kom je een keer kijken op donderdagavond?

