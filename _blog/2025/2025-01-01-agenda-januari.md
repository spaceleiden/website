---
title: Activiteiten januari 2025
author: Raoul
date: 2025-01-01
layout: blog
redirect_from:
  - /januari-2025/
header: /uploads/ctf.jpg
---
De agenda voor januari is bekend! We starten het nieuwe jaar met een gezellige nieuwjaarsborrel, een technische NUC-workshop, creatief bouwen en uitdagende puzzels. Lees verder voor meer informatie!
(English below)

<!--// more //-->

### 09 januari - Nieuwjaarsborrel / Projectavond
We luiden het nieuwe jaar in met een gezellige nieuwjaarsborrel gecombineerd met een projectavond! Proost met ons op een nieuw jaar vol creativiteit en samenwerking. 
Neem je lopende project mee of start iets nieuws, onder het genot van een drankje en goede gesprekken.

<img src="/uploads/2024/nieuwjaarsborrel-proost.jpg" alt="Nieuwjaarsborrel en Projectavond" style="width:600px"/>

### 16 januari - NUCs Workshop
We hebben maar liefst 14 NUCs (Next Unit of Computing) gedoneerd gekregen, en dat vraagt om een workshop! 
Tijdens deze avond duiken we in de wereld van Proxmox en ontdekken we hoe je je eigen homelab kunt bouwen. V
an netwerken en een VPN-server met PFsense, tot smart home-oplossingen met Home Assistant, multimedia met Plex, gaming of zelfs AI-toepassingen. 
De cloud – maar dan lokaal bij jou thuis – is de limit!

Na afloop van de workshop zullen we een deel van de NUCs in de verkoop doen voor een zacht prijsje. Terwijl we de rest beschikbaar stellen voor projecten op de Space.

<img src="/uploads/2025/nucs_workshop.png" alt="NUCs Workshop" style="width:600px"/>

### 23 januari - Vogelhuisje bouwen
Creativiteit en techniek komen samen tijdens onze vogelhuisjes-bouwavond! Of je nu een klassiek houten huisje maakt, of er slimme technologie in integreert 
– alles is mogelijk. Een leuke activiteit voor iedereen die graag iets tastbaars maakt.

<img src="/uploads/2025/vogelhuisje.png" alt="Vogelhuisje bouwen" style="width:600px"/>

### 30 januari - Break-in-Box Avond 2
Na het succes van onze eerste Break-in-Box avond, keren we terug met een vervolg! Dit keer staan fysieke puzzels en uitdagend denkwerk centraal. 
Werk samen, kraak de codes, en open de box. Wees voorbereid op een avond vol spanning en plezier!

<img src="/uploads/2024/escapebox.png" alt="Break-in-Box Avond 2" style="width:600px"/>

# Programme for January!

## January 9 - New Year’s Reception / Project Evening
We kick off the new year with a cozy New Year’s reception combined with a project evening! Join us to toast to a creative and collaborative year ahead. Bring your ongoing project or start something new while enjoying drinks and good company.

<img src="/uploads/2024/nieuwjaarsborrel-proost.jpg" alt="New Year's Reception and Project Evening" />

## January 16 - NUCs Workshop
We’ve received a generous donation of 14 NUCs (Next Unit of Computing), and it’s time to put them to use! In this workshop, we’ll dive into Proxmox and explore how to build your own homelab. From networking and a VPN server with PFsense, to smart home setups with Home Assistant, multimedia with Plex, gaming, or even AI. The cloud – but locally at your place – is the limit!

<img src="/uploads/2025/nucs_workshop.png" alt="NUCs Workshop" />

## January 23 - Build a Birdhouse
Creativity and craftsmanship combine in our birdhouse-building evening! Whether you create a classic wooden birdhouse or integrate some smart technology – the possibilities are endless. A fun and hands-on activity for all!

<img src="/uploads/2025/vogelhuisje.png" alt="Build a Birdhouse" />

## January 30 - Break-in-Box Night 2
After the success of our first Break-in-Box night, we’re back with a sequel! This time, physical puzzles and challenging brainteasers take center stage. Work together, crack the codes, and open the box. Prepare for an evening full of excitement and fun!

<img src="/uploads/2024/escapebox.png" alt="Break-in-Box Night 2" />
