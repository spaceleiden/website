---
title: Activiteiten Maart 2025  
author: Raoul  
date: 2025-02-11  
layout: blog  
redirect_from:  
- /maart-2025/  
header: /uploads/ctf.jpg
---
De agenda voor maart is bekend! Deze maand staat in het teken van onze jaarlijkse Capture The Flag (CTF). Lees verder voor meer informatie!  
(English below)

<!--// more //-->

### 06 maart - Projectavond
De eerste donderdag van de maand is gereserveerd voor een projectavond. Werk verder aan je eigen project, krijg feedback van anderen of help iemand met hun idee. Of je nu bezig bent met elektronica, softwareontwikkeling of iets creatiefs, er is altijd ruimte om samen te werken en kennis te delen!

<img src="/uploads/2023/raspberry_pi_zero.jpeg" alt="Projectavond" style="width:600px"/>

### 13 maart - CTF Opening
Op 13 maart starten we onze CTF, dit jaar doen we dit iets anders dan andere jaren. We zullen niet meer iedere dag 1 challenge online brengen om 13:37. 
In plaats daarvan brengen we iedere donderdag om 20:00 vijf nieuwe challenges online.
Het niveau is zoals ieder jaar beginner tot gevorderd. De moeilijkheid wordt geleidelijk aan hoger.
Aan deze challenges kan je de hele week werken. Bij de Space kan je natuurlijk meteen aan de slag, vragen stellen en met elkaar werken om de oplossing te vinden.

De CTF loopt van 13 maart tot en met 3 april. Op 10 april zullen we de winnaars bekend maken en de prijzen uitreiken wederom om 20:00.
Deze donderdag doen we daar gelijk een gezellige kickoff bij. Zien we je dan?

<img src="/uploads/2025/dating_ctf.jpeg" alt="CTF... its a match!" style="width:300px"/>

### 20 maart - CTF / Project avond
Tijdens deze avond is er ruimte om gezellig klaar te zitten voor de nieuwe CTF-challenges van deze week! Ben jij er klaar voor?
Uiteraard kan je ook gewoon komen werken aan eigen projecten. De Space is open voor iedereen! 
Pak je breakout box erbij, werk aan kleine electronica, slinger de 3D printer aan of ga aan de slag met de lasersnijder. Tot dan!

<img src="/uploads/2023/hw_hackaton.png" alt="Break-in-Box Realisatie" style="width:600px"/>

### 27 maart - CTF / Meetup Python Leiden User Group
Ook deze donderdag kan je aan de slag met de CTF-challenges van deze week. Ben jij al bij? Heb je vragen? Kom langs!

Deze avond hosten we ook de heren van [Python Leiden User Group](https://pythonleiden.nl/meeting-2025-03-27.html). Zij zullen een meetup houden over een interessante onderwerpen.
De talks bij deze meeting zijn in het Engels. De planning is als volgt:

- Talk 1: **Serialization in Python including hands-on workshop** by _John Labelle_
- Talk 2: **How to use uv for dependency management** by _Michiel Beijen_ (this talk was originally planned for the first meetup but got moved over because of time constraints)
- Talk 3: **Rendering spatial data in 3d using zarr, jax, babylon.js and DuckDB** by _Jesse Vanderhees_

<img src="/uploads/2025/pythonleidensquare.svg" alt="Python Leiden User Group" style="width:300px"/>

### 29 maart - Internationale Hackerspace Open dag
Ook dit jaar doet Space Leiden mee met de Open Hackerspace Dag 2025.
Sinds 2012 is er ieder jaar open dag voor hackerspaces, eerst alleen in Nederland en de laatste paar jaar ook internationaal.
De open dag is altijd de laatste zaterdag van maart, voor 2025 zal dit 29 maart zijn.

Meer informatie? -> [Internationale Open Hackerspace Dag - 29-03-2025](/open-dag/)

# Programme for March!
### March 6 - Project Night
The first Thursday of the month is reserved for a project night. Continue working on your own project, get feedback from others, or help someone with their idea. Whether you're working on electronics, software development, or something creative, there's always room to collaborate and share knowledge!

<img src="/uploads/2023/raspberry_pi_zero.jpeg" alt="Projectavond" style="width:600px"/>

### March 13 - CTF Opening
On March 13, we kick off our CTF, and this year we're doing things a little differently. Instead of releasing one challenge per day at 13:37, we will now release five new challenges every Thursday at 20:00.

As always, the difficulty level ranges from beginner to advanced, gradually increasing over time. You can work on the challenges throughout the week. At the Space, you can get started right away, ask questions, and collaborate to find solutions.

The CTF runs from March 13 to April 3. On April 10, we will announce the winners and hand out prizes—also at 20:00. This Thursday, we'll kick things off with a fun launch event. Will we see you there?

<img src="/uploads/2025/dating_ctf.jpeg" alt="CTF... its a match!" style="width:300px"/>

### March 20 - CTF / Project Night
This evening, we’ll be ready to dive into the new CTF challenges of the week! Are you up for the challenge?

Of course, you can also work on your own projects. The Space is open to everyone! Grab your breakout box, tinker with electronics, fire up the 3D printer, or get creative with the laser cutter. See you then!

<img src="/uploads/2023/hw_hackaton.png" alt="Break-in-Box Realisatie" style="width:600px"/>

### March 27 - CTF / Meetup Python Leiden User Group
This Thursday, you can continue working on the week's CTF challenges. Are you caught up? Have questions? Drop by!

We’re also hosting the team from [Python Leiden User Group](https://pythonleiden.nl/meeting-2025-03-27.html). They will be holding a meetup on some interesting topics.

The talks for this meeting will be in English. The schedule is as follows:

- **Talk 1:** *Serialization in Python including hands-on workshop* by _John Labelle_
- **Talk 2:** *How to use uv for dependency management* by _Michiel Beijen_ (this talk was originally planned for the first meetup but got moved due to time constraints)
- **Talk 3:** *Rendering spatial data in 3D using zarr, jax, babylon.js, and DuckDB* by _Jesse Vanderhees_

<img src="/uploads/2025/pythonleidensquare.svg" alt="Python Leiden User Group" style="width:300px"/>

### March 29 - International Hackerspace Open Day
Once again, Space Leiden is participating in the International Hackerspace Open Day 2025.

Since 2012, hackerspaces have hosted an annual open day—starting in the Netherlands and expanding internationally in recent years. The open day always takes place on the last Saturday of March, and in 2025, that will be March 29.

More info? -> [International Hackerspace Open Day - 29-03-2025](/open-dag/)  

