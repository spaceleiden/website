---
title: Activiteiten November & December 2023
author: Raoul
date: 2023-11-07
layout: blog
redirect_from:
- /november-december-2023/
header: /uploads/2023/nov-dec-banner.png
---
De agenda voor november en december is bekend! We hebben een aantal leuke talks / workshops op de planning!
Wij hebben er zin in, jullie ook?

<!--// more //-->

## 02 november & 9 november - Project Avond
Op 02 en 09 november is er weer een gezellige project avond! 
Kom langs met je eigen projecten of kom met een van onze tools werken. 
Geen inspiratie? Kom eens sparren misschien kunnen we je op weg helpen!
<img src="/uploads/2023/hw_hackaton.png" alt="Project Avond" style="width:400px"/>

## 16 november - Games Night - (Bordspellen / Digital games)
Op 16 november organiseren we een keertje iets anders, natuurlijk is er gezellig plek om te werken
aan eigen projecten. Maar daarnaast is er ook ruimte om gezellig een bordspel te spelen of neem je eigen console mee!
Kom je ook?

Om te voorkomen dat we allemaal het zelfde mee nemen kan je hier aanmelden dat je komt en welke spellen / consoles je mee neemt.
Idealiter zijn het natuurlijk wel party games ;)

<a href="https://docs.google.com/spreadsheets/d/1qwVKQdOhybAbnU4-U9GQHXNfLDAXI-IPkVyMu2GaxYc/edit?usp=sharing" target="_blank"> > Click hier om games door te geven! < </a>

<img src="/uploads/2023/game-night.png" alt="Game Night" style="width:400px"/>

## 23 november - Project Avond
Op 23 november is er weer een gezellige project avond!
Kom langs met je eigen projecten of kom met een van onze tools werken.
Geen inspiratie? Kom eens sparren misschien kunnen we je opweg helpen!

<img src="/uploads/conferencing.jpg" alt="Work Work" style="width:400px"/>

## 30 november - Advent Of Code
Het is weer die tijd van het jaar! Een nieuwe advent kalender,
de heuze Advent Of Code! https://adventofcode.com/

Raoul neemt jullie mee en samen gaan we weer wat opdrachtjes maken.

Naast deze talk / workshop zal Raoul iedere ochtend (je hoort het goed... IEDERE OCHTEND)
rond een uurtje of 7 op discord zijn om zijn advent of code struggles te streamen.

Vind je het leuk om mee te doen? Wees er dan bij!

Mee doen met het leaderboard van The Space?
Code: 125095-36a87d5b

Mogelijk is er een leuk prijsje te winnen voor de leader van het leaderboard!

<img src="/uploads/2023/advent-of-code.png" alt="Advent Of Code" style="width:400px"/>

## 07 december - Project Avond
Op 07 december is er weer een gezellige project avond!
Kom langs met je eigen projecten of kom met een van onze tools werken.
Geen inspiratie? Kom eens sparren misschien kunnen we je opweg helpen!

<img src="/uploads/blogging.jpg" alt="Work Work" style="width:400px"/>

## 14 december - Demoscene Talk
Op 14 december geeft Pjotr samen met een vriend een talk over de Demoscene.
De Demoscene is Internationale computer kunst sub cultuur ge-focussed op het produceren van demo's
Deze self-contained, extreem kleine computer programma's zijn vaak maar een paar kilobytes groot en
worden gebruikt om de programmeer skills, visuele kunst en muziek te laten zien.

De demoscene is een bijzondere wereld, met demoparties en competities.
Benieuwd naar deze wereld? Kom dan langs!

<img src="/uploads/2023/demoscene.png" alt="Demoscene" style="width:400px"/>

## 21 december - End of year PubQuiz
Als jaar afsluiter doen we dit jaar een leuke non-alcoholische pubquiz!
Ben jij een meester in gekke weetjes, popcultuur en The Space?
Kom en doe mee met onze PubQuiz!

<img src="/uploads/pubquiz.jpeg" alt="Pubquiz" style="width:400px"/>

## 28 december - Dicht tussen kerst en oud en nieuw
Het is kerstvakantie! Een mooie tijd van het jaar en om die reden hebben we besloten om
geen fysieke avond te organiseren. In het nieuwe jaar zijn jullie weer welkom!

<img src="/uploads/christmas.jpeg" alt="Merry Christmas" style="width:400px"/>