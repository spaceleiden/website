---
title: Activiteiten September 2023
author: Raoul
date: 2023-08-27
layout: blog
redirect_from:
- /september-2023/
header: /uploads/september-2022.jpg
---
De agenda voor september is bekend! We hebben een aantal leuke talks / workshops op de planning om tezamen gezellig het nieuwe (school)jaar in te gaan!
Hebben jullie weer zin in een productief najaar? Genoeg inspiratie om op te doen OF gewoon ruimte voor eigen projecten! 

<!--// more //-->

## 31 augustus - Zomer Opening - Pubquiz
Nog even een herhaling van de vorige agenda! De zomervakantie is weer bijna voorbij, iedereen gaat weer aan het werk, of begint aan een nieuw collegejaar.
Om te vieren dat we allemaal weer terug zijn, dat we een mooie zomer hebben gehad, en eens bij te praten over wat we deze zomer hebben meegemaakt, 
beginnen we eind augustus met een opening van het nieuwe jaar.

Op **donderdag 31 augustus** nodigen we jullie allemaal uit om langs te komen. 
Neem eens iemand mee, een vriend, vriendin, broer of zus, van wie je denkt dat die The Space ook heel leuk zou vinden.

<img src="/uploads/2023/borrel.jpeg" alt="borrel" style="width:400px"/>

## 07 september - Flashtalks
Deze avond geven we drie korte talks die een beeld geven van wat voor projecten en activiteiten we zoal doen bij The Space. 
Een ideale avond voor mensen die nieuw zijn bij ons! 

**Sprekers gezocht!** 
Heb je zelf een flashtalk (+- 15-20 minuten) die je zou willen geven, of wil je een van je projecten presenteren?
Dat kan! Geef dat even door via bestuur@spaceleiden.nl, dan plannen we je in!

<img src="/uploads/2023/flash-talk.png" alt="flash-talk" style="width:400px"/>

## 14 september - HardwareFest
Op 14 september organiseren we HardwareFest! We zullen zorgen dat er verschillende sensoren en soorten hardware aanwezig zijn.
Of je neemt gewoon je eigen hardware mee!

Het idee van deze avond is dat men lekker aan de slag kan gaan met de aanwezige hardware.
Nog nooit met Hardware gewerd? No problem, Dion en Raoul zullen aanwezig zijn om jullie hiermee te helpen.

Samen brainstormen en inspiratie opdoen, zo maken we er een gezellige avond van.

Kom je ook?

<img src="/uploads/2023/hw_hackaton.png" alt="Hardware Hackaton" style="width:400px"/>

## 21 september - Blender 101
Deze keer hebben we een workshop voor jullie in petto!
Jeanne gaat ons vertellen over Blender 3D en laat ons zien hoe Blender werkt en wat je er allemaal mee kan doen.

3D-modellen maken is een leuk proces en Jeanne loopt je er stap voor stap doorheen.

**LET OP! Je hebt een laptop nodig en kunt daarop van tevoren al Blender 3D installeren.**

<img src="/uploads/2023/blender.jpeg" alt="Blender 3D" style="width:400px"/>

## 28 september - Programmeren voor Dummies
Op de 28e geven we programmeren voor dummies. 
Een workshop/talk waarbij Jeffrey jullie de basis van programmeren uit zal leggen en tevens de belangrijke concepten daar in zal laten zien. 
Altijd al het programmeren magie gevonden? Na deze workshop / talk heb jij de juiste handvatten om aan het werk te gaan!

<img src="/uploads/code.jpg" alt="code" style="width:400px"/>
