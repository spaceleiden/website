---
title: Activiteiten April 2023
author: Raoul
date: 2023-03-20
layout: blog
redirect_from:
- /april-2023/
header: /uploads/2023/together2.jpeg
---
De agenda voor april is bekend! We hebben een aantal leuke talks op de planning staan 
en zijn daarnaast (bijna) elke donderdag geopend voor projecten!

<!--// more //-->

### 6 april - Talk: Networking & Secure connections: Hoe werkt encryptie?
Op 6 april komt Joffrey van Wageningen een talk geven over over netwerken en het opbouwen van een veilige verbinding.
Denk aan certificaten, CA's, DNSSec en de oude vraag, hoe werkt encryptie?
Joffrey is al jaren Systeem engineer en heeft onder andere bij KPN, [Munisense](https://munisense.nl/) en zijn eigen bedrijf [Vulpine ICT](https://vulpine-ict.nl/) gewerkt!
Wil jij meer leren over het internet, certificaten en encryptie? Wees er dan bij!

<img src="/uploads/2023/vulpine_ict.png" alt="Vulpine ICT Logo" style="width:400px"/>

### 13 april - Project Avond
Op 13 april is er weer een gezellige project avond!
Kom langs met je eigen projecten of kom met een van onze tools werken.
Geen inspiratie? Kom eens sparren misschien kunnen we je opweg helpen!

<img src="/uploads/blogging.jpg" alt="Work Work" style="width:400px"/>

### 20 april - AI-Art met Midjourney
Op 20 april geeft Jordy een talk over [Midjourney](https://www.midjourney.com). 
Samen zullen we kijken wat je met Midjourney allemaal kan bereiken en hoe je het kan gebruiken op onze Discord server.

Midjourney is een Discord bot die door middel van kunstmatige intelligentie, afbeeldingen op basis van promts met natuurlijke taal kan maken. 
Met andere woorden, geef een beschrijving van wat je wilt zien en Midjourney zal zijn best doen om een plaatje te maken wat daarop lijkt.

<img src="/uploads/2023/midjourney.png" alt="Midjourney Showcase" style="width:800px"/>

### 27 april - DICHT ivm Koningsdag
Op 27 april is het Koningsdag, een mooie dag voor een feestje. 
Deze donderdag zullen we dicht zijn, we ontvangen je graag de week erna weer op The Space Leiden.

<img src="/uploads/2023/koningsdag.png" alt="Koningsdag" style="width:600px"/>

