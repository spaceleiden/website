---
title: Internationale Open Hackerspace Dag - 25-03-2023
author: Raoul
date: 2023-02-22
layout: blog
redirect_from:
- /open-dag-23/
- /blog/open-dag-23/
header: /assets/img/workshop.jpg
---
De agenda voor maart is bekend! We hebben een uitreiking, extern event, twee toffe workshops, een opendag (met nog meer workshops) 
en zijn daarnaast elke donderdag geopend voor projecten!

<!--// more //-->

### 2 maart - CTF Prijsuitreiking
Op 2 maart doen we de prijsuitreiking van onze CTF. Zit je in de top 5?
Dan zullen we je zeker even uitnodigen! Er hangt namelijk een leuk prijsje aan vast.
Ook zullen we jullie vragen over moeilijke cases beantwoorden.
Welke dingen vonden jullie moeilijk of juist te makkelijk?
Hopelijk tot dan!
<img src="/uploads/2023/awards.jpeg" alt="awards" style="width:400px"/>

### 9 maart - Project Avond
Op 9 maart is er weer een gezellige project avond!
Kom langs met je eigen projecten of kom met een van onze tools werken.
Geen inspiratie? Kom eens sparren misschien kunnen we je opweg helpen!

### 16 maart - Project Avond / Talk op Externe locatie: Masterclass Syntax - NewStory
Op 16 maart is ook een gezellige project avond!
Kom langs met je eigen projecten of kom met een van onze tools werken.
Geen inspiratie? Kom eens sparren misschien kunnen we je opweg helpen!

Door een van onze partners studievereniging Syntax wordt op de Hogeschool een Masterclass geregeld. 
Dit keer komt het bedrijf [Newstory](https://www.newstory.nl) langs. Meer informatie over de inhoud en precieze aanvangstijd wordt hier later verstrekt.

<img src="/uploads/2023/newstory.png" alt="NewStory.nl Logo" style="width:400px"/>

### 23 maart - Workshop - Eigen Webserver opzetten met een Raspberry Pi
Op 23 maart zal Raoul het een en ander uitleggen over webservers en zullen we door middel van een kleine workshop laten zien hoe je 
zelf een webserver op zet op bijvoorbeeld een Raspberri Pi Zero. Er zullen een aantal Raspberry Pi's aanwezig zijn. 
Wil je echter dit vervolgens thuis inzetten dan is het handig om een eigen Raspberry Pi hiervoor aan te schaffen.
<img src="/uploads/2023/raspberry_pi_zero.jpeg" alt="Raspberry Pi Zero" style="width:400px"/>

### 25 maart - Internationale Hackerspaces Opendag
Ieder jaar is er de internationale Hackerspaces opendag.
Hier laten we zien wat onze hackerspace precies inhoud en wat we te bieden hebben. 
Zo zullen er projecten getoont worden en zijn er verscheidene talks en workshops.
Tijdens de opendag zijn we ook gewoon open voor projectwerk dus wil je een keer werken aan je eigen projecten dan zou je dat ook deze zaterdag kunnen doen!

Wil je graag helpen met de opendag door middel van een Talk of door het laten zien van een project waar je aan gewerkt hebt?
Neem dan contact op met het [bestuur](mailto:bestuur@spaceleiden.nl?subject=Opendag - Ik wil graag helpen!&body=Lief Bestuur, Ik kom graag helpen op 25 maart. Ik zou graag het volgende willen presenteren / laten zien / wil gewoon begeleiden) of spreek een van ons aan tijdens de donderdagen.

Meer informatie over de internationale Hackerspace Opendag is te vinden op [hackerspaces.nl](https://hackerspaces.nl/open-dag/)
en onze [blogpost](/blog/open-dag/)

<img src="/uploads/2023/hackerspaces.png" alt="hackerspaces" style="width:400px"/>

### 30 maart - Workshop TinkerCad 2.0
Op 30 maart zal Jelle het een en ander behandelen over het ontwerpen voor de 3D printer.
Door middel van TinkerCad is 3D ontwerp makkelijk(er) toegankelijk. 
Ben jij erbij?

<img src="/uploads/2023/tinkercat.jpeg" alt="TinkerCat" style="width:400px"/>
