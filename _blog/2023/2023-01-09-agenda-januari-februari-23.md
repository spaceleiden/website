---
title: Activiteiten Januari / Februari 2023
author: Raoul
date: 2023-01-08
layout: blog
redirect_from:
- /januari-2023/
- /februari-2022/
- /jan-feb-2023/
header: /uploads/ctf.jpg
---
Then eerste de beste wensen allemaal! We hebben goed nieuws!
Onze planning voor januari en februari is bekend.

<!--// more //-->

### 5 januari - Projectavond
Op 5 januari is er weer een gezellige project avond!
Kom langs met je eigen projecten of kom met een van onze tools werken.
Alleen langskomen om ons de beste wensen te is natuurlijk ook mogelijk!

<img src="/uploads/cosplay.jpeg" alt="cosplay" style="width:300px"/>

### 12 januari - Cosplay Talk
Cosplay! Wie, wat, waar?

Beginnen met cosplay? Soms zie je door alle bomen het bos niet meer. 
Er zijn zoveel dingen aan een kostuum waar je rekening mee moet houden, dat het soms lastig is om te weten waar je moet beginnen. 
Gelukkig zijn er veel cosplayers die wel een helpende hand willen aanreiken :)

Op 12 januari 19:30 zal Anika, kort een praatje komen geven over hoe je kan beginnen, of hoe je verder kan. Ze zal uitleggen wat cosplay nou eigenlijk is, 
hoe je het kan doen, waar je het kan doen en verschillende materialen en sources aanstippen zodat je daarna hopelijk genoeg weet om een beginnetje te maken!
Alvast zin om in de sferen te komen? Kijk vooral op haar insta @chasing_pixiedust, of een van de vele andere cosplay Instagrams beschikbaar!

<img src="/uploads/snake.png" alt="Snake" style="width:300px"/>

### 19 januari - Snake maken met een arduino, ledmatrix en een joystick!
Op 19 januari geeft Raoul een workshop, Snake maken met een arduino, ledmatrix en een joystick!
De workshop zal beginnen om 19:30 op de locatie van The Space.
Wil je mee doen met de workshop van komende week zullen dit de spullen zijn die we gaan gebruiken!

Er zullen een zestal setjes aanwezig zijn maar mocht je verzekerd willen zijn dat je mee kan doen kan je ook zelf de onderdelen bestellen:
- [8x8 matrix](https://www.conrad.nl/p/tru-components-tc-9072480-8x8-matrix-displaymodule-max7219-voor-arduino-2268120)
- [joystick](https://www.conrad.nl/p/joy-it-ky023jm-sensorkit-geschikt-voor-raspberry-pi-pcduino-banana-pi-arduino-1-stuks-1707629) 
- [kabels](https://www.conrad.nl/p/whadda-wpa428-jumper-kabel-40x-draadbrug-stekker-40x-draadbrug-bus-1500-cm-bont-2330814) (beperkt aanwezig op The Space)
- [Arduino](https://www.conrad.nl/nl/p/arduino-development-board-nano-every-nano-2240029.html) (Uno's zullen ook aanwezig zijn op The Space)

<img src="/uploads/netcompany.png" alt="netcompany" style="width:300px"/>

### 26 januari - Projectavond / Externe Talk: Masterclass Syntax - Netcompany
Op 26 januari is er weer een gezellige project avond!
Kom langs met je eigen projecten of kom met een van onze tools werken.

Extern wordt er door een van onze partners (SV. Syntax) een talk georganiseerd. 
Op de hogeschool Leiden komt Netcompany, o.a. bekend van Studiekeuze123 Corona Passport een vette talk houden
Deze talk is vrij toegankelijk voor de leden van The Space.
Meer informatie hiervoor volgt.

<img src="/uploads/ctf_2023.png" alt="CTF" style="width:300px"/>

### Geheel Februari - CTF!
Ook dit jaar is het weer tijd voor onze heuze, befaamde (tenminste dat vinden we zelf) CTF!
We hebben weer een goed verhaal verzonnen en dagelijks (13:37) zullen er weer puzzles en vragen online gezet worden.
Zin in? Laat het ons vooral weten via Discord! Vanaf 1 februari gaan we van start! Zoals ieder jaar belonen we de winnaar met een leuk prijsje!
Onze CTF is hier te vinden: https://ctf.spaceleiden.nl/

### 2 februari - CTF Start
Op 2 februari starten we op The Space officieel onze CTF. Nog nooit eerder mee gedaan of weet je niet waar je moet beginnen?
Sander en Niels vertellen het een en ander over de CTF en kunnen jullie wat handvatten geven om mee te beginnen. Zien we je daar?

### 9 februari - Projectavond
Op 9 februari is er weer een gezellige project avond!
Kom langs met je eigen projecten of kom met een van onze tools werken.

### 16 februari - A deep dive into The CTF
Op 16 februari geven Niels en Sander een nieuwe workshop over onze CTF.
Ze zullen wat dingen behandelen die al langs zijn gekomen en nieuwe tricks laten zien.
Het is nog niet te laat om te beginnen met de CTF doe je mee?

### 23 februari - Projectavond
Op 23 februari is er weer een gezellige project avond!
Kom langs met je eigen projecten of kom met een van onze tools werken.

### 2 maart - CTF-prijsuitreiking
Op 2 maart doen we de prijsuitreiking van onze CTF.
Ook zullen we jullie vragen over moeilijke cases beantwoorden. 
Welke dingen vonden jullie moeilijk of juist te makkelijk?
Hopelijk tot dan!
