---
title: Lasersnijder Update!
author: Raoul
date: 2023-10-01
layout: blog
redirect_from:
- /oktober-2023-lasersnijder-update/
header: /uploads/2023/lasersnijder.png
---
We kunnen weer lasersnijden!

<!--// more //-->

Zoals jullie weten was de lasersnijder sinds juni buitengebruik.
De laser ging gelijk al branden zodra deze werd aangezet en na 2 minuten stopte de lasersnijder met werken door een foutmelding.

Begin september is er een monteur langs geweest voor een onderhoudsbeurt, maar deze vond de machine prima werken. 
Helaas bleven wij de problemen houden waardoor we de lasersnijder buiten gebruik hielden.

Vorige week is er nogmaals een monteur langs geweest om te kijken en hebben ze de bron van de lasersnijder vervangen.
Een prijzige reparatie, maar wel eentje die betekent dat we weer aan de slag zouden moeten kunnen!

De firmware van de lasersnijder heeft een update gehad en tevens is het nu mogelijk om de lasersnijder aan te sturen via 
een nieuwe versie van Jobcontrol en ook via een online tool genaamd Ruby.

Komende donderdag (5 oktober) zullen we kijken of alles inderdaad weer naar behoren werkt waarna men weer zou kunnen lasersnijden.

Belangrijk is wel om aan te geven dat men zorgvuldig met de machine moet omgaan en dat de machine VOOR en NA gebruik moet worden schoongemaakt.

Mochten jullie vragen hebben of benieuwd zijn hoe jullie met de nieuwe software moeten werken, spreek ons vooral aan op de Space.
[Of mail ons](mailto:bestuur@spaceleiden.nl)!
