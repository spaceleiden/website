---
title: Activiteiten Oktober 2023
author: Raoul
date: 2023-10-01
layout: blog
redirect_from:
- /oktober-2023/
header: /uploads/2023/oktober_banner.png
---
De agenda voor oktober is bekend! We hebben een aantal leuke talks / workshops op de planning!
Wij hebben er zin in, jullie ook?

<!--// more //-->

## 05 oktober - Project Avond
Op 05 oktober is er weer een gezellige project avond! 
Kom langs met je eigen projecten of kom met een van onze tools werken. 
Geen inspiratie? Kom eens sparren misschien kunnen we je op weg helpen!
<img src="/uploads/2023/hw_hackaton.png" alt="Project Avond" style="width:400px"/>

## 12 oktober - Flutter App bouwen
Altijd al een eigen app willen bouwen, maar geen idee waar je moet beginnen? 
Op 12 oktober neemt Dion je mee met het bouwen van een eigen flutter app. 
Hij laat je de eerste stappen zien en neemt je mee in het proces van het bouwen van een eigen app. 

Inspiratie voor een eigen app? Na deze workshop moet je daarmee zeker aan de slag kunnen!

**LET OP! Je hebt een laptop nodig om met deze workshop mee te kunnen doen. [Idealiter installeer je ook alvast Flutter (of je gebruikt docker)](https://docs.flutter.dev/get-started/install)**

<img src="/uploads/2023/flutter_app.png" alt="FlutterApp" style="width:400px"/>

## 19 oktober - Project Avond
Op 19 oktober is er weer een gezellige project avond!
Kom langs met je eigen projecten of kom met een van onze tools werken.
Geen inspiratie? Kom eens sparren misschien kunnen we je op weg helpen!

<img src="/uploads/solderen.jpg" alt="Hardware Hackaton" style="width:400px"/>

## 26 oktober - Kubernetes 101 met Raspberry Pi's
Op 26 oktober geeft Matthijs een workshop/talk over Kubernetes en hoe je dit thuis op zou kunnen zetten.
Door een cluster op te zetten met behulp van Raspberry Pi's maken we infrastructuur die vaak in de Cloud draait,
maar dan iets dichter bij huis. Wat gebeurt er bijvoorbeeld als we de stroom van een van de Pi's eruit trekken?
En hoe beslist het cluster wat er op welke Pi terecht komt?

Benieuwd hoe Kubernetes werkt en hoe je dit zelf kan gebruiken? Kom gezellig langs!

**LET OP! Als je mee wilt doen!**
Je kan op 2 manieren mee doen met het interactieve deel. 
Als je een laptop mee neemt kan je zelf services gaan deployen op het cluster. 
Eventueel kan je met je laptop (met Linux/Docker) of een Pi 3/4/5 ook zelf deel nemen aan het cluster.

<img src="/uploads/2023/rpi_k8.png" alt="rpi_k8s" style="width:400px"/>
