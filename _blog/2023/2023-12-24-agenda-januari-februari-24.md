---
title: Activiteiten Januari / Februari 2024
author: Raoul
date: 2023-12-24
layout: blog
redirect_from:
- /januari-2024/
- /februari-2024/
- /jan-feb-2024/
header: /uploads/ctf.jpg
---
Ten eerste de beste wensen allemaal! We hopen dat iedereen een fijne kerst heeft en een goed nieuwjaar tegemoet gaat.
Ook in 2024 gaan we weer leuke dingen doen op The Space. Hieronder een overzicht van de activiteiten die we in januari en februari gaan doen.

<!--// more //-->

<img src="/uploads/kerstbal.png" alt="Kerstbal" style="width:350px"/>

### 4 januari - Projectavond
Op 4 januari beginnen we het jaar met een gezellige project avond!
Kom langs met je eigen projecten of kom met een van onze tools werken.
Alleen langskomen om ons de beste wensen te is natuurlijk ook mogelijk!

<img src="/uploads/2024/pc.png" alt="pc" style="width:300px"/>

### 11 januari - Nieuwjaarsborrel / games
Borrel? Gezelligheid? Games? Dat klinkt als een goed begin van het jaar!

Op 11 januari is er een nieuwjaarsborrel op Space Leiden. Ben jij van de partij? 
Onder het genot van een lekker (0,0) drankje, snacks en wat party games luiden wij het nieuwe jaar in. 

<img src="/uploads/2024/nieuwjaarsborrel-proost.jpg" alt="borrel" style="width:300px"/>

### 18 januari - HardwareFest - Climate Control
Op 18 januari organiseren we weer HardwareFest! We zullen zorgen dat er verschillende sensoren en soorten hardware aanwezig zijn.
Of je neem gewoon je eigen hardware mee!

Deze keer starten we met een Thema! Climate Control. We gaan aan de slag met sensoren die onder andere de temperatuur en luchtvochtigheid meten.
We gaan kijken hoe we deze data kunnen gebruiken om bijvoorbeeld een klimaat dashboard te maken of hoe je dit zou kunnen gebruiken om je eigen woning te automatiseren.

Het idee van deze avond is dat men lekker aan de slag kan gaan met de aanwezige hardware.
Nog nooit met Hardware gewerd? No problem, Dion en Raoul zullen aanwezig zijn om jullie hiermee te helpen.

Samen brainstormen en inspiratie opdoen, zo maken we er een gezellige avond van.

Kom je ook?

<img src="/uploads/climate-sensor.jpg" alt="Climate Sensor" style="width:400px"/>

### 25 januari - Projectavond
Kom langs met je eigen projecten of kom met een van onze tools werken.
Geen inspiratie? Kom eens sparren misschien kunnen we je op weg helpen!

<img src="/uploads/2023/hw_hackaton.png" alt="Project Avond" style="width:400px"/>


### Geheel Februari - CTF!
Ook dit jaar is het weer tijd voor onze heuze, befaamde (tenminste dat vinden we zelf) CTF!
We hebben weer een goed verhaal verzonnen en dagelijks zullen er weer puzzles en vragen online gezet worden.

Dit jaar hebben jullie zelfs een dag extra om de CTF te doen! Leuk he, zo'n schrikkeljaar!

Zin in? Laat het ons vooral weten via Discord #ctf-2024! Vanaf 1 februari gaan we van start! Zoals ieder jaar belonen we de winnaar met een leuk prijsje!
Onze CTF is hier te vinden: https://ctf.spaceleiden.nl/

<img src="/uploads/ctf_2023.png" alt="CTF" style="width:300px"/>

### 1 februari - CTF Startup
Op 1 februari starten we op The Space officieel onze CTF. Nog nooit eerder mee gedaan of weet je niet waar je moet beginnen?
Sander en Niels vertellen het een en ander over de CTF en kunnen jullie wat handvatten geven om mee te beginnen. Zien we je daar?

### 8 februari - Projectavond
Op 8 februari is er weer een gezellige project avond!
Kom langs met je eigen projecten of kom met een van onze tools werken.

Of neem je laptop mee en doe mee met onze CTF! Zelfs al loop je een paar dagen achter, geen probleem!
Het is niet te laat om er met de prijs vandoor te gaan!

### 15 februari - A deep dive into The CTF
Op 15 februari geven Niels en Sander een nieuwe workshop over onze CTF.
Ze zullen wat dingen behandelen die al langs zijn gekomen en nieuwe tricks laten zien.
Het is nog niet te laat om te beginnen met de CTF doe je mee?

### 22 februari - Projectavond
Op 22 februari is er weer een gezellige project avond!
Kom langs met je eigen projecten of kom met een van onze tools werken.

### 29 februari - Projectavond
Na een maand in het teken te hebben gestaan van onze CTF is er op de laatste avond van februari, 29, februari weer met een gezellige project avond!
Kom langs met je eigen projecten of kom met een van onze tools werken.

<img src="/uploads/2024/pc.png" alt="pc" style="width:300px"/>
