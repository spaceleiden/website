---
title: Lasersnijder Update
author: Raoul
date: 2023-06-28
layout: blog
redirect_from:
- /juni-2023-lasersnijder-update/
header: /uploads/2023/lasersnijder.png
---
Helaas is de lasersnijder op dit moment tot nader order buiten gebruik!
Er zal een onderhoudsbeurt vanuit de fabrikant nodig zijn om deze weer werkzaam te krijgen.


