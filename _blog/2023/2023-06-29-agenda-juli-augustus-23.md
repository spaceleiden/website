---
title: Activiteiten Juli & Augustus 2023
author: Raoul
date: 2023-06-29
layout: blog
redirect_from:
- /juli-2023/
- /augustus-2023/
- /juli-augustus-2023/
header: /uploads/2023/beach_banner.jpeg
---
De agenda voor juli en augustus is bekend! We hebben een aantal leuke workshops op de planning om jullie te vermaken tijdens dit warme weer.
Daarnaast zijn we elke donderdag geopend voor projecten!

<!--// more //-->
### 06 juli - Hack The Box
The Space Leiden heeft op 06 juli weer een Hack The Box Live demo. Niels gaat proberen om weer een leuke box te kraken.

Hack The Box is een service waar je zelf kan proberen om in te dringen in een bestaande server. Nieuwsgierig wat Hackt The Box is? Kom vooral kijken!

Wil je zelf mee doen? Zorg dan dat je een virtuele machine met Kali hebt draaien en een account hebt op Hack The Box.

<img src="/uploads/2023/hackthebox.png" alt="Hack The Box" style="width:400px"/>

### 13 juli  - Project Avond
Op 13 juli is er weer een gezellige project avond!
Kom langs met je eigen projecten of kom met een van onze tools werken.
Geen inspiratie? Kom eens sparren misschien kunnen we je opweg helpen!

<img src="/uploads/code.jpg" alt="Code" style="width:400px"/>

### 20 juli - Blender 101
Deze keer hebben we een workshop voor jullie in petto!
Jeanne gaat ons vertellen over Blender 3D en laat ons zien hoe Blender werkt en wat je er allemaal mee kan doen.

3D modellen maken is een leuk proces en Jeanne loopt je er stap voor stap doorheen. 

je hebt een laptop nodig en kunt daarop van te voren al Blender 3D installeren.

<img src="/uploads/2023/blender.jpeg" alt="Blender 3D" style="width:400px"/>

## 27 juli - Projectavond
Op 27 juli is er weer een gezellige project avond!
Kom langs met je eigen projecten of kom met een van onze tools werken.
Geen inspiratie? Kom eens sparren misschien kunnen we je opweg helpen!

<img src="/uploads/leren_programmeren.jpg" alt="Projects" style="width:400px"/>

### 3 augustus - Hardware Fest
Op 3 Augustus organiseren we weer HardwareFest! We zullen zorgen dat er verschillende sensoren en soorten hardware aanwezig zijn.
Of je neem gewoon je eigen hardware mee!

Het idee van deze avond is dat men lekker aan de slag kan gaan met de aanwezige hardware.
Nog nooit met Hardware gewerd? No problem, Dion zal aanwezig zijn om jullie hiermee te helpen.

Samen brainstormen en inspiratie opdoen, zo maken we er een gezellige avond van.

Kom je ook?

<img src="/uploads/2023/hw_hackaton.png" alt="Hardware Hackaton" style="width:400px"/>

### 10 augustus - Projectavond
Op 10 augustus juli is er weer een gezellige project avond!
Kom langs met je eigen projecten of kom met een van onze tools werken.
Geen inspiratie? Kom eens sparren misschien kunnen we je opweg helpen!

<img src="/uploads/blogging.jpg" alt="Blog work" style="width:300px"/>

## 17 augustus - D&D Oneshots
Op donderdag 17 augustus mei nemen de DM's van Space Leiden jullie mee in een nieuw avontuur!

Heb je je dice klaar? Veel voorbereiding aan jullie kant zal niet nodig zijn.
Er zullen premade karakters beschikbaar zijn om te spelen. Kies een class en ga los!

Nog nooit geDNDt maar wel benieuwd? Geen probleem! We zullen deze avond wat eerder beginnen en dan kun je alle uitleg horen!

Rond 18:00 zullen we verzamelen voor een hapje eten (pizza) en de uitleg voor zij die nog nooit eerder gespeeld hebben. 
De sessies zullen om 19:00 van start gaan en we verwachten rond 22:30-23:00 klaar te zijn!

Vanwege het feit dat er gebalanceerde groepen moeten zijn hebben we een inschrijving. Dit betekent ook dat er een maximum aan spelers kan mee spelen. 
Wil je erbij zijn, zorg er dan voor dat je je tijdig aanmeldt.

[Aanmelden klik hier!](https://forms.gle/9wnBMALcaAQwDnaG8)

<img src="/uploads/oneshot.jpeg" alt="oneshot" style="width:300px"/>

## 24 augustus - Projectavond
Op 24 augustus is er weer een gezellige project avond!
Kom langs met je eigen projecten of kom met een van onze tools werken.
Geen inspiratie? Kom eens sparren misschien kunnen we je opweg helpen!

<img src="/uploads/code.jpg" alt="Code" style="width:400px"/>

## 31 augustus - Zomer Opening - Pubquiz
De zomervakantie is weer bijna voorbij, iedereen gaat weer aan het werk, of begint aan een nieuw collegejaar.
Om te vieren dat we allemaal weer terug zijn, dat we een mooie zomer hebben gehad, en eens bij te praten over wat we deze zomer hebben meegemaakt, 
beginnen we eind augustus met een opening van het nieuwe jaar.

Op **donderdag 31 augustus** nodigen we jullie allemaal uit om langs te komen. 
Neem eens iemand mee, een vriend, vriendin, broer of zus, van wie je denkt dat die The Space ook heel leuk zou vinden.

<img src="/uploads/2023/borrel.jpeg" alt="borrel" style="width:300px"/>

