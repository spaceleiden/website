---
layout: blog
title: "Ternaire computers: 0, 1 en… -1??"
author: Jeanne
date: 2020-07-27
header: /uploads/matrix.jpg
redirect_from:
    - /news/ternaire-computers-0-1-en-1/
    - /en/news/ternaire-computers-0-1-en-1/
---
In 1958 ontwikkelde Nikolai Brusenzov met zijn team ’s werelds eerste
(en praktisch laatste) ternaire computer, genaamd SETUN. De computer is gebouwd
in de Research Computing Laboratories aan de Moscow State University en werd
vernoemd naar de nabijgelegen rivier Setun. Wat maakte deze computer zo bijzonder
en waarom hebben we toch voor binair gekozen?

## Wat is een ternaire computer?
Binaire computers werken met twee waardes, 1 en 0, aan en uit, true en false.
Klinkt voor de meesten vrij logisch en eenvoudig. Maar wat nou als we een extra
waarde toevoegen? In het geval van gebalanceerde ternaire logica is dit -1, in
het geval van ongebalanceerde logica is dit een 2. De SETUN werkte met -1 als
derde waarde.

Een voordeel van ternaire logica is dat negatieve waarden net zo makkelijk zijn
uit te drukken als positieve waardes. Dit voordeel leidt er toe dat sommige
berekeningen makkelijker zijn uit te voeren met ternaire logica.

## Waarom het interessant was
Zelfs met de Berlijnse muur die oost en west van elkaar scheidde, was er grote
interesse om van elkaar te leren. Deze nieuwe computer was geen uitzondering.
Nadat het Westen te horen kreeg van deze mogelijke nieuwe doorbraak zijn wetenschappers
aan de slag gegaan. Per slot van rekening hadden de Russen al eerder succes met ’s
werelds eerste satelliet Spoetnik 1, wie weet was hier ook iets moois uit te halen.

Het Westen kon het volgende leren van de Russen:

* De ternaire computers kunnen bepaalde rekenkundige problemen makkelijker oplossen dan
binaire computers.
* De ternaire computers waren goedkoper dan binaire computers.

## Waarom het toch niet zo interessant was
De onderzoeken naar ternaire computers en ternaire logica verdween stilletjes na
verloop van tijd en ondanks de voordelen zijn eigenlijk alle computers tegenwoordig binair.
Hiervoor is een aantal redenen te verzinnen:

* Het gebeurde nou eenmaal niet. De binaire computer bestond al en de voordelen
waren niet groot genoeg om alles om te gooien. Grote investeringen moeten worden
gedaan om voortaan ternaire chips te maken.
* SETUN realiseerde niet volledig de potentie van het ternaire stelsel. Dit komt
doordat elke trit (ternaire bit) werd opgeslagen in een stel magnetische spoelen.
Het implementeren van de logica was lastiger dan de theorie.
* De hardware voor ternaire computers was (toendertijd) lastiger te ontwerpen en
realiseren dan de hardware voor binaire computers.

## Waarom het interessant kan worden
In Zuid Korea is de eerste ongebalanceerde ternaire halfgeleider ontworpen.
De 0 en 1 in dit ontwerp zijn hetzelfde als bij een binaire computer, de 2
wordt gebruikt door lekstroom. Een teken dat we wellicht de bevindingen van de
russen toch nog in de toekomst kunnen inzetten. Maar voorlopig blijven we
rekenen in nullen en enen.
