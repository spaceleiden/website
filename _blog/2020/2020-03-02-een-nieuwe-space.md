---
layout: blog
title: Een nieuwe Space
author: Raoul
date: 2020-03-02
header: /uploads/table.jpg
redirect_from:
    - /news/een-nieuwe-space/
    - /en/news/een-nieuwe-space/
---
Een Makerspace, een Hackerspace, een FabLab: de laatste tijd hoor je deze termen
steeds vaker. Maar wat is een makerspace precies? Kort gezegd: een plek om te
maken. Op scholen, publieke instellingen en bij private organisaties zie je
steeds vaker makerspaces ontstaan, maar wat houdt dat eigenlijk in, en wat heb je er aan?

Een makerspace is vaak een non-profit, openbare organisatie waar mensen met
gedeelde interesses kunnen samenkomen, en iets kunnen creëren. Binnen een
makerspace zijn dit vaak interesses rondom computers, machines, technologie,
(digitale) kunst, houtbewerking, enzovoorts. Iedereen kan hier komen om te
werken aan projecten, om elkaar te ontmoeten, en te leren van de community.

## Maatschappelijke bijdrage
Zo’n ruimte kan mensen stimuleren om te netwerken, nieuwe dingen te proberen,
en samen te werken. Daarnaast kan een makerspace verschillende diensten en
machines aanbieden die niet iedereen zomaar in huis heeft: zoals grote boormachines,
3D-printers, zware apparatuur, servers, enzovoorts. Daarnaast kan het een plek
zijn om meetups te organiseren, workshops en lessen aan te bieden, en hackathons
te hosten.

![](/uploads/robot_car.jpg)

Vaak is een makerspace ook betrokken bij het starten van nieuwe projecten en
maatschappelijke initiatieven, open-source projecten, en kickstarters en
crowdfunding, en is regelmatig ook zelf afhankelijk van die laatste soort
inkomstenbronnen. Mensen met de ideeën, maar niet de mogelijkheden om zelf
een project te beginnen, kunnen hier gelijkgestemden en hulp vinden om hun
project van de grond te krijgen.

## Geschiedenis
Het concept van een hackerspace komt origineel uit Berlijn. In 1981 werd hier de
Chaos Computer Club (CCC) opgericht. Hier kwamen computer programmeurs samen om
apparatuur te hacken en het dingen te laten doen die het niet hoorde te doen.
Er werden regelmatig evenementen worden georganiseerd, waaronder congressen,
hackathons, etcetera. Sinds de jaren ‘90 zijn maker- en hackerspaces steeds
populairder geworden in Nederland. De eerste spaces in ons land werden opgericht
in kraakpanden, die regelmatig moesten verhuizen. Inmiddels bestaan er in
Nederland en België vele spaces, van heel kleine tot grote publieke. Spaces
worden opgericht in scholen en universiteiten, maar ook binnen bedrijven,
of in de publieke ruimte, zoals bij bibliotheken, of vanuit private organisaties.

## Lidmaatschappen
Om een space te kunnen onderhouden en te kunnen blijven aanbieden, maken veel
spaces gebruik van lidmaatschappen. Op die manier verzamelen ze contributie en
behouden ze een stabiel ledenbestand, maar daarnaast kunnen ze op die manier
controleren wie aan de slag gaat met hun apparatuur en materiaal.

## Een nieuwe space in Leiden?
Zo'n ruimte willen wij graag realiseren in onze eigen omgeving. Wij zien voor
ons een combinatie tussen een makerspace en een hackerspace, en precies wat
jij er als gebruiker uit wilt halen! Wil je meer weten over wat wij van plan
zijn, dan kun je meer informatie vinden op onze website. Heb je interesse in
zo'n omgeving in Leiden? Vul dan onze enquête in!
