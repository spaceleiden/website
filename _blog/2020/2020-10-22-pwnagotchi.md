---
title: Pwnagotchi talk
author: Mathijs
date: 2020-10-22
layout: blog
redirect_from:
    - /pwnagotchi-talk/
    - /en/pwnagotchi-talk/
    - /projects/pwnagotchi-talk/
header: /uploads/pwnagotchi.jpg
---

Op 22 oktober heeft gastspreker Mathijs het gehad over WiFi Security, wachtwoord veiligheid en het opensource project "Pwnagotchi". Met pwnagotchi verander je een pi (zero) in een WiFi pentesting tool (en tamagotchi met schattige gezichtjes).

De Pwnagotchi is een AI tool die van de verscheidene WiFi netwerken om zich heen leert. De Pwnagotchi verzameld zogenaamde WiFi Handschakes. Deze geven hackers informatie over het netwerk en geeft ze de mogelijkheid om te proberen om het WiFi wachtwoord te vinden!

Wil je er zelf mee aan de slag heb je de volgende materialen nodig:

- [Raspberry Pi Zero W](https://www.kiwi-electronics.nl/raspberry-pi-zero-wh-header-voorgesoldeerd) * (met GPIO pin headers erop voor de e-ink HAT)
- SD-kaartje
- Micro USB kabel
- [WaveShare v2 e-ink HAT](https://www.kiwi-electronics.nl/e-ink-display-hat-for-raspberry-pi-2-13inch-250x122?) (optioneel, maar wel leuk!) (in NL te bestellen bij Kiwi Electronics)
- Power bank (optioneel, voor als je 'm onderweg wilt gebruiken)

\* Het is ook mogelijk om een gewone Pi 3 of 4 te gebruiken dan heb je echter niet van die schattige gezichtjes!

De slides van deze talk zijn [hier te vinden](/uploads/slides_pwnagotchi.pdf).

![](/uploads/pwnagotchi.jpg)
