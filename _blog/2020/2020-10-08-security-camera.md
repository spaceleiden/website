---
title: Maak een security camera
author: Raoul en Sander
date: 2020-10-08
layout: blog
redirect_from:
    - /security-camera/
    - /en/security-camera/
    - /projects/security-camera/
header: /uploads/rpi-camera.jpg
---

Tijdens deze workshop gaan we met een Raspberry Pi en een Pi camera een low cost security camera. Wil je je waardevolle spullen in de gaten houden of gewoon zien wat je huisdieren thuis uitspoken dat kan gemakkelijk en hoeft niet duur te zijn!

Tijdens deze workshop maken we een camera en stellen we deze zo in dat je in je eigen huis een live stream kan bekijken en dat daar waar beweging is het filmpje wordt opgeslagen. Na het beginnende deel van de workshop zou je het eventueel zelf nog kunnen uitbreiden om bijvoorbeeld de stream ook buitenshuis te kunnen zien.

Voor deze workshop heb je nodig:

- [Raspberry Pi (3,4 of zero met een apart kabeltje)](https://www.kiwi-electronics.nl/raspberry-pi/board-and-kits/raspberry-pi-3-model-b-plus)
- [Raspberry Pi compatibel camera (v1.3 of v2.1, eventueel de Noir versie voor nachtzicht)](https://www.tinytronics.nl/shop/nl/raspberry-pi/raspberry-pi-compatible-camera-5mp-v1.3)
- SD kaartje voor in de Pi met kaartlezer (download op voorhand Raspberry Pi OS)
- Kabel om de Pi van stroom te voorzien (micro-usb Pi 3, usb-c Pi 4)
- Optioneel een extra USB voor extra opslagruimte
- Optioneel een Pi WiFi usb-stick voor oudere Pi’s zonder WiFi chip (Pi 1 & 2)
- Optioneel een infrarood licht setje van bijvoorbeeld Pi Supply
- Laptop om te verbinden met je Pi

We hebben een paar setjes om mee te spelen aanwezig maar deze zijn niet te koop. Wil je dus je eigen camera hebben? Zorg er dan dat je voor je eigen onderdelen mee neemt.

Tijdens deze workshop gaan we er van uit dat je al enige ervaring hebt met het aanspreken van een Raspberry Pi via je computer.

![](/uploads/rpi-camera.jpg)
