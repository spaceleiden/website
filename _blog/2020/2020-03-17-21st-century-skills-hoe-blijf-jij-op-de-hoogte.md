---
layout: blog
title: 21st Century Skills
author: Jeanne
date: 2020-03-17
header: /uploads/21stcenturyskills.jpg
redirect_from:
    - /news/21st-century-skills-hoe-blijf-jij-op-de-hoogte/
    - /en/news/21st-century-skills-hoe-blijf-jij-op-de-hoogte/
---
Technologie ontwikkelt zich bijzonder snel en er komen voortdurend nieuwe apparaten,
software, apps en gadgets op de markt. Deze verschillende technologieën richten
zich op alle facetten van het leven en proberen van alles te automatiseren,
leuker of gemakkelijker te maken en mensen te verbinden. Hoe blijf je op de hoogte
van alle ontwikkelingen? Hoe zorg je dat je weet waar jij, je kinderen of
je grootouders op moeten letten bij veilig gebruik van het internet?
En moet iedereen over vijf jaar kunnen programmeren?

21st Century Skills is een verzamelnaam voor algemene competenties die belangrijk
zijn in de huidige samenleving. Op scholen wordt deze term steeds meer geïntroduceerd
in de vorm van mediawijsheid, ict- en programmeervakken, computational thinking en privacy.
Kinderen groeien op in een steeds technischere en digitale wereld. Ze moeten leren
hoe ze hiermee om kunnen gaan en hierop in kunnen spelen. Niet alleen om te leren
hun privacy te beschermen en verantwoordelijk met social media om te gaan,
maar ook om te leren betrouwbare informatie te onderscheiden van fake news,
hoe ze computers kunnen inzetten bij hun dagelijkse bezigheden en hoe
ze zelf kennis kunnen verzamelen via het internet. Deze skills beginnen nu
steeds meer voor te komen in het onderwijs, maar dit houdt niet alle trends en
ontwikkelingen bij. Veel scholen bieden nog geen van deze ‘nieuwe’ vakken aan,
omdat het lesmateriaal er nog niet is, er nog geen docenten met genoeg kennis
zijn om het vak te kunnen aanbieden, of omdat de noodzaak nog niet wordt ingezien.
Daarnaast is er nog weinig ruimte voor deze vaardigheden in de landelijke leerplankaders.
Hierdoor hebben scholen minder ruimte om dit soort vakken in hun lesprogramma op te nemen.

## Leven lang leren
21st Century Skills zijn trouwens niet alleen voor kinderen nuttig, maar voor iedereen.
Iemand die begrijpt hoe een computer denkt en hoe het internet werkt kan verantwoord
met die technologieën omgaan en kan deze naar zijn hand zetten. In de toekomst
(maar eigenlijk is dit proces al lang begonnen) wordt al het repetitieve en
routinematige werk steeds meer overgenomen door computers en wordt het werk
van de menselijke werknemers om zich meer te richten op doelen stellen en problemen oplossen.
De huidige economie verschuift op die manier steeds meer naar een kenniseconomie.
Om op de hoogte te blijven van wat er gebeurt en de nieuwste ontwikkelingen te kunnen volgen,
is het belangrijk om te blijven leren en te blijven investeren in kennis. Hier komt het thema
‘een leven lang leren’ vandaan.

Om te laten zien welke aspecten belangrijk zijn en hoe ook jij kunt blijven leren,
publiceren wij een serie blogposts in het thema van 21st Century Skills.

## Programmeren is de toekomst
Deze stelling hoor je steeds vaker om je heen. De wereld ontwikkelt, computers
en IT worden steeds belangrijker en je zal dit in alle facetten van je leven
tegenkomen. Denk aan nieuwe apps op je mobiele telefoon, een steeds ‘slimmer’
huis met een voice assistant, slimme thermostaat of koelkast, maar ook op
je werk: nieuwe software of websites die jouw werk of je samenwerking met
collega’s nog makkelijker maken. Voor al deze tools geldt dat je er mee om
moet leren gaan en moet begrijpen wat ze doen en hoe ze werken. Als je begrijpt
hoe een applicatie gemaakt wordt en hoe deze communiceert met het internet,
andere apparaten of applicaties, kun je deze misschien sneller en gemakkelijker
inzetten op de voor jou beste manier. Daarnaast helpt het om te begrijpen hoe
jouw privacy en autonomie al dan niet wordt gewaarborgd of bedreigd door een
nieuwe technologie. Zo kun je jezelf beschermen en optimaal gebruik maken van
de nieuwste mogelijkheden.

Maar hoe doe je dat, leren programmeren? Waar begin je? En waarmee? Lees dit
allemaal binnenkort op onze blog.

## Een makerspace
Een hackerspace of makerspace is een plek waar mensen van allerlei achtergronden
samenkomen om elkaar te ontmoeten, van elkaar te leren en nieuwe dingen te maken.
Zo’n plek kan jou helpen om aan de slag te gaan met je ideeën. Om nieuwe dingen
te leren en inspiratie en ondersteuning te vinden. Mis jij een applicatie op je
telefoon die jouw leven veel makkelijker zou kunnen maken en wil je die zelf gaan
ontwikkelen, maar niet in je eentje? Wil je je huis automatiseren, of een robot
bouwen die jouw koffie van je keuken naar je bank brengt, maar heb je niet de
kennis of gewoon niet de apparatuur om zoiets te maken?

In een makerspace vind je vaak allerlei apparaten die jij thuis niet zomaar
hebt, zoals zagen en boren, maar ook grotere machines zoals lasersnijders
zaagbanken of 3D printers. Er is plek voor kleine elektronica, om solderen,
te prototypen en te werken met sensoren.

Wij willen graag zo’n ruimte beginnen in Leiden, een ruimte om samen te leren
en jouw 21st Century Skills te verbeteren. Een ruimte om te maken, hacken en
creëren. Hiervoor hebben we jouw input nodig.

Op dit moment zijn wij in gesprek met de gemeente, de Hogeschool, de Universiteit
en verschillende instellingen in Leiden om dit waar te maken. Hiervoor willen we
graag weten hoe groot jouw interesse is en wat jij graag zou zien bij onze makerspace.
Vul onze enquête in en laat ons weten wat jij wil!
