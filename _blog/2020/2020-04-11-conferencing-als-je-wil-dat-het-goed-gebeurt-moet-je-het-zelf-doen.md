---
layout: blog
title: "Conferencing: als je wil dat het goed gebeurt, moet je het zelf doen!"
author: Niels
date: 2020-04-11
header: /uploads/conferencing.jpg
redirect_from:
    - /news/conferencing-als-je-wil-dat-het-goed-gebeurt-moet-je-het-zelf-doen/
    - /en/news/conferencing-als-je-wil-dat-het-goed-gebeurt-moet-je-het-zelf-doen/
---
We zitten inmiddels al tijdje thuis door het COVID-19 virus (op het moment van
schrijven een week of 3) en zijn al aardig bedreven geraakt in het gebruiken
van conferencing apps. Tot voor kort was de populairste applicatie voor
conferencing Zoom, deze is vooral populair bij scholen en thuisgebruikers.
Nu is er in de afgelopen tijd een groot aantal privacy klachten omhoog gekomen
over Zoom en zo zouden ook de desktop client voor zowel Windows als OSX
beveiligingslekken hebben. Hieronder een aantal bronnen over de problemen
van Zoom:

* Zoom liet e-mailadressen uitlekken
* Erkenning van de problemen door Zoom zelf
* Beveiligingslekken in de OSX client

Nu is dit artikel niet bedoeld om Zoom de grond in te drukken, het wordt echter
gebouwd door mensen en mensen maken fouten. Het feit dat ze hun fouten netjes
toegeven en duidelijk vertellen hoe ze het aan het verbeteren zijn is juist zeer
netjes. Ik zou graag zien dat meer bedrijven dat deden!

Nu is het natuurlijk bijna overal de vraag hoeveel van jouw data privé blijft.
Tenzij je uiteraard (we blijven nerds) je eigen conferencing server opzet!

## Jitsi
Ik ben een beetje gaan speuren op het internet en kwam Jitsi tegen. Na het
proberen van deze applicatie op een server ben ik van mening dat deze goed
te installeren en te configureren is!

Dit zijn de requirements die je nodig hebt om deze guide stap voor stap te volgen:

* Een Server/VM met minimaal 2 CPU Cores en 2GB aan RAM
* Een geüpdatete ubuntu 18.04 installatie
* Een domeinnaam
* Als je achter een NAT zit moeten de volgende porten geforward zijn:
    - Tcp Port 80
    - Tcp Port 443
    - Udp Port 10000

## Installatie
Ik ga twee configuraties uitleggen, eerst de configuratie voor als jouw machine
direct aan het internet hangt. Bijvoorbeeld als je een VPS bij een hosting
bedrijf hebt staan. Ook leg ik uit hoe je Jitsi configureert als je server
achter een NAT staat. Bijvoorbeeld als deze thuis staat achter je router of
je hebt een server die als hypervisor fungeert!

Aangezien we wat sources gaan toevoegen moeten we eerst het Software Properties
Common pakket installeren:
```
sudo apt install software-properties-common
```
Jitsi maakt gebruikt van Universe paketten, dit zijn paketten die door de Ubuntu
community worden onderhouden. Deze repo is echter niet altijd standaard beschikbaar,
om ervoor te zorgen dat deze beschikbaar is voor apt runnen we:
```
sudo apt-add-repository universe
```
Nu voegen we de repo url van Jitsi toe aan de source lijst van apt. Dit zorgt ervoor
dat apt Jitsi paketten kan installeren en automatisch kan updaten. Eerst voegen we de url toe:
```
echo 'deb https://download.jitsi.org stable/' | sudo tee /etc/apt/sources.list.d/jitsi-stable.list
```
Nu voegen we de gpg key van de Jitsi repo toe aan de toegestane keys waar apt gebruik van mag maken:
```
wget -qO -  https://download.jitsi.org/jitsi-key.gpg.key | sudo apt-key add -
```
Nu we alle nodige repo's hebben toegevoegd moeten we apt alle lijsten laten updaten:
```
sudo apt update
```
Nu is Jitsi te installeren met het eenvoudige commando:
```
sudo apt install jitsi-meet
```
Tijdens de installatie zal je gevraagd worden wat de hostname moet zijn voor de
Jitsi installatie.

![](/uploads/jitsi_hostname.png)

Geef hiervoor de hostnaam of het IP adres waar jouw server vanaf de buitenkant
mee te bereiken is! Dus als jij straks jouw Jitsi installatie wil hosten
op https://meet.spaceleiden.nl dan vul je hier meet.spaceleiden.nl in!

Wanneer je gevraagd wordt of je een certificaat wil generen,
kies dan "I want to use my own certificate" aangezien we die later toe gaan voegen!

![](/uploads/jitsi_ssl.png)

Als je gevraagd wordt wat de volledige locatie van je SSL key file en Certificaat is
gebruik voor allebei de standaard waarde, we gaan deze in een latere stap nog aanpassen.

![](/uploads/jitsi_ssl_2.png)

Goed nu is Jitsi geïnstalleerd, nu is het tijd om een SSL certificaat te regelen.

## Nginx Configuratie

Eerst veranderen we de port van de webserver Nginx van de Jitsi standaard (4443)
naar de normale HTTPS standaard (443).
```
nano /etc/nginx/sites-enabled/{DOMEINNAAM}.conf
```
Je ziet nu 2 secties:
```
server {
    listen 80;
    listen [::]:80;
    server_name {DOMEINNAAM};

    location ^~ /.well-known/acme-challenge/ {
       default_type "text/plain";
       root         /usr/share/jitsi-meet;
    }
    location = /.well-known/acme-challenge/ {
       return 404;
    }
    location / {
       return 301 https://$host$request_uri;
    }
}
```
en:
```
server {
    listen 443 ssl;
    listen [::]:443 ssl;
    server_name {DOMEINNAAM};

    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_ciphers "EECDH+ECDSA+AESGCM:EECDH+aRSA+AESGCM:EECDH+ECDSA+SHA256:EECDH+aRSA+SHA256:EECDH+ECDSA+SHA384:EECDH..."

    add_header Strict-Transport-Security "max-age=31536000";

    ssl_certificate /etc/ssl/{DOMEINNAAM}.crt;
    ssl_certificate_key /etc/ssl/{DOMEINNAAM}.key;
......
```

Zorg ervoor dat er in de 2e sectie listen 443 en niet listen 4443 (of iets anders).
En verwijder de ssl_certficate en ssl_certificate_key regels aangezien we die opnieuw
gaan genereren in de volgende stap!

## SSL
Ik ga er van uit dat je nog geen SSL certificaat hebt, die gaan we in deze stap
genereren. Belangrijk is, is dat je domein (of subdomein) wijst naar jouw server.
Om makkelijk een SSL certficaat te generen gebruiken we Letsencrypt en certbot.

Certbot is een stukje software van Letsencrypt die voor ons het generen en
verifiëren van het SSL certificaat regelt!

Eerst voegen we de certbot repo toe:
```
sudo add-apt-repository ppa:certbot/certbot
```
Vervolgens installeren we certbot en de certbot extensie voor nginx:
```
sudo apt install certbot python-certbot-nginx
```
Nu gaan we een SSL certifcaat genereren met:
```
sudo certbot certonly --nginx
```
Nu krijg je de volgende prompt:
```
Plugins selected: Authenticator nginx, Installer nginx

Which names would you like to activate HTTPS for?
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
1: {DOMEINNAAM}
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Select the appropriate numbers separated by commas and/or spaces, or leave input
blank to select all options shown (Enter 'c' to cancel):
```
Selecteer 1 wacht tot het certificaat gegenereerd is.

Je ziet nu een melding met waar je certificaten staan opgeslagen dat is standaard:
```
    /etc/letsencrypt/live/{DOMEINNAAM}/fullchain.pem
    /etc/letsencrypt/live/{DOMEINNAAM}/privkey.pem
```
Nu voegen we de certificaten toe aan de nginx configuratie:
```
nano /etc/nginx/sites-enabled/{DOMEINNAAM}.conf
```
en bij de 443 voeg de onderste 2 regels toe:
```
server {
    listen 443 ssl;
    listen [::]:443 ssl;
    server_name {DOMEINNAAM};

    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_ciphers "EECDH+ECDSA+AESGCM:EECDH+aRSA+AESGCM:EECDH+ECDSA+SHA256:EECDH+aRSA+SHA256:EECDH+ECDSA+SHA384:EECDH..."

    add_header Strict-Transport-Security "max-age=31536000";

    ssl_certificate /etc/letsencrypt/live/{DOMEINNAAM}/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/{DOMEINNAAM}/privkey.pem;
......
```
Nu is de normale setup van Jitsi klaar!

## NAT

Als jouw server achter een NAT staat moeten we nog 1 klein dingetje configureren.
De server weet namelijk niet wat het buitsenste IP adres is dus dat moet verteld
worden in de config.

Open de relevante config:
```
nano /etc/jitsi/videobridge/sip-communicator.properties
```
En voeg de volgende regels toe:
```
org.ice4j.ice.harvest.NAT_HARVESTER_LOCAL_ADDRESS={IP ADRES VAN JE MACHINE}
org.ice4j.ice.harvest.NAT_HARVESTER_PUBLIC_ADDRESS=<HET BUITENSTE IP ADRES>
```
Verwijder de lijn die er zo uitziet:
```
org.ice4j.ice.harvest.STUN_MAPPING_HARVESTER_ADDRESSES=meet-jit-si-turnrelay.jitsi.net:443
```

## Conclusie

We hebben nu een Jitsi server gebouwd waarmee je zelf kan videobellen! Dit soort
servers opzetten en onderhouden is een van de dingen waar we ons mee bezig willen
houden bij onze hackerspace. Ook geïnteresseerd? Vul dan onze enquête in!
