---
title: AM-radio bouwen
author: Sander
date: 2020-09-24
layout: blog
redirect_from:
    - /am-radio-bouwen/
    - /en/am-radio-bouwen/
    - /projects/am-radio-bouwen/
header: /uploads/am-radio-box.jpg
---
Donderdag avond 24 september geven Sander en Raoul een workshop voor het maken van je eigen kortegolf AM-radio. Met een AM-radio kan je internationale radiozenders van over de hele wereld ontvangen. Op deze avond zal je alles leren over de natuurkunde achter amplitudemodulatie en zal je zelf een werkende radio in elkaar solderen.

![](/uploads/am-radio.webp)

We gebruiken voor deze workshop een al bestaand bouwpakket. Mocht het niet lukken om de radio tijdens de avond af te maken kan je het altijd nog thuis afmaken of gewoon de volgende keer mee nemen.

The Space heeft 15 radio's ingekocht uit Duitsland, deze kan je overnemen voor €18,50. Geef dit aan op het aanmeldformulier, OP=OP. Mochten de radio's van ons op zijn, dan kan je het bouwpakket [online uit Duitsland hier](https://www.franzis.de/maker/bausaetze/das-franzis-roehrenradio-zum-selberbauen-bausatz) voor €25,95 bestellen (5 dagen verzendtijd).

Mocht dit te laat zijn, dan kan je voor ~~€35,45~~ €25,49 het zelfde pakket kopen [bij Conrad uit Nederland](https://www.conrad.nl/p/bouwpakket-franzis-verlag-das-franzis-rohrenradio-zum-selberbauen-67041-vanaf-14-jaar-1954010).

The Space heeft een beperkt aantal bouwpakketten en AA-batterijen beschikbaar tegen kostprijs, geef het aan op het aanmeldformulier als je gebruik wil maken van een pakket van The Space!

De Nederlandstalige handleiding van de AM-radio kan je [hier downloaden](https://drive.google.com/file/d/1QBjpfltScc0Kzqybfy-85IaP_iWSUv69/view).
