---
layout: blog
title: "Een duik in obscure besturingssystemen: TempleOS"
author: Jelle
date: 2020-07-02
header: /uploads/templeos.png
redirect_from:
    - /news/een-duik-in-obscure-besturingssystemen-templeos/
    - /en/news/een-duik-in-obscure-besturingssystemen-templeos/
---
Vanzelfsprekend zijn we allemaal (in bepaalde mate) bekend met Windows, macOS, en
Linux. De onderlinge verschillen, sterktes, zwaktes, we hebben ze al vaak voorbij horen komen.

Daarom gaan we het nu hebben over TempleOS, een uniek besturingssysteem geschreven
in een unieke programmeertaal met een nogal interessante oorsprong en uitzonderlijke
opvattingen. Allemaal gemaakt door één man: Terry Davis.

## Terry Davis
Terry Davis is in 1992 afgestudeerd aan de Arizona State University met een
GPA van 3.63. Voor de mensen die niet in de Angelsaksische sferen wonen, dat
betekent een gemiddeld gewogen tentamencijfer van 7,56. Zijn IT carrière begon
al iets eerder in 1990 toen begon hij te werken voor Ticketmaster als programmeur.

In 1996 begon hij manische episoden te krijgen en leidde dit tot opnames.
Uiteindelijk is hij gediagnosticeerd met schizofrenie en leefde hij van
arbeidsongeschiktheidsuitkeringen bij zijn ouders in Las Vegas. In deze jaren
heeft hij aan verschillende projecten gewerkt. Zijn grootste project, TempleOS,
kostte hem 12 jaar.

## Visie voor TempleOS

> "It's fun to have access to everything. When I was a teenager, I had a book,
Mapping the Commodore 64, that explained every memory location of the machine.
I liked to copy the ROM inRAM and browse through the ROM variables of BASIC.
Everyone hit the hardware ports directly."

Zijn visie en eerste reden voor TempleOS is te vergelijken met een moderne
Commodore 64. Makkelijk te begrijpen en goed mee te werken. Het kan daarmee
worden gezien als een educatief platform voor programmeer experimentjes.
De programmeur heeft direct toegang tot de hardware en kan er alles mee doen,
er worden geen beperkingen opgelegd. Het is daarom ergens een goed middel om de
complexiteit van het programmeren voor moderne computers te laten zien en
programmeurs lekker hun gang te laten gaan.

Een tweede motivatie voor het maken van TempleOS was een stuk persoonlijker.
De opdracht tot het maken van dit besturingssysteem zou komen van God.
Net als de tempel van Solomon moest dit een plaats worden waar offers gemaakt
konden worden en waar God om raad kon worden gevraagd. Alle specificaties zoals
de 640x480 resolutie en 16-kleur display waren expliciet door God gegeven.

![](/uploads/templeos_screenshot.png)

## Shell

> “If you guys have ideas for things to do, let me know. I probably will ignore them.”

Los van de x64 assembly op de wat lagere niveaus is TempleOS volledig geschreven
in HolyC. Volgens Davis was HolyC minder dan C++, maar meer dan C. Wat verrassend
is, is dat dezelfde programmeertaal ook gebruikt wordt voor de shell. Inderdaad,
je kan in de shell commando’s uitvoeren door in HolyC te tikken waarna het
direct in de compiler wordt gegooid.

Rekenmachine? Niet nodig! De shell is een rekenmachine. Schrijf je op de
command line 4+4, dan geeft het systeem 8 als antwoord.

Autocomplete? Deze zit overal in het systeem. Te allen tijde wanneer de gebruiker
op Ctrl-F1 drukt komt er een lijst tevoorschijn met alle mogelijk aanvullingen.
Dit geldt niet alleen voor bestandsnamen, maar het werkt ook met symbolen.
Alle broncode is geïndexeerd en kan er worden gesprongen naar iedere functie
vanuit iedere mogelijke plek, zowel in de shell als ieder programma in het OS.

Met de Type() functie kunnen er files laten worden zien. Vanzelfsprekend werkt
die met tekst, zoiets hebben we elders hebben gezien. Maar wat deze shell uniek
maakt is dat Type() ook kan worden gebruikt om .BMP files direct te laten zien
in de shell. Dit brengt een interessante kwestie, waarom zouden shells puur
voor tekst bedoeld zijn? Waarom niet meer soorten bestanden?

## DolDoc
Een ander interessant aspect van TempleOS is dat het in tegenstelling tot Unix
alles opslaat in DolDoc format. DolDoc is echter niet alleen voor tekst, er
kunnen afbeeldingen, 3D meshes direct worden opgeslagen. Macro’s zijn ook mogelijk,
hyperlink commands die aan de slag gaan zodra je er op klikt. Een menu is dus
makkelijk gemaakt, je maakt gewoon een nieuw tekstdocument en zet er links in.

![](/uploads/templeos_shell.png)

HTML, JSON, XML, scripts, en tekstbestanden worden allemaal vervangen door één
verenigde hypertext representation.

Op elk moment kan er via Ctrl-R een resource editor tevoorschijn komen waarmee
je kan tekenen. De sprites worden direct in het document gezet en kan je er naar
refereren met tags. Er is dus geen apart paint programma nodig, want deze
functionaliteit is gewoon overal en altijd beschikbaar.

## HolyC

> “In TempleOS, my compiler will put together multiple characters in a character
constant. We don't do Unicode. We do ASCII--8-bit ASCII, not 7-bit ASCII;
7-bit signed ASCII is retarded."

De programmeertaal lijkt erg op C, maar heeft een aantal aantoonbare verschillen.
Er is geen main() in TempleOS programma’s. Alles wat je op top-level scope schrijft
wordt uitgevoerd terwijl het door de compiler gaat. Elk stuk code in TempleOS
wordt JIT (just in time) compiled.

Een functie kan een tag krijgen door middel van de #help_index compiler directive
waarna het automatisch in de documentatie op de juiste plek verschijnt zonder dat
het proces moet worden gerebuild. Gewoon compilen en de documentatie update vanzelf.

Ook biedt HolyC een #exe compiler instructie. Deze kan worden gebruikt om
externe commands te gebruiken en hun output terug te geven aan de broncode.
Op deze manier kan men functionaliteiten implementeren waar gewoonlijk macros
of ondersteuning van een compiler voor nodig is.

![](/uploads/templeos_metadata.png)

## Hardware en beveiliging

> “Linux is a semi-truck with 20 gears to operate. Windows is more like a car.
TempleOS is a motorbike. If you lean over too far, you'll fall off. Don't do that.”

Naast de essentiële onderdelen die een pc een pc maakt, ondersteunt TempleOS
geen andere hardware. Er is geen ondersteuning voor grafische kaarten of
geluidskaarten. Wat wellicht nog wel het meest opvallende is, is dat TempleOS
geen netwerk ondersteunt.

Memory protection is ook geen optie in TempleOS: alles draait op ring 0, het
hoogste rechtenniveau. Ruimte voor fouten is er dus niet echt en dus kan een
fout makkelijk voor crashes zorgen.

## Tot slot
TempleOS is wellicht niet het meest praktische besturingssysteem, maar het laat
zeker unieke ideeën en opvattingen zien. Niet ieder idee hoeft een succes te zijn
en puur omdat het geschreven is door één iemand in een span van 12 jaar betekent
niet dat er niets te ontdekken valt.

TempleOS is een voorbeeld van passie en toewijding van iemand aan zijn vakmanschap.
Meer dan dat hoeft het ook eigenlijk niet te zijn.
