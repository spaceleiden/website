---
title: Klimaatstation Sensor
author: Raoul
date: 2020-09-03
layout: blog
redirect_from:
    - /klimaatstation/
    - /en/klimaatstation/
    - /projects/klimaatstation/
header: /uploads/climate-sensor.jpg
---

Tijdens de eerste donderdag waarop we open gaan zullen sommige van ons gaan werken aan een binnenklimaatsensor.

Met deze sensor kun jij de temperatuur, luchtvochtigheid en CO2 van een kamer meten. De data hiervan gaan we opslaan zodat we een tijdspanne hebben waarover we onze data kunnen inzien. Hier kan je vervolgens conclusies uit trekken over de leefbaarheid van je eigen ruimtes.

Is de temperatuur te hoog? Pak er een ventilator bij.
Te hoge luchtvochtigheid? Misschien is het tijd voor een vochtvanger?
Wordt de CO2 waarde in je huis te hoog? Tijd voor een open raampje!

Kortom een klein leuk project om mee te beginnen.

## Benodigdheden
Voor dit project heb je een paar kleine sensoren nodig. Deze kun je bestellen bij bijvoorbeeld TinyTronics. De benodigdheden voor donderdag zijn als volgt:

- [Wemos D1 Mini V3 - ESP8266](https://www.tinytronics.nl/shop/nl/platforms/wemos-lolin/main-boards/wemos-d1-mini-v2-esp8266-12f-ch340)
- [DHT22 Temperatuur en luchtvochtigheid sensor](https://www.tinytronics.nl/shop/nl/sensoren/temperatuur-lucht-vochtigheid/dht22-thermometer-temperatuur-en-vochtigheids-sensor)
- [MQ135 Gas Sensor](https://www.tinytronics.nl/shop/nl/sensoren/temperatuur-lucht-vochtigheid/mq-135-gas-sensor-module)
- Micro usb kabel
- Laptop (om code op te kunnen schrijven en de Wemos te poweren)

Optioneel zou je er nog een batterij bij kunnen kopen om de gehele setup ergens neer te kunnen leggen zonder dat je stroom nodig hebt. Maar voor vanavond zul je deze niet nodig hebben.

Meer informatie over hoe je dit project kunt doen is te vinden [op GitLab](https://gitlab.com/spaceleiden/project-indoor-climate-wemos).

![](/uploads/climate-sensor.jpg)
