---
title: Game programmeren op hardware
author: Niels
date: 2020-09-10
layout: blog
redirect_from:
    - /game-programmeren-op-hardware/
    - /en/game-programmeren-op-hardware/
    - /projects/game-programmeren-op-hardware/
header: /uploads/uno-r3.JPG
---

Deze week maken we een fysieke videogame op hardware. Compleet met schermpje, knopjes en code.

Het wordt een leuk project, onder leiding van Niels. De moeilijkheid van dit project is gemiddeld.

## Benodigdheden
Voor dit project heb je een paar kleine onderdelen nodig. Deze kun je bestellen bij bijvoorbeeld TinyTronics. De benodigdheden voor donderdag zijn als volgt:

- 1 x [uno r3 compatible usb](https://www.tinytronics.nl/shop/nl/arduino/main-boards/uno-r3-compatible-usb-b)
- 1 x [lcd keypad shield](https://www.tinytronics.nl/shop/nl/display/lcd/lcd-keypad-shield)
- 1 x [usb-B kabel](https://www.tinytronics.nl/shop/nl/kabels/usb-b-usb-kabel-30cm)
- Laptop (om code op te kunnen schrijven en de Arduino te poweren)

Koop de benodigdheden zelf en weet zeker dat je mee kunt doen! Wil je liever op locatie een set kopen? Dat kan maar hier geld wel OP=OP we hebben slechts een paar sets gekocht.

Tot donderdag 10 september!

![](/uploads/arduino-lcd-keypad.jpg)

![](/uploads/uno-r3.JPG)
