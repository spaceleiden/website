---
layout: blog
title: "Redden wat er te redden valt"
author: Niels
date: 2020-07-13
header: /uploads/harddisk.jpg
redirect_from:
    - /news/redden-wat-er-te-redden-valt/
    - /en/news/redden-wat-er-te-redden-valt/
---
Stel je hebt je scriptie op een externe hardschijf staan die je overal mee naar
toe neemt. Je hebt de boel net opgeslagen en je wil het enkel nog even aan je
ouders laten lezen om te controleren op spelfouten. Je springt op je fiets naar
huis maar onderweg rij je over een losse steen in de weg waardoor je met fiets
en al achterover valt. Jij op je tas waar je hardeschijf in zit, vanaf dat moment
begint hij rare dingen te doen. Windows herkent hem niet meer en de paar keren
dat hij alsnog in je verkenner verschijnt krijg je de melding dat je je schijf
moet formatteren. Is het dan echt over en uit?

Ten eerste, foei dat jij je scriptie enkel op een externe hardeschijf hebt staan.
Maak een backup. Echter nemen gedane zaken geen keer en als je hardeschijf kuren
vertoont en er staan belangrijke bestanden op dan is het je er alles aan gelegen
om deze te herstellen.

Het is nu eerst belangrijk om vast te stellen wat er precies kapot is, en omdat
we graag laag bij het metaal willen spelen als het gaat om kapotte hardeschijven
gebruiken we Linux en niet Windows.

## Vaststellen hoe kapot de boel is
Lanceer een shell in Linux (geen zorgen als je geen shell ervaring hebt, alles
wordt rustig uitgelegd). Ten eerste willen we kijken welke opslag devices
aanwezig zijn **zonder dat de kapotte hardeschijf is aangesloten**. Zo kunnen
we een 0 meting doen. Hiervoor gebruiken we het commando `lsblk`

```
niels@workstation$ lsblk
NAME                  MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
loop0                   7:0    0 161,4M  1 loop  /snap/gnome-3-28-1804/128
sda                     8:0    0 931,5G  0 disk
└─sda1                  8:1    0 931,5G  0 part  /media/data
sdb                     8:16   0 238,5G  0 disk
├─sdb1                  8:17   0   512M  0 part  /boot/efi
├─sdb2                  8:18   0   732M  0 part  /boot
└─sdb3                  8:19   0 237,3G  0 part
  └─sda3_crypt        253:0    0 237,3G  0 crypt
    ├─mint--vg-root   253:1    0 236,3G  0 lvm   /
    └─mint--vg-swap_1 253:2    0   976M  0 lvm   [SWAP]
sdc                     8:32   0   1,8T  0 disk
└─sdc1                  8:33   0   1,8T  0 part  /media/niels/Backup
nvme0n1               259:0    0 238,5G  0 disk
├─nvme0n1p1           259:5    0   529M  0 part
├─nvme0n1p2           259:6    0   100M  0 part
├─nvme0n1p3           259:7    0    16M  0 part
└─nvme0n1p4           259:8    0 237,9G  0 part
```

Je ziet een aantal devices, als je `loop` devices ertussen hebt staan dan kan je
die negeren. Deze liggen buiten de scope van deze post. Je zult zeer waarschijnlijk
een schijf zien als `sda` of `sdb`. Daaronder (verder in de structuur) staan schijven
als `sda1` en `sdb1`. `sda` is een schijf en `sda1` is de eerste partitie op die schijf,
`sda2` de tweede enzovoorts.

Sluit nu de externe hardeschijf aan en kijk welke schijf erbij komt:

```
niels@workstation$ lsblk
NAME                  MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
loop0                   7:0    0 161,4M  1 loop  /snap/gnome-3-28-1804/128
sda                     8:0    0 931,5G  0 disk
└─sda1                  8:1    0 931,5G  0 part  /media/data
sdb                     8:16   0 238,5G  0 disk
├─sdb1                  8:17   0   512M  0 part  /boot/efi
├─sdb2                  8:18   0   732M  0 part  /boot
└─sdb3                  8:19   0 237,3G  0 part
  └─sda3_crypt        253:0    0 237,3G  0 crypt
    ├─mint--vg-root   253:1    0 236,3G  0 lvm   /
    └─mint--vg-swap_1 253:2    0   976M  0 lvm   [SWAP]
sdc                     8:32   0   1,8T  0 disk
└─sdc1                  8:33   0   1,8T  0 part  /media/niels/Backup
sde                     8:64   0   1,8T  0 disk
└─sde1                  8:65   0   1,8T  0 part
nvme0n1               259:0    0 238,5G  0 disk
├─nvme0n1p1           259:5    0   529M  0 part
├─nvme0n1p2           259:6    0   100M  0 part
├─nvme0n1p3           259:7    0    16M  0 part
└─nvme0n1p4           259:8    0 237,9G  0 part
```

Zoals je kan zien is er een schijf bijgekomen, namelijk: `sde`. Als je schijf
niet verschijnt is het aan te raden om het nog een aantal keren te proberen.
Probeer andere USB poorten, probeer je behuizing uit elkaar te halen en de harde
schijf aan te sluiten met een SATA naar USB adapter. Als hij echter hier niet
tevoorschijn komt is het einde oefening. Zie je hem wel dan kunnen we verder
naar de volgende stap.

## De simpelste manier om een backup te maken
De makkelijkste manier op ieder Linux systeem om een schijf te backuppen is door
het commando dd te gebruiken. Dit commando neemt een bestand en maakt een bit
voor bit naar een uitvoer bestand. Aangezien alles in Linux een bestand is
(dus ook de harde schijven) kunnen we dit commando gebruiken om een backup
te maken van de hardeschijf.

Als we ervan uit gaan dat `sde` de hardeschijf is dan kunnen we de backup starten
door het volgende commando:

```
dd if=<PAD NAAR SCHIJF> of=<PAD NAAR BACKUP>
```
Oftewel:

```
dd if=/dev/sde of=/media/data/hdd_backup.dd
```

Het jammere is nu alleen dat we niet zien wat er gebeurt, dat is prima als we
een USB stick hebben van 512mb maar als we een externe hardeschijf hebben
van 500gb dan duurt dat wel even. Gelukkig kunnen we de hulp van een ander
commando inschakelen namelijk `pv`. Dit commando neemt de input van een commando
en stuurt dat door naar een ander, vervolgens print het statistieken over
hoeveel data er door de "pijp" heen gaat. We kunnen dit op de volgende
manier gebruiken:

```
dd if=/dev/sde | pv | dd of=/media/data/hdd_backup.dd
```

We gebruiken eerst dd om de input file (`/dev/sde`) te lezen. Zonder een ouput
file mee te geven stuurt `dd` de data naar `stdout`. We kunnen echter een `|` teken
gebruiken dan vangen we alles wat naar `stdout` gestuurd wordt en sturen we dat
naar het commando dat we daarna invoeren, `pv` dus. Vervolgens passen we hetzelfde
geintje toe na `pv` en sturen de data door naar een 2e `dd` commando die de output
leest en dat vervolgens in de output file stopt.

## De andere manier om een backup te maken
Nu kan het zijn dat je hardeschijf zo naar z'n grootje is dat sommige delen
niet meer te lezen zijn, of misschien zijn ze de ene keer wel te lezen en de
andere keer weer niet (het blijft mechanica in zo'n schijf). We kunnen dan gebruik
maken van een commando dat ddrescue heet. Deze doet technisch gezien hetzelfde
als het dd commando hierboven echter probeert de niet leesbare delen later nog
eens om te proberen de gaten in te vullen.

Om een backup te maken met `ddrescue` kun je het volgende commando gebruiken:

```
ddrescue -d -r3 <PAD NAAR SCHIJF> <PAD NAAR BACKUP> logfile.log
```
Oftewel:
```
ddrescue -d -r3 /dev/sdc /media/data/hdd_backup.dd logfile.log
```

```
niels@workstation$ ddrescue -d -r3 /dev/sdc /media/data/hdd_backup.dd logfile.log
GNU ddrescue 1.23
Press Ctrl-C to interrupt
Initial status (read from mapfile)
rescued: 0 B, tried: 23264 kB, bad-sector: 0 B, bad areas: 0

Current status
     ipos:   15728 kB, non-trimmed:   23526 kB,  current rate:       0 B/s
     opos:   15728 kB, non-scraped:        0 B,  average rate:       0 B/s
non-tried:  144115 TB,  bad-sector:        0 B,    error rate:       0 B/s
  rescued:        0 B,   bad areas:        0,        run time:      5m 55s
pct rescued:    0.00%, read errors:        4,  remaining time:         n/a
                              time since last successful read:         n/a
Copying non-tried blocks... Pass 5 (forwards)
```

Laat dit draaien tot het klaar is en je hebt een keurig backup van je schijf.

## Hoe kom ik nu bij m'n bestanden?

Dat is een goeie vraag, wat we net gedaan hebben is een kopie gemaakt van de
volledige schijf, je bestanden zitten daar in we moeten Linux enkel vertellen
dat hij de file als een hardeschijf moet behandelen.

Dat doen we met `losetup`, hiermee is het mogelijk om een loop device te maken.
Loop devices doen alsof ze een device zijn terwijl ze eigenlijk gewoon een file
zijn. Heel veel programma's, waaronder het programma dat we gebruiken om de
backup mee te mounten willen een device in plaats van een file.

Om het loop device te maken kun je het volgende commando gebruiken:
```
losetup <PAD_NAAR_HET_TE_MAKEN_DEVICE> <PAD NAAR BACKUP>
```
Oftewel
```
losetup /dev/loop100 /media/data/hdd_backup.dd logfile.log
```
Nu kunnen we fdisk gebruiken om de structuur van de backup te bekijken:
```
sudo fdisk -l /dev/loop100
```

```
niels@workstation$ fdisk /dev/loop100 -l
Disk /dev/loop100: 124,2 MiB, 130048000 bytes, 254000 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 1D577CEA-F4A6-4E00-A6DE-59A82D960588

Device         Start    End Sectors  Size Type
/dev/loop100p1  2048 253951  251904  123M Microsoft basic data
```

Zoals je ziet herkent fdisk de partitie die op de schijf stond als `/dev/loop100p1`,
nu kunnen we deze mounten met het volgende commando:
```
mount <PAD_NAAR_DEVICE> <PAD_NAAR_MOUNT_DIRECTORY>
```
Oftewel:
```
mount /dev/loop100p1 /media/niels/backup
```
(De mount directory moet wel al bestaan dus maak deze eerst).

Als het goed is is je backup nu beschikbaar onder de mount directory! Nu is het
enkel en alleen zaak om deze bestanden van het mountpoint te kopiëren naar een
andere hardeschijf. En als je dan toch lekker bezig bent, gooi ze dan ook gelijk
op een clouddienst.

Ben je benieuwd naar meer van dit soort workshops en informatie? We willen een
hackerspace vormen waar je meer kan leren en je interesses en hobby's gestimuleerd
 worden. We willen een plek zijn voor makers, denkers en geïnteresseerden in de
 digitale wereld. Lijkt het jou interessant? Neem een kijkje op onze site of join
 onze discord!
