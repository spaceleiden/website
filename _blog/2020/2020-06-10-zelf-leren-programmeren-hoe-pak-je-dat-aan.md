---
layout: blog
title: "Zelf leren programmeren: hoe pak je dat aan?"
author: Raoul
date: 2020-06-10
header: /uploads/leren_programmeren.jpg
redirect_from:
    - /news/zelf-leren-programmeren-hoe-pak-je-dat-aan/
    - /en/news/zelf-leren-programmeren-hoe-pak-je-dat-aan/
---
Programmeren is niet moeilijk. Het is gemakkelijk zelf aan te leren, en het
internet staat vol met hulpmiddelen en communities om je te helpen. Er zijn tal
van resources online te vinden waar je gratis aan de slag kan, en binnen no
time kun je zelf al een aardige webpagina maken. Maar, misschien dankzij al
deze mogelijkheden, is het moeilijk te zien waar te beginnen. Er zijn veel
programmeertalen, voor elk project wel een andere, en op de ene webpagina wordt
een taal afgeschreven, waar deze op een andere pagina wordt verheerlijkt.
Hoe weet je wat je moet doen, en waar je moet beginnen? In deze blogpost
helpen wij je op weg.

## Waarom?
De eerste vraag is natuurlijk: waarom zou je leren programmeren? Wat voegt het
toe aan jouw gebruik en begrip van het internet, of aan je cv? Eigenlijk is het
antwoord bijna altijd: veel. Programmeren is momenteel voor iedereen relevant.
Dagelijks gebruiken we meerdere verschillende apparaten, waaronder smartphones,
computers, smart apparatuur zoals televisies en thermostaten, en zelfs auto’s
bevatten tegenwoordig een aardige computer met gebruikersinterface.

Om te begrijpen hoe deze apparaten werken, of soms waarom ze niet werken, is het
nuttig om te weten wat er binnenin gebeurt. Hoe ‘denkt’ zo’n machine, en wat
voor input of output is wel of niet mogelijk? Maar ook: wat doet de machine met
jouw informatie? Waar blijft je data, en hoe wordt het gebruikt? Als je wat
ervaring hebt met het zelf maken van software (of hardware), heb je meer begrip
van de mogelijkheden en limieten van de systemen die jij dagelijks gebruikt.
Zo leer je bijvoorbeeld te begrijpen wat voor data nodig is voor het functioneren
van een applicatie, en welke van jouw data niet per se verzameld had hoeven worden.

Een andere reden om te leren programmeren is de creativiteit die je er in kwijt kan.
Het biedt veel mogelijkheden, en je kan alle applicaties maken die jij maar wilt.
Mis jij een bepaalde app in je dagelijks leven? Of heb jij het idee voor het gat
in de markt van een nieuwe app? Bouw ‘m zelf! Zo kun je alles aanpassen naar jouw
eisen, en je eigen uitdagingen vinden.

Daarnaast kan een begrip of beheersing van programmeerskills je verder helpen op de
arbeidsmarkt. Ook al word je geen die-hard programmeur, als jij begrip en ervaring
kan tonen met programmeertalen, SQL of hardware heb je al een streepje voor op de
arbeidsmarkt. Zelfs al kun je zelf geen applicatie coden, kan het nog steeds handig
zijn om de globale werking van een applicatie te kennen, de limieten en mogelijkheden
te begrijpen, en een redelijke basis te hebben van computational thinking. Dit stelt
je in staat om te communiceren tussen de business en de ICT kant van een bedrijf, en
de (soms grote) kloof te overbruggen.

## Maar hoe?
Waar leer je dan programmeren, en hoe gaat dat? Kan ik het zelf? Hoe lang duurt het?
Er zijn zoveel opties en zoveel manieren en talen om te programmeren, dat het voor
beginners soms overweldigend kan zijn. De beste tip die wij kunnen geven is gewoon
iets kiezen en beginnen. Begin met python of javascript, dan leer je gemakkelijk
de basis van variabelen, arrays, loops, functies, klassen en objecten. Wanneer
je deze basis een beetje onder de knie hebt, wordt het ook makkelijker om de
verschillen tussen alle programmeertalen te begrijpen, en kun je altijd nog een
nieuwe taal leren als die beter blijkt te passen bij wat jij wil gaan maken.

En waar leer je dit? Er zijn veel online resources om te leren programmeren in
de browser. Probeer www.codecademy.com voor gratis cursussen voor verschillende
talen en concepten. www.htmldog.com biedt een goede html, css en javascript
cursus om je eerste webpagina te maken. Daarnaast zijn er betaalde cursussen,
kijk daarvoor eens op www.udemy.com, of www.coursera.com. Hier betaal je per cursus.

## Wat kun je maken?
Wanneer je de basis van het programmeren onder de knie hebt, opent zich een wereld
van mogelijkheden voor je eerste projecten. Maar waar haal je inspiratie vandaan?
Kijk om je heen naar wat voor websites jij dagelijks gebruikt, en probeer die na
te maken. Je zal niet zomaar de volgende Facebook bouwen, maar het immiteren van
een voorbeeld kan je veel leren over de werking daarvan.

Of maak iets wat jij in je dagelijks leven kan gebruiken, voor je hobby’s of werk.
Is er een simpele applicatie die je daarbij zou kunnen helpen?
Maak een persoonlijke website, met jouw ervaringen. Dit kan een professionele
website zijn om jezelf op de kaart te zetten voor je werk, of een persoonlijke
blog over jouw hobby. Zo maak je jouw eigen online portfolio.
Of wil je iets fysieks maken? Een sensor die meet wanneer je kamerplant dorst heeft,
of een sensor die jou op de hoogte stelt wanneer er post in je brievenbus belandt?
Met behulp van bijvoorbeeld een Raspberry Pi, een paar sensoren en wat python
code kom je al snel een heel eind.

## Een hackerspace
Online zijn veel resources te vinden voor het maken van je eigen projecten.
Je vindt er hulp, voorbeelden, en handleidingen voor elk project. Maar niet
alleen online, ook in je eigen omgeving kun je hulp vinden. Wij willen in Leiden
een hackerspace beginnen waar we gelijkgezinden kunnen ontmoeten, kennis en tools
kunnen delen en samen toffe projecten kunnen maken. Heb jij interesse in zo’n
omgeving? Vul dan onze enquête in, volg ons op social media en lees onze andere blogposts!
