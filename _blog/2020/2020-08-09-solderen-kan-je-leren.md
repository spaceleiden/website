---
layout: blog
title: "Solderen, kan je leren!"
author: Raoul
date: 2020-08-09
header: /uploads/solderen.jpg
redirect_from:
    - /news/solderen-kan-je-leren/
    - /en/news/solderen-kan-je-leren/
---
Iedereen heeft ze wel, een afgetrapt paar oortjes die eigenlijk net te vies zijn
maar die OH ZO CHILL zijn. Niks is erger dan dat deze oortjes het eindelijk,
zoals de toekomst is van alle oortjes, begeven. Draadbreuk, afgescheurde oortjes,
je hoort het allemaal.

Wat nou als ik je vertel dat dit niet het einde hoeft te zijn voor je oortjes?
Tuurlijk, op Ali Express zijn ze voor de helft van de prijs, maar daar moet je
dan wel 5 weken op wachten… Wat nou als je ze nu al kan repareren? Wat nou als
alles wat je nodig hebt een kleine soldeerbout is?

Solderen is eigenlijk niks meer dan het aan elkaar vastmaken van kleine draden
of elektronische onderdelen.

## Wat is nou precies solderen?
Ieder elektronisch apparaat bestaat uit verscheidene draden en elektronische
onderdelen, in het geval van je oortjes zijn de draden duidelijk zichtbaar en
zitten deze aangesloten aan kleine speakers. In andere gevallen is er een circuit
board waar alle elektronische onderdelen zoals LEDs, weerstanden, transistors etc
op zijn vastgemaakt.

Solderen is het process van het vastmaken van deze elektronische onderdelen met
soldeertin. Soldeertin is een metaal legering bestaande uit tin (duh) en
verscheidene andere materialen zoals koper, zilver of hars. Oudere vormen van
soldeertin bevatten ook nog veel lood, dit mag inmiddels niet meer al kom je het
nog wel regelmatig tegen. Omdat het een mix is van verschillende metalen is
afzuiging of ventilatie van de ruimte waarin je soldeert ook super belangrijk.
Mocht je met lood soldeertin werken: was dan ook regelmatig je handen, je wilt
jezelf immers niet per ongeluk vergiftigen.

![](/uploads/solderen_schema.jpg)

Okay solderen is dus een simpel iets maar wat gebruik je dan?
Er zijn verschillende soorten soldeer gereedschap die je kan gebruiken.
De makkelijkste en goedkoopste variant is een soldeerbout.

## Soldeerbout
Soldeerbouten zijn een stuk handgereedschap die direct in een stopcontact geplugd
worden. De meeste soldeerbouten hebben maar één temperatuur en die ligt rond de
330 tot 350 graden celsius. Omdat de soldeerstations maar één temperatuur hebben
zijn ze relatief goedkoop. De goedkoopste varianten hebben geen verwisselbare punt.
Als je wel een variant hebt die verschillende soldeer punten kan gebruiken zorg dan
dat je soldeerbout eerst is afgekoeld voordat je van soldeerpunt wisselt.

## Soldeerstation
Een soldeerstation is een meer geavanceerdere versie van de basis soldeerbout.
Als je veel aan het solderen bent is het fijn om meer flexibiliteit en controle
te hebben over wat en hoe je precies soldeert. Het voornaamste voordeel van een
soldeerstation is dat je de temperatuur zeer precies in kan stellen. Bij sommige
projecten wil je misschien met een lagere temperatuur solderen dan bij andere
projecten. Een ander voordeel is dat je ten alle tijden kan zien hoe warm de
soldeerbout momenteel is. Bij praktisch alle soldeerstations kan je de
soldeerpunt wisselen.

## Hot air station
Een hot air station is een mini haardroger on steroids. Ideaal voor kleine taken
die je snel wilt afronden. Het laat je snel componenten loshalen en vastzetten.
Een flink nadeel aan een hot air station is dat deze relatief duur is en vaak
ook groot en lastig te verplaatsen. Vaak wordt dit meer in een professionele
setting gebruikt waar veel gesoldeerd wordt. Een hot air station heeft geen
soldeerpunten, er zijn wel verschillende vormen voor de blazer, groot klein of plat.

## Soldeerpunten
Aan het eind van de meeste soldeerstations zit dus een metalen punt die ook wel
de soldeerpunt genoemd wordt. Deze punt is bij veel modellen verwisselbaar. Er
zijn verschillende vormen van de punt die je kunt gebruiken. Hoe dikker de punt
hoe groter het oppervlak wat je kan solderen. Vaak gebruik je dikkere punten voor
bijvoorbeeld het solderen van draden en de dunnere punten voor het vast solderen
van elektronische componenten op een circuit board.

Onderhoud van een soldeerpunt is wel belangrijk. Het is belangrijk om je soldeerpunt
te coaten met soldeertin. Dit process zorgt ervoor dat de warmte beter overgedragen
wordt, maar het zorgt er ook voor dat de punt beschermd wordt tegen oxidatie.
Je zou je soldeerpunt moeten coaten voordat je begint met solderen en wanneer
je klaar bent om ervoor te zorgen dat deze het langste mee gaat.

![](/uploads/solderen_punten.jpg)

## Helping hand of derde hand

Dit is een klein apparaat wat 2 of meer alligator klemmen heeft en je helpt om
onderdelen vast te houden die je probeert te solderen. Veel van deze apparaten
hebben ook een vergrootglas.

![](/uploads/solderen_helping_hand.jpg)

## Spons of metaal spons
Een soldeerspons, zowel een kleine natte spons als een metalen “spons” worden
gebruikt om de soldeerpunt schoon te houden tijdens het solderen. Je haalt de
oxidatie weg die op de punt gevormd wordt. Je kunt een normale natte spons
gebruiken maar dit kan de levensduur van je punt aantasten omdat deze je punt
aan verschillende extreme temperaturen blootstelt. Het beste is om een metalen
spons te gebruiken. Vaak is dit een messing spons.

![](/uploads/solderen_spons.jpg)

## Meer weten of proberen?
Vanaf september zullen we iedere donderdagavond in de ruimte van Technolab Leiden
aanwezig zijn voor workshops, projectjes of gewoon gezelligheid. Wil je nieuwe
dingen leren of wil je gewoon lekker werken met gereedschap wat je thuis niet
hebt? Kom bij ons langs, dan zullen we gezellig samen aan de slag gaan!

Ons programma komt binnenkort online, maar alvast een tipje van de sluier van
dingen die je kan solderen zijn: een radio, speaker, walkie talkie en veel meer!
Tot snel?!
