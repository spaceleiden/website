---
layout: blog
title: "Het belang van backups in een digitale wereld"
author: Raoul
date: 2020-05-13
header: /uploads/backups.jpg
redirect_from:
    - /news/het-belang-van-backups-in-een-digitale-wereld/
    - /en/news/het-belang-van-backups-in-een-digitale-wereld/
---


We leven in een digitale wereld: bijna alles wat we doen, doen we digitaal. Op
onze computers, tablets en smartphones. Veel van onze communicatie bestaat uit
enen en nullen. We chatten, mailen, bellen, schrijven verhalen, verslagen en blogs.
Voor school maken we digitaal ons huiswerk, schrijven scripties en bewaren we
onze werk gerelateerde documenten. Zelfs de foto’s en videos van onze ervaringen,
onze dierbaren, onze kinderen slaan we digitaal op. Daar waar onze ouders en
grootouders herinneringen hebben in de vorm van brieven, boeken en fotoboeken
slaan wij al onze herinneringen op in onze apparaten.

Niets is dan ook erger dan het kwijtraken van een hardeschijf of het stukgaan
van een computer met daarop al je herinneringen. Als dit bij onze ouders en
grootouders gebeurde raakten ze een fysiek iets kwijt iets waar geen kopieën
van zijn. Kopieën, juist dat is iets wat in een digitale wereld makkelijk kan.
Herinneringen, projecten, werk dit hoeft niet kwijt te raken wanneer je laptop
kapot gaat of je telefoon gestolen wordt.

Een goede back up routine zorgt ervoor dat, wat je ook doet, je altijd kopieën
hebt om je herinneringen en harde werk in terug te zoeken. Een backup strategie
kan bestaan uit het opslaan van je gegevens op meerdere plekken, er altijd voor
zorgen dat je een kopie hebt van de bestanden die jij belangrijk vind. Dit kan
door bijvoorbeeld alle bestanden die je belangrijk vindt regelmatig op een externe
harde schijf, usb sticks of een oude computer te zetten. Zo maak je het risico
dat je iets kwijt raakt net ietsjes kleiner.

Een nadeel is wel, die verschillende vormen van backups kunnen kwijtraken, corrupt
raken of zelfs kapot gaan. Het idee van een fysieke backup van je bestanden is
leuk, maar als je bestanden op zowel je laptop als op een harde schijf staan die
beide in dezelfde omgeving aanwezig zijn, heb je nog steeds het risico dat beide
kopieën tegelijk verloren gaan wanneer er bijvoorbeeld brand uitbreekt in je huis.
Naast het enorme verlies van je huis ben je ook nog eens al je digitale werk en
herinneringen kwijt.

Zoiets kan je voorkomen door bijvoorbeeld gebruik te maken van een cloud dienst.
Je data in de cloud, eigenlijk is de cloud niks meer dan een verzameling van
computers gedistribueerd over verschillende plekken waar je je data op kan slaan.
Denk hierbij aan bijvoorbeeld Google Drive, Onedrive, dropbox en vele andere
aanbieders. Sommige gratis, andere betaald. Maar allen zorgen ze ervoor dat jij
geen data kwijt raakt.

Persoonlijk gebruik ik beide methodes, ik neem mijn laptop overal mee naar toe,
door middel van speciale synchronisatie software, synchroniseer ik de bestanden
die ik belangrijk vind naar een vast computer die bij mij thuis staat, hetzelfde
doe ik voor familie en vrienden die dit aan mij vragen. De computer bij mij thuis
die wordt ieder kwartier gesynchroniseerd naar een kleine computer met een harde
schijf die alles opslaat en alles wat het krijgt voor drie maanden bewaard. Zelfs
als de oorspronkelijke computer wel iets heeft weggegooid is er op die manier dus
altijd een backup in de buurt. De laatste stap is dat die kleine computer ieder
uur naar een cloud provider een backup wegschrijft waar alles minstens een jaar
bewaard wordt.

Sommige mensen zouden dit misschien overdreven vinden. En misschien is dat het ook,
helaas gebeurt het vaak genoeg dat familie of vrienden per ongeluk iets hebben
weggegooid, een telefoon zijn kwijtgeraakt of soms maanden later opeens iets missen.
Het feit dat het voor mij dan een kleine moeite is om iets terug te zoeken brengt
mijzelf, mijn familie en vrienden gemoedsrust. Mijn eigen herinneringen en die van
anderen zijn niet zomaar weg.

Opslagruimte fysiek en digitaal zijn tegenwoordig de kosten niet meer. Doe je zelf,
je gezin en je familie een plezier: zorg voor goede backups. Want in deze digitale
wereld is er niks belangrijkers dan de enen en nullen die jouw herinneringen vormgeven.

Wil je meer weten over mijn setup? Stuur ons een bericht, dan wil ik best een blogpost
schrijven over hoe mijn systeem precies in elkaar zit. Welke methode van backups
je ook kiest alles is beter dan een enkel exemplaar op een oude laptop.

Meer weten over ons en The Space? We willen een hackerspace vormen waar je meer
kan leren en je interesses en hobby's gestimuleerd worden. We willen een plek zijn
voor makers, denkers en geïnteresseerden in de digitale wereld. Lijkt het jou
interessant? Neem een kijkje op onze site of join onze discord!
