---
title: Activiteiten Juli / Augustus 2024
author: Raoul
date: 2024-07-09
layout: blog
redirect_from:
- /juli-augustus-2024/
header: /uploads/2024/july_banner.jpeg
---
De agenda voor Juli en Augustus is bekend! In de zomervakantie schakelen we een standje terug, en zullen we elke 3 weken een workshop hebben ipv om de week. 
Maar dat maken we goed in september, want dan hebben we weer een volle maand om het begin van het nieuwe studiejaar te vieren! 
Heb jij nog verzoeknummers of ideeën voor een leuke talk in september? Laat het ons vooral weten!

<!--// more //-->

(English below)

### 04 juli - Projectavond / Arduino cursus 5
Deze week is de vijfde les van de Arduino cursus maar hebben we verder geen programma, de Space is gewoon open om lekker aan de slag te gaan met je eigen project!
Neem je materialen mee en ga lekker aan het werk. Heb je ergens hulp of uitleg bij nodig?
Spreek gerust een van de bestuursleden aan.

### 11 juli - Projectavond / Arduino cursus 6
Deze week is de zesde les van de Arduino cursus maar hebben we verder geen programma, de Space is gewoon open om lekker aan de slag te gaan met je eigen project!
Neem je materialen mee en ga lekker aan het werk. Heb je ergens hulp of uitleg bij nodig?
Spreek gerust een van de bestuursleden aan.

### 18 juli - Projectavond / Arduino cursus 7
Deze week is de zevende les van de Arduino cursus maar hebben we verder geen programma, de Space is gewoon open om lekker aan de slag te gaan met je eigen project!
Neem je materialen mee en ga lekker aan het werk. Heb je ergens hulp of uitleg bij nodig?
Spreek gerust een van de bestuursleden aan.

### 25 juli - D&D / Arduino cursus 8
Deze week is de achtste en laatste les van de Arduino cursus! Maar daarnaast is het weer tijd voor een waanzinnig avontuur. 

25 juli spelen we weer een nieuwe one-shot! Dit is een kort verhaal wat
we in een sessie kunnen afronden. Dit verhaal zal ook geschikt zijn voor mensen die nog nooit Dungeons and Dragons
hebben gespeeld. Heb je geen eigen dobbelstenen? Geen probleem, wij zorgen dat die aanwezig zijn. We zullen ook enkele
character sheets mee nemen waar jullie uit kunnen kiezen, dus je hoeft geen character voor te bereiden.
Om eventuele nieuwe spelers het spel uit te kunnen leggen voor aanvang, zullen wij iets eerder aanwezig zijn dan normaal.

Het is nodig om je te registreren omdat we een beperkt aantal plekken voor deelnemers hebben: [Inschrijven kan hier!](https://forms.gle/Pct3z7z6JBTpkAqN8)

- 18:00 uur: speluitleg voor beginners
- 19:00 uur: start spel

Deze avond zijn we natuurlijk ook open om gewoon met je project aan de slag te gaan, als je dat liever doet. Er zijn
misschien wel wat minder bestuursleden beschikbaar om te helpen, dus houd daar rekening mee als je iets wilt doen waar
je wat hulp en uitleg bij nodig hebt.

### 01 augustus - Projectavond
Deze week hebben we een project avond en is de Space gewoon open om lekker aan de slag te gaan met je eigen project!
Neem je materialen mee en ga lekker aan het werk. Heb je ergens hulp of uitleg bij nodig?
Spreek gerust een van de bestuursleden aan.

### 15 augustus - Projectavond
Deze weken hebben we een project avond en is de Space gewoon open om lekker aan de slag te gaan met je eigen project!
Neem je materialen mee en ga lekker aan het werk. Heb je ergens hulp of uitleg bij nodig?
Spreek gerust een van de bestuursleden aan.

### 22 augustus - LLMs & AI - How to run on your own machine
Deze week vertelt Raoul je wat over LLMs en AI, en hoe je deze op je eigen machine kunt draaien.
ChatGPT, GPT-3, BERT, en vele anderen, hoe werken ze en hoe kun je ze zelf gebruiken?
Deze avond is er ook ruimte om gewoon lekker aan je eigen project te werken, dus neem je spullen mee en ga lekker aan de slag!

### 29 augustus - Opening / flashtalks
De zomervakantie is weer bijna voorbij, iedereen gaat weer aan het werk, of begint aan een nieuw collegejaar.
Om te vieren dat we allemaal weer terug zijn, dat we een mooie zomer hebben gehad, en eens bij te praten over wat we deze zomer hebben meegemaakt,
beginnen we eind augustus met een opening van het nieuwe jaar.

Op **donderdag 29 augustus** nodigen we jullie allemaal uit om langs te komen.
Neem eens iemand mee, een vriend, vriendin, broer of zus, van wie je denkt dat die The Space ook heel leuk zou vinden.

Tijdens deze avond houden we verschillende flash talks, korte presentaties van +- 15 minuten, over verschillende onderwerpen.
Wil je zelf een flash talk geven? Neem dan contact op met het bestuur!

# Programme for Juli and August!
### July 4 - Project evening / Arduino course 5
This week is the fifth lesson of the Arduino course, but we have no further program, the Space is open to get started on your own project!
Bring your materials and get to work. Do you need help or explanation with something?
Feel free to speak to one of the board members.

### July 11 - Project evening / Arduino course 6
This week is the sixth lesson of the Arduino course, but we have no further program, the Space is open to get started on your own project!
Bring your materials and get to work. Do you need help or explanation with something?
Feel free to speak to one of the board members.

### July 18 - Project evening / Arduino course 7
This week is the seventh lesson of the Arduino course, but we have no other program, the Space is open to get started on your own project!
Bring your materials and get to work. Do you need help or explanation with something?
Feel free to speak to one of the board members.

### July 25 - D&D / Arduino course 8
This week is the eighth and final lesson of the Arduino course! But besides that, it's time for an amazing adventure.

July 25th we will play a new one-shot! This is a short story
we can complete in one session. This story will also be suitable for people who have never played Dungeons and Dragons
have played. Don't have your own dice? No problem, we will ensure that they are present. We will also have some
Bring character sheets that you can choose from, so you don't have to prepare a character.
In order to explain the game to any new players before the start, we will be there a little earlier than usual.

It is necessary to register because we have a limited number of places for participants: [Signup here!](https://forms.gle/Pct3z7z6JBTpkAqN8)

- 6:00 PM: game explanation for beginners
- 7:00 PM: Start game

This evening we are of course also open to just get started on your project, if you prefer. There are
there may be fewer board members available to help, so keep that in mind if you want to do something where
you need some help and explanation.

### August 1 - Project evening
This week we have a project evening and the Space is open as usual so you can get started on your own project!
Bring your materials and get to work. Do you need help or explanation with something?
Feel free to speak to one of the board members.

### August 15 - Project evening
This week we have a project evening and the Space is open as usual so you can get started on your own project!
Bring your materials and get to work. Do you need help or explanation with something?
Feel free to speak to one of the board members.

### August 22 - LLMs & AI - How to run on your own machine
This week Raoul tells you something about LLMs and AI, and how you can run them on your own machine.
ChatGPT, GPT-3, BERT, and many others, how do they work and how can you use them yourself?
This evening there is also room to simply work on your own project, so bring your stuff and get started!

### August 29 - Opening / flash talks
The summer holidays are almost over, everyone is going back to work or starting a new academic year.
To celebrate that we are all back, that we had a beautiful summer, and to catch up on what we have experienced this summer,
we start at the end of August with an opening of the new year.

On **Thursday, August 29** we invite you all to come along.
Bring someone with you, a friend, brother or sister, who you think would also enjoy The Space.

During this evening we will hold various flash talks, short presentations of +- 15 minutes, on various topics.
Would you like to give a flash talk yourself? Please contact the board!
