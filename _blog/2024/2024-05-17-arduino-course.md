---
title: Arduino Course (EN)
author: Jeanne
date: 2024-05-26
layout: blog
redirect_from:
- /arduino-course/
header: /uploads/2024/arduinocourse2.png
---
Space Leiden is hosting an 8 lesson course on Arduino! 

<!--// more //-->

Recently we were approached by Shay, who offered to host his Arduino course at our space! Shay is very knowledgeable about Arduino, sensors, and coding, and has a passion for passing on his knowledge to others. 
During this 8 lesson course he will tell us all about it. Each lesson will delve deeper into the matter, starting from controlling a simple LED, and moving on to other modules, sensors and components each week. We are very excited about this new course, and we hope to see you there!

The course will start on the 6th of June, and take place every week on Thursday evening at our space. We will ask participants for a small financial compensation, so that we can compensate Shay for his hard work. This will be 5 euros per lesson, for a total of 40 euros. Payment can be completed at once during the first lesson. 

In order to be able to contact you during the course in the case of any changes, cancellations or moving of lessons, we would ask you to register for the course in the link [here](https://forms.gle/UV2gw2z3smrvnHrP7).

<video src="/uploads/2024/Arduino_promo_video_1.mp4" alt="Arduino course promo video" style="width:600px" controls></video>

### What do you need?
Firstly, and most importantly, you will need to bring your own laptop, with a working wifi connection. 

Arduino is a piece of hardware, along with all its components and materials. What's better than actually handling all the pieces and connecting the wires to make your LED light up? The full list of components we will be using during the course is listed below. The full course can also be followed digitally using [Wokwi](https://wokwi.com/), an online simulator for Arduino, but we recommend getting your hands on the real thing! 

- 1 Arduino uno board
- 5 LEDs
- 2 Buttons
- 2 Potentiometers
- 1 RGB-LED
- 1 LDR
- 1 DHT11
- 1 HC-SR04
- 1 Passive Buzzer
- 1 Active Buzzer
- 1 Relay
- 220 Ohm resistors 
- 2K, 5K, 10K Ohm resistors 
- 1 IR Remote control + IR Receiver

At Space Leiden we have some (second hand) Arduino Uno boards available for you, which you can purchase or borrow from us to use during the course. We do not have all the components unfortunately, so the rest of them you will have to obtain by yourself. Don't worry if you don't have the hardware before the course starts, remember you can follow each lesson without the hardware, using the online simulator Wokwi.

We will be available each Thursday night and during the lessons for questions, about the course or our space. You can also contact us at bestuur@spaceleiden.nl or through our social media channels. 

<video src="/uploads/2024/Arduino_promo_video_2.mp4" alt="Arduino course promo video 2" style="width:600px" controls></video>

### Summed up!

To sum it all up!

Our course will start on the **6th of June**, lasting **8 weeks every Thursday night**. We will open at 19:00 and the lessons will start at 19:30. Our address is Betaplein 28, Leiden. 

Course participation is **40 euros**, which can be paid during the lessons, with the teacher. 

Participants will need to **bring their own laptop**. The full course can be followed using Wokwi.com, and if you want to use the physical hardware, the full set is listed above. 

We hope to see you there! 

<img src="/uploads/2024/arduinocourse%20kopie.png" alt="Arduino course" style="width:600px"/>