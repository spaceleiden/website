---
title: Activiteiten Mei 2024
author: Jeanne
date: 2024-04-28
layout: blog
redirect_from:
- /mei-2024/
header: /uploads/2024/spring.jpg
---
De agenda voor Mei is bekend! Deze maand zijn er wat dingen anders dan normaal, lees hier verder voor meer informatie.
(English below)

<!--// more //-->

### 02 mei - Borrel

Onze locatie is helaas gesloten ivm de meivakantie en examens die worden afgenomen door de scholen in het pand. Maar 
dat betekent niet dat we niet wat leuks gaan doen! 2 mei doen we een borrel bij restaurant Vet Gezond, om elkaar eens 
wat beter te leren kennen in een andere context, onder het genot van een biertje. We zullen vanaf 19:00 uur bij Vet 
Gezond zijn, en het eerste rondje trakteren wij ;).

### 09 mei - GESLOTEN - Hemelvaartsdag

9 Mei is Hemelvaartsdag en zullen wij gesloten zijn. We hebben deze avond ook geen externe activiteit.

### 16 mei - Dungeons & Dragons

Het is weer tijd voor een waanzinnig avontuur. 16 mei spelen we weer een nieuwe one-shot! Dit is een kort verhaal wat 
we in een sessie kunnen afronden. Dit verhaal zal ook geschikt zijn voor mensen die nog nooit Dungeons and Dragons 
hebben gespeeld. Heb je geen eigen dobbelstenen? Geen probleem, wij zorgen dat die aanwezig zijn. We zullen ook enkele
character sheets mee nemen waar jullie uit kunnen kiezen, dus je hoeft geen character voor te bereiden.
Om eventuele nieuwe spelers het spel uit te kunnen leggen voor aanvang, zullen wij iets eerder aanwezig zijn dan normaal.

Het is nodig om je te registreren omdat we een beperkt aantal plekken voor deelnemers hebben: https://forms.gle/kotdG2kG2Lg61mRBA

- 18:00 uur: speluitleg voor beginners
- 19:00 uur: start spel

Deze avond zijn we natuurlijk ook open om gewoon met je project aan de slag te gaan, als je dat liever doet. Er zijn 
misschien wel wat minder bestuursleden beschikbaar om te helpen, dus houd daar rekening mee als je iets wilt doen waar 
je wat hulp en uitleg bij nodig hebt. 

### 23 mei - Arduino Course Introduction 🇬🇧

Wij starten binnenkort met onze eerste cursus! In een serie van 8 lessen gaat Shay Goldin jullie mee nemen in de wereld
van Arduino. Hij zal beginnen bij de basis, en elke les wat verder in gaan op alle mogelijkheden en onderdelen die je 
kunt gebruiken om met Arduino je eigen device of prototype te maken. Deze serie lessen zal in juni beginnen, maar deze 
maand bieden we alvast een proefles! We zullen een bijdrage vragen voor de cursus zodat wij Shay kunnen compenseren voor 
zijn inzet, maar de eerste les is gratis! Meer informatie over de exacte data en kosten volgen binnenkort, dus houd onze
blog in de gaten! Deze serie lessen zal in het Engels worden gegeven.

### 30 mei - Projectavond

Deze week hebben we geen programma, maar is de Space gewoon open om lekker aan de slag te gaan met je eigen project!
Neem je materialen mee en ga lekker aan het werk. Heb je ergens hulp of uitleg bij nodig? Spreek gerust een van de 
bestuursleden aan.

# Programme for May!

### 02 May - drinks

Our location will unfortunately be closed this week due to holidays, and exams taking place in the building. But that 
doesn't mean we won't be doing something fun! The 2nd of May we will be having some drinks at restaurant Vet Gezond, to
get to know one another better in a different context, under the enjoyment of a beer. We will gather at 19h at Vet 
Gezond, and the first round of beers will be on us ;).

### 09 May - CLOSED - Ascension Day 

The 9th of May we will be closed, because Ascension Day is a national holiday in The Netherlands. There will not be an 
external activity this evening.

### 16 May - Dungeons and Dragons

It is time for another wonderful adventure. The 16th of May we will be playing a new one-shot! This is a short adventure
which we can finish in a single session. This story will also be accessible to people who have never played Dungeons
and Dragons before. Do you not have your own dice? No worries, we will make sure there are enough present. We will also
bring some character sheets for you to choose from, so you don't have to prepare a character.
To be able to explain the game to new players before we start playing, we will be present at the location somewhat 
earlier than usual. 

Registration is necessary because we have a limited number of spots available: https://forms.gle/kotdG2kG2Lg61mRBA. If you do not speak Dutch, send us an email in addition to your registration and we'll try to set 
up a table speaking only in English! 

- 18:00 h: game explanation for beginner players
- 19:00 h: start of the game

Of course you can also work on your own projects this evening, if you wish to! We might have fewer board members 
available for help though, so keep that in mind if you want to do something you need help with. 

### 23 May - Arduino Course Introduction 🇬🇧

We are starting our first course soon! In a series of 8 lessons, Shay Goldin will introduce you to the world of Arduino.
He will start at the very basis, and each week will go deeper into all the possibilities and parts you can use to 
build your own device or prototype. This series of lessons will start in June, but this month we will offer an 
introductory lesson! We will ask for a financial contribution for the course so that we can imburse Shay for his efforts 
and input, but the first lesson is free! More information on the exact dates and costs will follow soon, so keep an eye 
on our blog! This series of lessons will be fully in English.

### 30 May - Project night

This week we have no programme, but we will be open to get working on your own projects! Bring your materials and get 
working! Do you need help or an explanation for something? Don't hesitate to ask one of our board members. 