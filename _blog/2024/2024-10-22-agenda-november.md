---
title: Activiteiten November 2024  
author: Raoul  
date: 2024-10-22  
layout: blog  
redirect_from:  
- /november-2024/  
header: /uploads/2024/sint-piet.png
---
De agenda voor november is bekend! Ook deze maand hebben we weer een interessante mix van projecten, talks en puzzels. Lees verder voor meer informatie over de activiteiten.  
(English below)

<!--// more //-->

### 07 november - Projectavond
Op donderdag 7 november houden we weer een projectavond. 
Heb je een eigen project waar je aan wilt werken, of zoek je inspiratie? 
Iedereen is welkom om samen te werken, kennis te delen en aan de slag te gaan in een creatieve sfeer!

<img src="/uploads/2024/project-avond.png" alt="Projectavond" style="width:600px"/>

### 14 november - Talk over DIVD
Op 14 november duiken we in de wereld van cyber security met een interessante talk over het Dutch Institute for Vulnerability Disclosure (DIVD).
Het DIVD, is een Nederlandse non-profitorganisatie die zich richt op het opsporen en rapporteren van kwetsbaarheden in digitale systemen. 
Hun doel is om de veiligheid van het internet te verbeteren door proactief beveiligingsproblemen te identificeren en deze op een verantwoorde manier te melden 
aan de betrokken partijen. Een medewerker van de DIVD komt ons vertellen hoe dit in zijn werk gaat en wat de impact is van hun werk.

<img src="/uploads/2024/divd.png" alt="Talk over DIVD" style="width:600px"/>

### 21 november - Projectavond
We hebben op 21 november nog een projectavond gepland. 
Neem je werk mee, vraag om hulp, of kom gewoon voor de gezelligheid en om te zien waar anderen mee bezig zijn!

<img src="/uploads/code.jpg" alt="Projectavond" style="width:600px"/>


## November 28 - Break-out-box Avond (Doel, Thema, design and Digital Puzzles)
We beginnen met het doel, thema en ontwerp vast te stellen voor onze escape-box, daarna gaan we nu verder met puzzels en duiken we in de digitale kant!
Deze sessie in de reeks richt zich volledig op digitale puzzels voor onze breakout box. Denk aan Arduinos,
Raspberry Pi’s en andere slimme elektronische oplossingen! Samen zullen we brainstormen, ontwerpen en bouwen aan een high-tech puzzelbox
die de perfecte mix biedt van creativiteit en technologie.

<img src="/uploads/2024/escapebox.png" alt="Break-out-box avond" style="width:600px"/>

# Programme for November!

## November 7 - Project Evening
On November 7, we’re hosting another project evening. 
Bring your own project, or join to get inspired! 
Everyone is welcome to collaborate, share knowledge, and enjoy a creative atmosphere!

<img src="/uploads/2024/project-avond.png" alt="Project Evening" />

## November 14 - Talk about DIVD
On November 14th, we will dive into the world of cybersecurity with an engaging talk about the Dutch Institute for Vulnerability Disclosure (DIVD). 
The DIVD is a Dutch non-profit organization focused on detecting and reporting vulnerabilities in digital systems. 
Their goal is to enhance internet security by proactively identifying security issues and responsibly disclosing them to the relevant parties. 
A member of the DIVD will join us to explain how this process works and what the impact of their work is.

<img src="/uploads/2024/divd.png" alt="Talk about DIVD" />

## November 21 - Project Evening
Another project evening is scheduled for November 21. 
Bring your work, ask for help, or just enjoy the company and see what others are working on!

<img src="/uploads/code.jpg" alt="Project Evening" />

## November 28 - Break-out-box Evening (Purpose, Theme, design and Digital Puzzles)
We’ll start by defining the goal, theme, and design for our escape box, and then move on to the puzzles and dive into the digital side! 
This session in the series focuses entirely on digital puzzles for our breakout box. 
Think Arduinos, Raspberry Pis, and other smart electronic solutions! Together, we’ll brainstorm, design, and build a high-tech puzzle 
box that offers the perfect mix of creativity and technology.

Will we see you there?

<img src="/uploads/2024/escapebox.png" alt="Break-out-box evening" />  
