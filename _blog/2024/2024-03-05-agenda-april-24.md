---
title: Activiteiten April 2024
author: Raoul
date: 2024-03-05
layout: blog
redirect_from:
- /april-2024/
header: /uploads/2024/pasen.JPG
---
Het is bijna april en dat betekent pasen! In april hebben we, zo vlak na de open-dag, weer een maand vol leuke activiteiten geplanned!

(English below)

<!--// more //-->

### 04 april - HardwareFest
Op 4 april is het weer tijd voor hardwarefest! Raoul neemt gezellig al zijn hardware mee!
Met verschillende sensoren, Arduino's, Raspberry Pi's en andere development boards gaan we lekker aan de slag!
Kom jij prototypen, leren of gewoon gezellig meedoen?

<img src="/uploads/2023/hw_hackaton.png" alt="Hardware" style="width:400px"/>

### 11 april - Inkscape & Lasersnijder
Op de Space Leiden hebben we een hele mooie Trotec lasersnijder.
Ben jij benieuwd hoe jij hiervoor moet ontwerpen en deze lasersnijder kan gebruiken?
Dion neemt ons mee in de wondere wereld van Inkscape en de lasersnijder.
Ben jij erbij?

<img src="/uploads/space_laser.jpg" alt="Laser snijder" style="width:400px"/>

### 18 april - Blender 101
Op 18 april neemt Jeanne ons mee in de wondere wereld van Blender 3D.
In Blender 101 leer je de basis van het 3D modelleren en laat Jeanne zien hoe jij je eigen 3D model kan maken of sculpten.

<img src="/uploads/2024/sculptedSnake.jpg" alt="Tangled Snake" style="width:400px"/>

### 25 april - HackTheBox
25 april staat in het teken van security en ethical hacking. Sander neemt ons mee om een leuke box te poppen.
Ben jij geïnteresseerd in hacking en security? Kom dan zeker langs! Sander is in het dagelijks leven forensisch IT specialist en kan je alles vertellen over security.

<img src="/uploads/2023/hackthebox.png" alt="HackTheBox" style="width:400px"/>

## Agenda for April (EN)

### 04 april - HardwareFest
On the 4th of April it is time for hardwarefest! Raoul will bring all his hardware!
With several sensors, Arduino's Raspberry Pi's and other development boards we will start building and playing. Want to come make a prototype, or discover new things?

<img src="/uploads/2023/hw_hackaton.png" alt="Hardware" style="width:400px"/>

### 11 april - Inkscape & Lasercutter
At Space Leiden we have a beautiful Trotex lasercutter. Are yiou curious about how to design and use this lasercutter? Dion will introduce us to the wondrous world of Inkscape and the lasercutter. Will you join us?

<img src="/uploads/space_laser.jpg" alt="Laser snijder" style="width:400px"/>

### 18 april - Blender 101
On april 18th Jeanne will take us along into the world of Blender 3D. In Blender 101 you will learn the basics of 3D modelling and Jeanne will show you how to model or sculpt your own 3D model!

<img src="/uploads/2024/sculptedSnake.jpg" alt="Tangled Snake" style="width:400px"/>

### 25 april - HackTheBox
25 april is all about security & ethical hacking. Sander will guide us on popping a cool box. Are you interested in hacking and security? Be sure to join! Sander is a forensic IT specialist in real life, and can tell you everything about security.

<img src="/uploads/2023/hackthebox.png" alt="HackTheBox" style="width:400px"/>


