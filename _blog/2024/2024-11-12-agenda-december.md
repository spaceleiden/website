---
title: Activiteiten December 2024
author: Raoul
date: 2024-11-12
layout: blog
redirect_from:
  - /december-2024/
header: /uploads/2024/sint_3D.PNG  
---
De agenda voor december is bekend! We hebben een feestelijke mix van creatieve en gezellige activiteiten gepland. 
Van Sinterklaasavond met een nieuwe blender wedstrijd en Advent of Code, tot onze traditionele eindejaars pubquiz. Lees verder voor meer informatie!  

<!--// more //-->

(English below)

### 05 december - Sinterklaas-blender + AOC Helppunt
We beginnen de maand feestelijk op 5 december met een creatieve Sinterklaas-blender wedstrijd. 
Maak jij het beste sinterklaas themed 3D model?. Daarnaast is er ook een Advent Of Code (AoC) helppunt voor al je vragen over de puzzels van dit jaar!
Kom langs voor inspiratie, hulp en gezelligheid!

<img src="/uploads/2024/sint_3D.PNG" alt="Sinterklaas-blender en AOC Helppunt" style="width:600px"/>

### 12 december - Projectavond
Op donderdag 12 december is er weer een projectavond. Heb je een lopend project waar je verder aan wilt werken, of wil je gewoon wat feedback? 
Iedereen is welkom om samen te werken, kennis te delen en van elkaar te leren! Waar ga jij aan werken? 
Er zijn genoeg tools om mee te werken, van 3D-printers tot soldeerbouten en de lasersnijder, tot dan!

<img src="/uploads/code.jpg" alt="Projectavond" style="width:600px"/>

### 19 december - Eindejaars Pubquiz
Op 19 december sluiten we het jaar af met onze beroemde eindejaarspubquiz! 
Test je kennis over allerlei onderwerpen, van de Space Leiden, techniek en wetenschap, IT tot popcultuur, strijd om de befaamde eer en een kleine prijs.
Een avond vol gezelligheid en competitie – de perfecte afsluiting van het jaar!

<img src="/uploads/pubquiz.jpeg" alt="Eindejaars Pubquiz" style="width:600px"/>

### 26 december - DICHT - Kerst
Op donderdag 26 december is de Space gesloten vanwege Kerst. 
We wensen iedereen fijne feestdagen en kijken ernaar uit om in het nieuwe jaar weer samen te komen!

<img src="/uploads/christmas.jpeg" alt="Dicht - Kerst" style="width:600px"/>

# Programme for December!
### December 5 - Sinterklaas Blender Challenge + AoC Help Desk
We’re kicking off the month on December 5 with a festive Sinterklaas-themed blender contest. Can you create the best Sinterklaas-themed 3D model? Additionally, there will be an Advent of Code (AoC) help desk for all your questions about this year’s puzzles! Join us for inspiration, help, and fun!

<img src="/uploads/2024/sint_3D.PNG" alt="Sinterklaas Blender and AoC Help Desk" style="width:600px"/>

### December 12 - Project Evening
On Thursday, December 12, we’re hosting another project evening. Do you have an ongoing project you'd like to work on or just want some feedback? Everyone is welcome to collaborate, share knowledge, and learn from each other! What will you be working on? We have plenty of tools available, from 3D printers and soldering irons to the laser cutter. See you there!

<img src="/uploads/code.jpg" alt="Project Evening" style="width:600px"/>

### December 19 - End-of-Year Pub Quiz
On December 19, we wrap up the year with our famous end-of-year pub quiz! Test your knowledge on a variety of topics, from Space Leiden, technology and science, IT to pop culture, and compete for the prestigious honor and a small prize. A night full of fun and friendly competition – the perfect way to end the year!

<img src="/uploads/pubquiz.jpeg" alt="End-of-Year Pub Quiz" style="width:600px"/>

### December 26 - CLOSED - Christmas
On Thursday, December 26, the Space will be closed for Christmas. We wish everyone happy holidays and look forward to gathering again in the new year!

<img src="/uploads/christmas.jpeg" alt="Closed - Christmas" style="width:600px"/>
