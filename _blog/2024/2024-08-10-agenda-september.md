---
title: Activiteiten September 2024
author: Raoul
date: 2024-08-27
layout: blog
redirect_from:
- /september-2024/
header: /assets/img/solderen_2.jpg
---
De agenda voor september is bekend! We hebben deze maand weer een mix van creatieve en uitdagende activiteiten gepland, van 3D design tot hacking challenges. Lees hieronder verder voor meer informatie.
(English below)

<!--// more //-->

### 05 september - Projectavond
Deze week houden we het lekker informeel! Neem je eigen projecten mee en kom werken in de Space. Of je nu aan je hardware of software project werkt, er is altijd ruimte voor hulp, advies en inspiratie. De Space is open voor iedereen die wil werken, leren en experimenteren.

<img src="/uploads/2024/pc.png" alt="Projectavond" style="width:600px"/>

### 12 september - Blender 3D Workshop
Heb je altijd al willen leren hoe je 3D modellen maakt? Deze donderdag duiken we in Blender, de open-source 3D design software. Of je nu een beginner bent of al wat ervaring hebt, we helpen je op weg met de basis en geven je tips om je modellen tot leven te brengen. Neem je laptop mee en installeer alvast Blender (te vinden op [blender.org](https://www.blender.org)).

<img src="/uploads/2024/sculptedSnake.jpg" alt="Blender Workshop" style="width:600px"/>

### 19 september - Hack The Box Challenge
Zin in een uitdaging? Doe mee met onze Hack The Box avond! Hack The Box is een online platform waar je je hacking skills kunt testen op verschillende virtuele machines en scenario's. Samen gaan we proberen de vlaggen te veroveren en nieuwe technieken te leren. Of je nu een beginner bent of al ervaring hebt, iedereen is welkom om mee te doen.

<img src="/uploads/2023/hackthebox.png" alt="Hack The Box" style="width:600px"/>

### 26 september - Projectavond
We sluiten de maand af met nog een relaxte projectavond. Dit is je kans om verder te werken aan je eigen project, feedback te krijgen van anderen of gewoon te genieten van een productieve avond in goed gezelschap. Zoals altijd staan de deuren van de Space open voor iedereen!

<img src="/uploads/code.jpg" alt="Projectavond" style="width:600px"/>

# Programme for September!
### September 5 - Project Evening
This week we keep it informal! Bring your own projects and come work at the Space. Whether you're working on hardware or software, there's always room for help, advice, and inspiration. The Space is open to everyone who wants to work, learn, and experiment.

<img src="/uploads/2024/pc.png" alt="Projectavond" style="width:600px"/>

### September 12 - Blender 3D Workshop
Have you always wanted to learn how to create 3D models? This Thursday, we're diving into Blender, the open-source 3D design software. Whether you're a beginner or already have some experience, we'll guide you through the basics and provide tips to bring your models to life. Bring your laptop and make sure to install Blender (available at [blender.org](https://www.blender.org)).

<img src="/uploads/2024/sculptedSnake.jpg" alt="Blender Workshop" style="width:600px"/>

### September 19 - Hack The Box Challenge
Up for a challenge? Join us for our Hack The Box night! Hack The Box is an online platform where you can test your hacking skills on various virtual machines and scenarios. Together, we will try to capture the flags and learn new techniques. Whether you're a beginner or have experience, everyone is welcome to join.

<img src="/uploads/2023/hackthebox.png" alt="Hack The Box" style="width:600px"/>

### September 26 - Project Evening
We end the month with another relaxed project evening. This is your chance to continue working on your own project, get feedback from others, or simply enjoy a productive evening in good company. As always, the Space doors are open to everyone!

<img src="/uploads/code.jpg" alt="Project Evening" style="width:600px"/>
