---
title: Activiteiten Oktober 2024
author: Raoul
date: 2024-10-08
layout: blog
redirect_from:
- /oktober-2024/
header: /uploads/2024/halloween.png
---
De agenda voor oktober is bekend! We hebben deze maand weer een mix van creatieve en uitdagende activiteiten gepland, van 3D design tot hacking challenges. Lees hieronder verder voor meer informatie.
(English below)

<!--// more //-->

### 03 oktober - DICHT
Deze donderdag is de Space Leiden gesloten vanwege het 450 jarige viering van 3 oktober. We wensen iedereen een fijne viering!
<img src="/uploads/2024/3okt.png" alt="3 oktober - DICHT" style="width:600px"/>

### 10 oktober - Spelletjes avond
Soms is het gewoon tijd voor wat anders! Neem je favoriete bordspel of game console mee, vraag al je vrienden mee naar de space, en kom gezellig met ons spellen spelen!
Zien we je dan?

<img src="/uploads/2024/spellenavond.png" alt="Spellen avond" style="width:600px"/>

### 17 oktober - Hardware Festival
Het is weer tijd voor Hardware Fest! Neem je electronica mee, sensoren, RPis of whatever else! We gaan samen aan de slag met solderen, programmeren en experimenteren. 
Heb je geen eigen hardware? Geen probleem, we hebben genoeg spullen om te delen! Dion en Raoul gaan aan de slag met het maken van een kleine speaker, kom je ook?

<img src="/uploads/2024/speakers.png" alt="Hardware fest" style="width:600px"/>

### 24 oktober - Projectavond
Op 24 oktober hebben we een gezellige project avond. Dit is je kans om verder te werken aan je eigen project, feedback te krijgen van anderen of 
gewoon te genieten van een productieve avond in goed gezelschap. Zoals altijd staan de deuren van de Space open voor iedereen!

<img src="/uploads/code.jpg" alt="Projectavond" style="width:600px"/>

### 31 oktober - Box Escape - Maak je eigen escape box
We sluiten de maand af met een gezellige halloween-avond! Deze avond is de eerste van een serie aan avonden waar we samen gaan werken aan het maken van een
Escape puzzel box! Leuk als spel, kado verpakking of surprise! Van fysieke puzzels tot hardwarematige electronische puzzels, we brainstormen en gaan dan (met behulp van de gamma om de hoek)
aan de slag met het maken van de boxen! Deze sessie staat in het teken van het doel, thema en design van onze boxes!

<img src="/uploads/2024/escapebox.png" alt="Escape Puzzel Box" style="width:600px"/>

# Programme for Oktober!
## October 3 - CLOSED
This Thursday, Space Leiden is closed due to the 450th anniversary celebration of October 3rd. We wish everyone a happy celebration! 

<img src="/uploads/2024/3okt.png" alt="3 oktober - DICHT" />

## October 10 - Game Night
Sometimes it's just time for something different! Bring your favorite board game or game console, invite all your friends to the space, 
and come play games with us! See you there?  

<img src="/uploads/2024/spellenavond.png" alt="Spellen avond" />

## October 17 - Hardware Festival
It's time for Hardware Fest again! Bring your electronics, sensors, RPis, or whatever else!
We will work together on soldering, programming, and experimenting. Don't have your own hardware? No problem, we have plenty of stuff 
to share! Dion and Raoul will be working on making a small speaker, will you join?  

<img src="/uploads/2024/speakers.png" alt="Hardware fest" />

## October 24 - Project Evening
On October 24, we have a cozy project evening. This is your chance to continue working on your own project, get feedback from others, 
or simply enjoy a productive evening in good company. As always, the doors of the Space are open to everyone!  

<img src="/uploads/code.jpg" alt="Projectavond" />

## October 31 - Box Escape - Make Your Own Escape Box
We end the month with a cozy Halloween evening! This evening is the first of a series of evenings where we will work together on making an 
Escape puzzle box! Great as a game, gift box, or surprise! From physical puzzles to hardware electronic puzzles, we brainstorm and then 
(with the help of the hardware store around the corner) get to work on making the boxes! This session focusses on the goal, theme and design of our boxes!

<img src="/uploads/2024/escapebox.png" alt="Escape Puzzel Box" />
