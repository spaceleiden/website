---
title: Activiteiten Juni 2024
author: Raoul
date: 2024-06-15
layout: blog
redirect_from:
- /juni-2024/
header: /uploads/2024/summer_rain_banner.jpg
---
De agenda voor Juni is bekend! Deze staat de hele maand in het teken van de Arduino course van Shay, maar dat is natuurlijk niet het enige wat we gaan doen. Lees hieronder verder voor meer informatie.
(English below)

<!--// more //-->

### 06 juni - Start Arduino cursus 1
Deze week begint  onze eerste cursus! In 8 engelstalige lessen leer je alles over arduino. 
Je kunt de hele cursus volgen met alleen je laptop en online simulator wokwi, of je kunt alle hardware fysiek aanschaffen en daarmee mee doen! 
Lees alles nog eens rustig na in onze [blog post](https://spaceleiden.nl/blog/arduino-course/). In de eerste les gaan we aan de slag met het volgende:

1. Arduino UNO board.
2. breadboard
3. buttons
4. LEDs
5. 5K Ohn resistor
6. 220 Ohm resistors
7. Jumper wires

Zien we je daar?

<img src="/uploads/2024/arduino_course.png" alt="arduino cursus" style="width:600px"/>

### 13 juni - Projectavond / Arduino cursus 2
Deze week is de tweede les van de Arduino cursus maar hebben we verder geen programma, de Space is gewoon open om lekker aan de slag te gaan met je eigen project! 
Neem je materialen mee en ga lekker aan het werk. Heb je ergens hulp of uitleg bij nodig? 
Spreek gerust een van de bestuursleden aan.

### 20 juni - BattleBots maken / Arduino cursus 3
Het is weer tijd voor hardware festival, met dit keer het thema battle robots! 
We maken donderdag samen leuke borstelrobots, en daarna krijg je een hele week de tijd om die te pimpen, upgraden en 
battle ready te maken voor de battle arena volgende week 27 juni! Maak jij m slimmer, sterker of mooier? 
Er is een leuk prijsje voor de winnaar!

Daarnaast is deze week de derde les van de Arduino cursus, en ben je natuurlijk welkom om lekker met je eigen project aan de slag te gaan, 
of met de rest van de hardware te werken. Zien we je donderdag?

<img src="/uploads/2024/battlebot_1.jpeg" alt="BattleBots" style="width:600px"/>

### 27 juni - BattleBots Battle! / Arduino cursus 4
Zit jij ook weg te smelten thuis? Op de Space is het meestal lekker koel! Deze week geeft Shay de 4e les van de Arduino cursus, 
welke gaat over user input en het maken van een binary klok. 

Daarnaast gaan wij verder met de borstelrobots. Je krijgt nog even de tijd om aan je robot verder te werken, om 20:30 zullen we een battle doen, 
en hebben we een prijsje voor de winnaar. 

Wil je alsnog mee doen? Er zijn nog robotjes, en je hebt dus nog even tijd om er een te maken voor het begin van de battle! ;)
Uiteraard is er zoals altijd ruimte om met je eigen project aan de slag te gaan! Zien we je vanavond?

<img src="/uploads/2024/battlebot_2.jpeg" alt="BattelBots Wedstrijd" style="width:600px"/>

# Programme for June!
### June 6 - Start Arduino course 1
Our first course starts this week! In 8 English lessons you will learn everything about Arduino.
You can take the entire course with just your laptop and online simulator wokwi, or you can physically purchase all the hardware and join in!
Read everything carefully in our [blog post](https://spaceleidingen.nl/blog/arduino-course/). In the first lesson we will work on the following:

1. Arduino UNO board.
2. breadboard
3. buttons
4. LEDs
5. 5K Ohn resistor
6. 220 Ohm resistors
7. Jumper wires

Will we see you there?

<img src="/uploads/2024/arduino_course.png" alt="arduino course" style="width:600px"/>

### June 13 - Project evening / Arduino course 2
This week is the second lesson of the Arduino course, but we have no further program, the Space is open to get started on your own project!
Bring your materials and get to work. Do you need help or explanation with something?
Feel free to speak to one of the board members.

### June 20 - Making BattleBots / Arduino course 3
It's time for the hardware festival again, this time with the theme battle robots!
We will make nice brushing robots together on Thursday, and then you will have a whole week to pimp, upgrade and improve them.
battle ready for the battle arena next week June 27! Will you make me smarter, stronger or more beautiful?
There is a nice prize for the winner!

In addition, this week is the third lesson of the Arduino course, and you are of course welcome to get started on your own project,
or work with the rest of the hardware. Will we see you on Thursday?

<img src="/uploads/2024/battlebot_1.jpeg" alt="BattleBots" style="width:600px"/>

### June 27 - BattleBots Battle! / Arduino course 4
Are you also melting away at home? It is usually nice and cool at the Space! This week Shay teaches the 4th lesson of the Arduino course,
which is about user input and creating a binary clock.

We will also continue with the brush robots. You will have some time to continue working on your robot, at 8:30 PM we will have a battle,
and we have a prize for the winner.

Do you still want to participate? There are still robots, so you still have time to make one before the start of the battle! ;)
Of course, as always, there is room to get started on your own project! Will we see you tonight?

<img src="/uploads/2024/battlebot_2.jpeg" alt="BattelBots Competition" style="width:600px"/>
