---
title: Activiteiten Maart 2024
author: Raoul
date: 2024-02-28
layout: blog
redirect_from:
- /maart-2024/
header: /uploads/2023/together2.jpeg
---
Het is maart! Bijna tijd voor lente! In maart hebben we weer een hoop leuke dingen geplanned!

<!--// more //-->

### 7 maart - CTF prijsuitreiking
Op 7 maart doen we de prijsuitreiking van onze CTF.
Ook zullen we jullie vragen over moeilijke cases beantwoorden.
Welke dingen vonden jullie moeilijk of juist te makkelijk?
Hopelijk tot dan!

<img src="/uploads/ctf_2023.png" alt="CTF" style="width:300px"/>

### 14 maart - PCB Talk / Workshop
Op 14 maart hebben we een talk/ workshop gepland over het maken van je eigen PCB!

[-> Inschrijven kan hier! <- Click hier!](http://tinyurl.com/space-pcbs)

Marcel, hardware engineer bij Munisense B.V., vertelt ons alles, van het ontwerpen tot het bestellen van een PCB.
Waarna je ook zelf aan de slag kan gaan met het ontwerpen van een eigen P1 meter (om je slimme meters in de meterkast uit te lezen).

Wil je gelijk een P1 meter mee naar huis? Voor 10-15 euro (afhankelijk van de hoeveelheid deelnemers) zorgen wij voor een PCB en onderdelen om te monteren! Meld je aan via de link, en tot dan!

<img src="/uploads/2024/pcb_talk.jpeg" alt="PCBs" style="width:300px"/>

### 21 maart - Project avond
Op 21 maart weer met een gezellige project avond!
Kom langs met je eigen projecten of kom met een van onze tools werken.

<img src="/uploads/climate-sensor.jpg" alt="Climate Sensor" style="width:400px"/>

### 28 maart - Paaseieren lasersnijden en schilderen
Het is bijna pasen en soms hebben we even een leuk projectje nodig om ons huis te versieren, vind je ook niet?
Kom gezellig paaseieren lasersnijden en schilderen bij Space Leiden!

<img src="/uploads/2024/paasei.png" alt="Project Avond" style="width:400px"/>

### 30 maart - Internationale Hackerspaces Opendag
Ieder jaar is er de internationale Hackerspaces opendag op de laatste zaterdag van maart.
Dit jaar is dat 30 maart! En ook wij zijn weer van de partij!
We doen weer wat leuke demo's en workshops en zijn gewoon open voor projecten!
Dus heb je zin om een dagje op zaterdag op de Space te zijn? Kom dan langs!

Wil je zelf een demo geven of een talk doen? Neem dan contact op met bestuur@spaceleiden.nl!

Voor meer over het programma van de opendag neem een kijkje op de pagina van de [open-dag](/open-dag/)

<img src="/uploads/2024/opendag2024.png" alt="Opendag" style="width:400px"/>