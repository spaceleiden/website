---
title: Activiteiten zomer (juli / augustus) 2022
author: Raoul
date: 2022-06-30
layout: blog
redirect_from:
- /juli-2022/
- /augustus-2022/
- /juli-augustus-2022/
header: /uploads/summertime.jpeg
---
De agenda voor deze zomer is bekend! Het is vakantie en vrij warm maar dat houd ons niet tegen!
Iedere donderdag zullen we gewoon gezellig open zijn! Lees hier wat we deze maand gaan doen.

<!--// more //-->

### 7 juli Projectavond
Op 7 juli zijn we open voor projectwerk.
We hebben geen workshop of talk, maar je bent welkom om je project mee te nemen en aan de slag te gaan, om gezellig bij te
praten, te werken met de lasersnijder, 3D printer of andere tools of gewoon te sparren over je project.

### 14 juli Talk - How to delete yourself from the internet
Sander gaat ons vertellen hoe je jezelf kunt verwijderen van het internet, geen social media meer en niet meer vindbaar willen zijn? 
Benieuwd? Kom langs!

<img src="/uploads/delete_yourself.jpg" alt="delete_yourself" style="width:500px"/>

Naast de talk ben je natuurlijk altijd welkom om te werken aan je eigen projecten!

### 21 juli Projectavond
Op 21 juli zijn we weer open voor projectwerk.
We hebben geen workshop, maar je bent welkom om je project mee te nemen en aan de slag te gaan, om gezellig bij te
praten, of te sparren over je project.

### 28 juli D&D Oneshot
Voor de 28e gaan we een keertje wat anders doen. D&D! Ben je al een oude rot in het vak of een nieuweling die graag wilt leren?
Het idee is om gezellig met de geïnteresseerde aanwezigen een oneshot te gaan doen. Inschrijven kan [hier](https://forms.gle/qFEKGcqKvka69hRy7)! Maar let op! Vol=vol. Zien we je dan?

<img src="/uploads/oneshot.jpeg" alt="oneshot" style="width:300px"/>

Naast de oneshot ben je natuurlijk altijd welkom om te werken aan je eigen projecten!

### 04 augustus Projectavond
Op 04 augustus zijn we weer open voor projectwerk.
We hebben geen workshop, maar je bent welkom om je project mee te nemen en aan de slag te gaan, om gezellig bij te
praten, of te sparren over je project.

### 11 augustus Talk / Workshop Onder Optie: Het Proces van taarten versieren
Op 11 augustus hebben we mogelijk een gastspreker die ons het een en ander kan vertellen over het versieren van taarten. 
Benieuwd hoe je zoiets aanpast of welke tips en tricks je bij je eigen baksels kan gebruiken? Kom langs!

### 18 augustus Projectavond
Op 18 augustus zijn we weer open voor projectwerk.
We hebben geen workshop, maar je bent welkom om je project mee te nemen en aan de slag te gaan, om gezellig bij te
praten, of te sparren over je project.

### 25 augustus WorkshopAvond D&D props
Op The Space hebben we veel verschillende mensen die werken aan allerlei props voor D&D.
Het idee van deze avond is gezellig samen aan de slag! Benieuwd waar iedereen mee bezig is? 
Aan het werk aan dungeon tiles of andere props? Of wil je liever gewoon mini's schilderen? 
Kom samen met gelijkgestemden werken aan jouw wereld! 

<img src="/uploads/tiles.jpg" alt="tiles" style="width:700px"/>

Geen zin in D&D? Je kan natuurlijk ook altijd werken aan je eigen projecten. 
Meer leren over de lasersnijder of gewoon lekker 3D printen.
