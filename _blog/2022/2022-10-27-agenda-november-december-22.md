---
title: Activiteiten November / December 2022
author: Raoul
date: 2022-10-27
layout: blog
redirect_from:
- /november-2022/
- /december-2022/
- /nov-dec-2022/
header: /uploads/christmas.jpeg
---

Onze planning voor november en december is bekend!

<!--// more //-->

Het is alweer bijna november, en wij beginnen ons voor te bereiden op het najaar en de winter. Ook in november en december hebben we weer een aantal leuke talks op de planning staan!

<img src="/uploads/oneshot.jpeg" alt="oneshot" style="width:300px"/>

### 3 november - workshop: DND oneshot
Vanwege de overweldigende opkomst van de vorige keer is het tijd om het nog een keer te organiseren!
Op donderdag 3 november (volgende week) nemen Jeanne, Jelle, Jordy en Raoul jullie mee in een nieuw avontuur!

Heb je je dice klaar? Veel voorbereiding aan jullie kant zal niet nodig zijn.
Er zullen premade karakters beschikbaar zijn om te spelen. Kies een class en ga los!

Nog nooit geD&Dt maar wel benieuwd? Geen probleem! We zullen deze avond wat eerder beginnen en dan kun je alle uitleg horen!

Rond 18:00 zullen we verzamelen voor een hapje eten (pizza) en de uitleg voor zij die nog nooit eerder gespeeld hebben. De sessies zullen rond 19:00 van start gaan en we verwachten rond 22:30-23:00 klaar te zijn!

Vanwege het feit dat er gebalanceerde groepen moeten zijn hebben we een inschrijving. Dit betekent ook dat er een maximum aan spelers kan mee spelen. Wil je erbij zijn, zorg er dan voor dat je je tijdig aanmeldt.

[Aanmelden klik hier!](https://forms.gle/CcjX7xCDbwBep9ts9)

### 10 november - Projectavond
Op 10 november is er weer een gezellige project avond!
Kom langs met je eigen projecten of kom met een van onze tools werken!

### 17 november - Projectavond
Op 17 november is er weer een gezellige project avond!
Kom langs met je eigen projecten of kom met een van onze tools werken!

<img src="/uploads/vue-react.jpeg" alt="vue-react" style="width:800px"/>

### 24 november - Crawler bouwen
We hadden een hele coole talk geplanned over de frontend frameworks Vue en React.
Helaas kan deze komende donderdag niet doorgaan door een scheduling error.
De talk zal verplaatst worden naar donderdag 8 december.

Niet getreurd! In plaats daarvan zal Jeffrey een workshop geven over het maken van een web crawler!
Jeffrey zal zijn eigen crawler schrijven in PHP en mocht je mee willen doen neem vooral je eigen laptop mee!
Wil je werken in je eigen taal? Dat kan natuurlijk ook! Het principe is namelijk voor alle talen wel gelijk.

Zien we je dan?

### 1 december - Advent Of Code
Het is weer die tijd van het jaar! Een nieuwe advent kalender, 
de heuze Advent Of Code! https://adventofcode.com/ 

Raoul neemt jullie mee en samen gaan we weer wat opdrachtjes maken. 

Naast deze talk / workshop zal Raoul iedere ochtend (je hoort het goed... IEDERE OCHTEND)
rond een uurtje of 7 op discord zijn om zijn advent of code struggles te streamen. 

Vind je het leuk om mee te doen? Wees er dan bij! 

Mee doen met het leaderboard van The Space? 
Code: 125095-36a87d5b

Mogelijk is er een leuk prijsje te winnen voor de leader van het leaderboard!

### 8 december - Vue / React
Op 8 december geeft Wesley Klop een leuke talk over Vue en React.
Deze keer neemt Wesley jullie mee in de wereld van frontend frameworks.
Hij zal het gaan hebben over Vue en React. Wat is het? Wat doet het?
En wanneer kan je het beste welk framework gebruiken.
Benieuwd? Wees erbij donderdag 8 december vanaf 19:30!

<img src="/uploads/aoc.jpeg" alt="AOC" style="width:800px"/>

### 15 december - Workshop Kerstbal maken
15 december geeft Dion een workshop kerstballen maken met de Lasersnijder!
Wil jij een eigen coole kerstbal voor in de boom? Kom langs en maak er een!

<img src="/uploads/pubquiz.jpeg" alt="PubQuiz" style="width:800px"/>

### 22 december - PubQuiz
Als jaar afsluiter doen we dit jaar een leuke non-alcoholische pubquiz!
Ben jij een meester in gekke weetjes, popcultuur en The Space?
Kom en doe mee met onze PubQuiz! 

### 29 december - Gesloten!
Het is kerst! Een mooie tijd van het jaar en om die reden hebben we besloten om 
geen fysieke avond te organiseren. In het nieuwe jaar zijn jullie weer welkom!
