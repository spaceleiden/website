---
title: Capture the flag 2022
author: Raoul & Dion
date: 2022-02-09
layout: blog
redirect_from:
  - /capture-the-flag-22/
  - /projects/capture-the-flag-22/
header: /uploads/blog-ctf-slots.jpg
---
Ook dit jaar organiseert hackerspace The Space Leiden een maand lang online
een capture the flag evenement. Iedere dag komen er twee nieuwe challenges online.

<!--// more //-->

Lekker hacken, cracken en puzzelen in de nieuwe 2022 Space CTF. We hebben verschillende categorieen waaronder Netsec, Forensisch en natuurlijk ook gewone kennisvragen.
En dit allemaal gewoon vanuit jou luie stoel! Wel organiseren wij een aantal momenten waarin wij op de donderdag avond gezamelijk door de challenges heenlopen. Mocht je nou ergens vastzitten kijken we er dan gezamelijk naar en delen we tips uit.
Ook zitten wij dagelijks bij het opengaan van een nieuwe challenge rond 13:37 vaak klaar in [Discord]({{ site.socials.discord }}) om gezellig samen aan de slag te gaan.

Al deze challenges zijn samengekomen in een thema: Kaasino, the best casino in Zwammerdam.
Het casino, Kaasino, is jaren lang een fysiek spectakel geweest in Zwammerdam.
Ondanks dat ze de boot gemist hebben met het overgaan naar het moderne tijdperk. 
Hebben ze vanwege COVID toch maar de gok genomen en hebben ze een online omgeving gelanceerd. 

![](/uploads/blog-ctf-kaasino.png)

De CTF is nog deze hele maand actief. Aan het einde van de maand zullen we de winnaar bekend maken en deze een leuke prijs aanbieden.
Mocht je dus nog een plek zoeken om op een leuke manier te leren hoe CTFs werken,
of ben je een doorgewinterde CTF'er en wil je je skills up-to-date houden, neem
dan snel een kijkje op [ctf.spaceleiden.nl](https://ctf.spaceleiden.nl)!
