---
title: Activiteiten september 2022
author: Jeanne
date: 2022-08-14
layout: blog
redirect_from:
- /september-2022/
- /september-2022/ 
header: /uploads/september-2022.jpg
---

Onze planning voor september is bekend!

<!--// more //-->

<img src="/uploads/cal-sept-22.jpeg" alt="calendar september 2022" style="width:500px"/>

De zomervakantie is weer bijna voorbij, iedereen gaat weer aan het werk, of begint aan een nieuw collegejaar. 
Om te vieren dat we allemaal weer terug zijn, dat we een mooie zomer hebben gehad, en eens bij te praten over wat we deze zomer hebben meegemaakt, beginnen we september met een opening van het nieuwe jaar. 
Op **donderdag 1 september** nodigen we jullie allemaal uit om langs te komen. Neem eens iemand mee, een vriend, vriendin, broer of zus, van wie je denkt dat die The Space ook heel leuk zou vinden.

De week daarna gaan we van start met een mooie serie workshops en talks. Voor elke avond geldt dat we vanaf 19:00 uur geopend zijn, en de workshop/talk begint om 19:30.

### 8 september - live Hack The Box
Niels en Sander gaan weer samen aan de slag om een box te kraken. Kom er bij zitten, kijk mee, en denk met ze mee! Neem ook gerust je eigen laptop mee om live mee te doen, of met een eigen box aan de slag te gaan.

### 15 september - Workshop self-watering plant
Wegens succes in de herhaling: we werken nog een keer samen met Offerzen om een workshop neer te zetten over de self-watering plant.

Tijdens deze workshop bouwen we met sensoren en een arduino een systeem wat de vochtigheid van de aarde meet, en op basis daarvan water toedient aan je plant. 
Dat is nog eens handig! Materialen worden verzorgd door Offerzen.

Deze workshop vindt plaats op 15 september vanaf 19:00 uur op Betaplein 28 in Leiden.
Let op dit is een half uur eerder dan normaal! We zullen ook een half uurtje eerder open zijn!

Meld je nu aan en ontvang gratis alle benodigdheden voor dit project, met een leuke goodiebag van Offerzen. Je kunt je hier [inschrijven](https://offerzen.typeform.com/to/F9HMsj9a)

<img src="/uploads/offerzen.jpeg" alt="calendar september 2022" style="width:500px"/>

### 22 september - flashtalks: honeypot, webscraping en lasersnijden
Deze avond geven we drie korte talks die een beeld geven van wat voor projecten en activiteiten we zoal doen bij The Space. Een ideale avond voor mensen die nieuw zijn bij ons! Heb je zelf een flashtalk (+- 20 minuten) die je zou willen geven, of wil je een van je projecten presenteren? Dat kan! Geef dat even door via bestuur@spaceleiden.nl, dan plannen we je in!

### 29 september - workshop ontwerpen voor de 3D printer
Heb jij altijd al eens wat willen printen, maar geen idee wat? Of heb je een heel specifiek model nodig wat je nergens kan vinden? Doe dan mee met onze workshop Blender voor de 3D printer. Neem een eigen laptop mee om op te werken, en zorg dat je van tevoren vast Blender hebt geinstalleerd, dan kunnen we vlot aan de slag tijdens de workshop.

Zien we jou in september?
