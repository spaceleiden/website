---
layout: blog
title: Fysiek bezoek is weer mogelijk!
author: Raoul
redirect_from: /opening-2022
header: /uploads/opening_header.jpg
---
Per 25 januari 2022 zijn er verschillende versoepelingen doorgevoerd door de overheid.
Hierdoor kunnen wij jullie met trots melden dat we vanaf deze donderdag
(27 januari) weer fysiek open gaan.

<!--// more //-->

Haal je projecten uit het stof, koop je nieuwe materialen in, we kunnen weer aan
de slag! We zullen fysiek aanwezig zijn op onze locatie bij Technolab. Jullie
zijn welkom van 19:00 tot 22:00 uur om te komen werken aan jullie eigen projecten.

Wel gelden er nog een aantal basis regels, [lees ze hier](/covid-19).

Betreffende onze talks en workshops deze zullen nog even digitaal gegeven worden omdat ze op die manier al waren geplanned.

Mochten jullie vragen hebben dan horen wij dit graag via [onze mail](/contact)
of via [Discord]({{ site.socials.discord }}). Zien we jullie snel?

Liefs,
Raoul namens Team The Space

![opening_2021.jpg](/uploads/opening_header.jpg)
