---
title: Activiteiten agenda april 2022
author: Jeanne
date: 2022-03-30
layout: blog
redirect_from:
  - /april-2022/ 
header: /assets/img/padlock.jpg
---
De agenda voor april is bekend! We hebben twee toffe workshops gepland, en zijn elke donderdag geopend voor projecten!

<!--// more //-->
De agenda voor april is bekend! We hebben twee toffe workshops gepland, en zijn elke donderdag geopend voor projecten.

Donderdag 7 april hebben we een talk en demonstratie over het lockpicken van een Anker 3800. Dit slot staat bekend als
een van de grotere uitdagingen in de lock picking community.

En donderdag de 21e bieden we een workshop Blender 3D. Leer een ontwerp maken voor de 3D printer! 
Voor deze workshop is het handig om je eigen laptop mee te nemen, en van tevoren alvast de gratis software [Blender 3D 
te installeren](https://www.blender.org/download/).

Als het niet mogelijk is om een eigen laptop mee te brengen, stuur ons dan van tevoren een berichtje op 
bestuur@spaceleiden.nl, dan helpen wij met een oplossing, zodat je toch mee kan doen met de workshop.

![](/uploads/apr2022.png)
