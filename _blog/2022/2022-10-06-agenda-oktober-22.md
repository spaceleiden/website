---
title: Activiteiten Oktober 2022
author: Raoul
date: 2022-10-06
layout: blog
redirect_from:
- /oktober-2022/
header: /uploads/september-2022.jpg
---

Onze planning voor oktober is bekend!

<!--// more //-->

Oktober staat ook wel bekend als een spooky maand, het echte begin van de herfst. Stormen, regen, harde wind en alles komt tot zijn climax bij een mooie avond genaamd Halloween.
Ook wij zijn deze oktober weer van de partij. Ondanks dat we echte Leidenaren zijn zullen we geen activiteit doen rondom 3 oktober. Daar kunnen we in de stad al genoeg om genieten.
Wel hebben we deze maand weer twee talks paraat!.

### 6 oktober - Talk: Tesorion War-stories
6 oktober komt Suzanne vertellen over haar werkzaamheden als incident handler bij Tesorion.
Ze zal komen uitleggen wat haar job inhoudt en als kers op de taart gaat ze ons een warstory vertellen over een ransomware incident waar zij bij betrokken is geweest.

Kleine disclaimer:
De talk van Suzanne is TLP: RED.
Dit houdt in dat de inhoud van de talk niet gedeeld mag worden met anderen buiten de aanwezigen.

Benieuwd naar Suzanne haar talk? Kom dan langs! De talk zal om 19:30 beginnen!

### 20 oktober - Talk: Mede maken
Op 20 oktober zal Raoul vertellen over een van zijn hobbies. Het brouwen van mede!
Mede is een alcoholische honing drank die veel in de middeleeuwen gedronken werd. 

Ben je benieuwd naar wat mede precies is, wat er voor nodig is om deze hobby zelf op te pakken? 
Kom donderdag langs!
